<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MainsController@verion2');
Route::get('/version2', 'MainsController@verion2');

Route::resource('main-deps', 'MainDepsController');
Route::resource('departments', 'DepartmentsController');
Route::resource('job-types', 'JobTypesController');
Route::resource('jobs', 'JobsController');
Route::resource('shifts', 'ShiftsController');
Route::resource('templatems', 'TemplateMsController');
Route::resource('staff-shares', 'StaffSharesController');
Route::get('/qc-staffs/uploadAll', 'QcStaffsController@upload');
Route::post('/qc-staffs/uploadAction', 'QcStaffsController@uploadAction');
Route::resource('qc-staffs', 'QcStaffsController');
Route::get('/laco-staffs/upload', 'LacoStaffsController@upload');
Route::post('/laco-staffs/uploadAction', 'LacoStaffsController@uploadAction');
Route::resource('laco-staffs', 'LacoStaffsController');
Route::resource('laco-task-jobs', 'LacoTaskJobsController');
Route::get('/laco-tasks/generate/{id}', 'LacoTasksController@generateJobs');
Route::post('/laco-tasks/generateAction/{id}', 'LacoTasksController@generateJobsAction');
Route::get('/laco-tasks/adddetail/{laco_task_id}/{laco_task_job_id}', 'LacoTasksController@adddetail');
Route::get('/laco-tasks/detetepost/{laco_task_id}/{laco_job_pos_id}', 'LacoTasksController@detetepost');
Route::get('/laco-tasks/editpos/{laco_job_pos_id}', 'LacoTasksController@editpos');
Route::post('/laco-tasks/editposAction/{laco_job_pos_id}', 'LacoTasksController@editposAction');
Route::get('/laco-tasks/printbarcode/{id}', 'LacoTasksController@printbarcode');


Route::resource('laco-tasks', 'LacoTasksController');
Route::get('/laco-task-dates/generatePlan/{laco_task_id}', 'LacoTaskDatesController@generatePlan');
Route::post('/laco-task-dates/generatePlanAction/{laco_task_id}', 'LacoTaskDatesController@generatePlanAction');
Route::get('/laco-task-dates/resetPlan/{laco_task_date_id}', 'LacoTaskDatesController@resetPlan');

Route::get('/laco-task-dates/scanjob/{laco_task_date_id}/{laco_staff_id}', 'LacoTaskDatesController@scanjob');
Route::get('/laco-task-dates/scanstaff/{laco_task_date_id}', 'LacoTaskDatesController@scanstaff');
Route::get('/laco-task-dates/removestaffjob/{laco_task_date_id}/{laco_staff_id}', 'LacoTaskDatesController@removestaffjob');
Route::get('/laco-task-dates/dashboard/{laco_task_date_id}', 'LacoTaskDatesController@dashboard');
Route::get('/laco-task-dates/scanprocess/{laco_task_date_id}', 'LacoTaskDatesController@scanprocess');
Route::get('/laco-task-dates/scanprocessaction/{laco_task_date_id}', 'LacoTaskDatesController@scanprocessaction');
Route::get('/laco-task-dates/changestatus/{laco_task_date_id}/{status}', 'LacoTaskDatesController@changestatus');


Route::get('/laco-task-dates/scaninout/{laco_task_date_id}', 'LacoTaskDatesController@scaninout');
Route::get('/laco-task-dates/scaninoutaction/{laco_task_date_id}', 'LacoTaskDatesController@scaninoutaction');

Route::get('/laco-task-dates/export/{laco_task_date_id}', 'LacoTaskDatesController@export');

Route::resource('laco-task-dates', 'LacoTaskDatesController');





Route::get('/staffs/import', 'StaffsController@import');
Route::post('/staffs/importAction', 'StaffsController@importAction');
Route::get('/staffs/setExp/{id}', 'StaffsController@setExp');
Route::post('/staffs/setExpAction/{id}', 'StaffsController@setExpAction');
Route::get('/staffs/cometowork/{date}/{shiftid}/{maindepid}', 'StaffsController@cometowork');
Route::get('/staffs/manualcome/{processlogid}/{date}/{shiftid}/{depid}', 'StaffsController@manualcome');
Route::get('/staffs/importExp', 'StaffsController@importExp');
Route::post('/staffs/importExpAction', 'StaffsController@importExpAction');
Route::get('/staffs/uploadHr', 'StaffsController@uploadHr');
Route::post('/staffs/uploadHrAction', 'StaffsController@uploadHrAction');

Route::get('/staff-shares/listshare/{staff_id}', 'StaffSharesController@listshare');
Route::get('/staff-shares/add/{staff_id}', 'StaffSharesController@add');

Route::resource('staffs', 'StaffsController');

Route::get('/templatems/setJob/{id}', 'TemplateMsController@setJob');
Route::post('/templatems/setJobAction/{id}', 'TemplateMsController@setJobAction');
Route::get('templatems/createdetail/{template_m_id}', 'TemplateMsController@createdetail');
Route::post('templatems/createdetailAction/{template_m_id}', 'TemplateMsController@createdetailAction');
Route::get('templatems/deletedetail/{template_d_id}', 'TemplateMsController@deletedetail');

Route::resource('planms', 'PlanMsController');

Route::get('planms/dashboard/{plan_m_id}', 'PlanMsController@dashboard');
Route::get('planms/createdetail/{plan_m_id}', 'PlanMsController@createdetail');
Route::post('planms/createdetailAction/{plan_m_id}', 'PlanMsController@createdetailAction');
Route::get('planms/deletedetail/{plan_d_id}', 'PlanMsController@deletedetail');
Route::get('planms/editdetail/{plan_d_id}', 'PlanMsController@editdetail');
Route::post('planms/editdetailAction/{plan_d_id}', 'PlanMsController@editdetailAction');
Route::get('planms/template2Plan/{template_m_id}', 'PlanMsController@template2Plan');
Route::post('planms/template2PlanAction/{template_m_id}', 'PlanMsController@template2PlanAction');
Route::get('planms/clearmap/{plan_m_id}', 'PlanMsController@clearmap');
Route::get('planms/remap/{plan_m_id}', 'PlanMsController@remap');
Route::get('planms/remapall/{date}/{shift}', 'PlanMsController@remapall');
Route::get('planms/cometowork/{plan_d_user_id}', 'PlanMsController@cometowork');
Route::get('planms/assignmanual/{plan_d_user_id}', 'PlanMsController@assignmanual');
Route::post('planms/assignmanualaction/{plan_d_user_id}', 'PlanMsController@assignmanualAction');
Route::get('planms/processdateagain/{date}/{shift}/{redirect}', 'PlanMsController@processdateagain');
Route::get('planms/getscan/{date}', 'PlanMsController@getscan');
Route::get('planms/scanlist/select', 'PlanMsController@scanlist');
Route::get('planms/getselectscan/{date}/{ip}/{port}', 'PlanMsController@getselectscan');

Route::get('DashboardQcs/index', 'DashboardQcsController@index');
Route::get('DashboardQcs/view/{date}', 'DashboardQcsController@view');
Route::get('DashboardQcs/mapview/{date}/{shiftid}', 'DashboardQcsController@mapview');
Route::get('DashboardQcs/viewdetail/{pos_code}/{pos_txt}/{date}', 'DashboardQcsController@viewdetail');

Route::get('monitors/main/{date}', 'MonitorsController@main');

Route::get('scans/index', 'ScansController@index');
Route::post('scans/savedata', 'ScansController@savedata');

Route::get('Process/job/{laco_task_date_id}', 'ProcesssController@processjob');
Route::get('Process/timeline/{laco_task_date_id}', 'ProcesssController@processtimeline');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');



