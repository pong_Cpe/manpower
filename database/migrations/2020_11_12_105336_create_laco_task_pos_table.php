<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLacoTaskPosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('laco_task_pos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('laco_task_id');
            $table->integer('laco_task_job_id');
            $table->string('position_code', 20);
            $table->string('position_x', 20);
            $table->string('position_y', 20);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('laco_task_pos');
    }
}
