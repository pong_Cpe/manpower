<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLacoTaskDateDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('laco_task_date_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('laco_task_date_id');
            $table->integer('laco_task_job_id');
            $table->integer('laco_task_pos_id')->nullable();
            $table->integer('laco_staff_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('laco_task_date_details');
    }
}
