<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLacoTaskDatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('laco_task_dates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('laco_task_id');
            $table->date('process_date');
            $table->integer('shift_id');
            $table->integer('plan_staff');
            $table->text('desc')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('laco_task_dates');
    }
}
