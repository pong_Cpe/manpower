<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlanMsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plan_ms', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',200);
            $table->datetime('startdatetime')->nullable();
            $table->datetime('enddatetime')->nullable();
            $table->integer('template_m_id')->nullable();
            $table->text('desc');
            $table->string('status',20);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plan_ms');
    }
}
