<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLacoTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('laco_transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('laco_staff_id');
            $table->integer('laco_task_date_id')->nullable();
            $table->integer('laco_task_date_detail_id')->nullable();
            $table->string('process')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('laco_transactions');
    }
}
