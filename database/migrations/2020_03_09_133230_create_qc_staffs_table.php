<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQcStaffsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('qc_staffs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('lacocode',20);
            $table->string('name',100);
            $table->string('type',10);
            $table->string('pos',50);
            $table->string('pos_code',10);
            $table->integer('shift_id')->nullable();
            $table->string('status', 20);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('qc_staffs');
    }
}
