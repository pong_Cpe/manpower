<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlanM extends Model
{
    public $fillable = [
        'name','startdatetime','enddatetime','template_m_id','desc','status','plandate','shift_id'
    ];

    public function templatem()
    {
        return $this->belongsTo('App\TemplateM', 'template_m_id', 'id');
    }

    public function shift()
    {
        return $this->belongsTo('App\Shift', 'shift_id', 'id');
    }

    public function pland()
    {
        return $this->hasMany('App\PlanD', 'plan_m_id');
    }

    public function planduser()
    {
        return $this->hasMany('App\PlanDUser', 'plan_m_id');
    }

    public function available_planduser()
    {
        return $this->planduser()->where('staff_id', '<>', '0');
    }

    public function comewithplanduser()
    {
        return $this->available_planduser()->where('status', 'Active');
    }
}
