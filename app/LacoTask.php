<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LacoTask extends Model
{
    protected $fillable = ['name','product','plan_staff','map','desc'];

    public function taskpos()
    {
        return $this->hasMany('App\LacoTaskPos', 'laco_task_id');
    }
}
