<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QcProcessLog extends Model
{
    protected $fillable = ['log_date', 'shift_id', 'user_no', 'status', 'staff_id'];

    public function staff()
    {
        return $this->belongsTo('App\QcStaff', 'staff_id', 'id');
    }
}
