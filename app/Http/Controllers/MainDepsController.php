<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\MainDep;
use Illuminate\Http\Request;

class MainDepsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $maindeps = MainDep::latest()->paginate($perPage);
        } else {
            $maindeps = MainDep::latest()->paginate($perPage);
        }

        return view('main-deps.index', compact('maindeps'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('main-deps.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();

        MainDep::create($requestData);

        return redirect('main-deps')->with('flash_message', 'MainDeps added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $maindep = MainDep::findOrFail($id);

        return view('main-deps.show', compact('maindep'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $maindep = MainDep::findOrFail($id);

        return view('main-deps.edit', compact('maindep'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $maindep = MainDep::findOrFail($id);
        $maindep->update($requestData);

        return redirect('main-deps')->with('flash_message', 'MainDeps updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        MainDep::destroy($id);

        return redirect('main-deps')->with('flash_message', 'MainDeps deleted!');
    }
}
