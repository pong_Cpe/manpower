<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\LacoTransaction;
use App\LacoTaskDateDetail;
use App\LacoStaff;
use App\LacoTaskDate;

class ProcesssController extends Controller
{
    public function processjob($laco_task_date_id){
        $data = array();
        $datainout = array();
        $staffjoblist = LacoTransaction::where('laco_task_date_id', $laco_task_date_id)
        ->groupBy('laco_staff_id', 'laco_task_date_detail_id')
        ->select('laco_staff_id', 'laco_task_date_detail_id')
        ->get();
        //var_dump($staffjoblist);
        foreach ($staffjoblist as $staffjobObj) {
            $staffTemp = LacoStaff::where('id', $staffjobObj->laco_staff_id)->first();
            $lacoTaskDateDetailTmp = LacoTaskDateDetail::where('id', $staffjobObj->laco_task_date_detail_id)->first();

            $tmpdata = LacoTransaction::where('laco_task_date_id', $laco_task_date_id)
                ->where('laco_staff_id', $staffjobObj->laco_staff_id)
                ->where('laco_task_date_detail_id', $staffjobObj->laco_task_date_detail_id)
                ->orderBy('created_at')
                ->get();
            $flag = "In";
            foreach ($tmpdata as $transactionObj) {

                $tmpTransaction = array();
                $tmpTransaction['staff_id'] = $staffTemp->id;
                $tmpTransaction['staff_name'] = $staffTemp->name;
                $tmpTransaction['laco_task_date_detail_id'] = $staffjobObj->laco_task_date_detail_id;
                
                if (!empty($lacoTaskDateDetailTmp)) {
                    $tmpTransaction['laco_task_job_id'] = $lacoTaskDateDetailTmp->lacotaskjob->id;
                    $tmpTransaction['job_name'] = $lacoTaskDateDetailTmp->lacotaskjob->name;
                }else{
                    $tmpTransaction['laco_task_job_id'] ="";
                    $tmpTransaction['job_name'] = "งานถูกลบไป id :".$staffjobObj->laco_task_date_detail_id;
                }
                $tmpTransaction['db_process'] = $transactionObj->process;
                $tmpTransaction['auto_process'] = $flag;
                $tmpTransaction['log_time'] = $transactionObj->created_at;
                
                $data[$staffTemp->id][$staffjobObj->laco_task_date_detail_idmin][] = $tmpTransaction;
             //   echo $transactionObj->created_at." ". $transactionObj->process." [Auto] ".$flag. "<br/>";
                if($flag == "In"){
                    $flag = "Out";
                }else{

                    $flag = "In";
                }
            }
          //  echo "<br/>";
            
        }
        //var_dump($data);
        return view('process.processjob', compact('data'));
    }

    public function processtimeline($laco_task_date_id)
    {
        $maindata = LacoTaskDate::where('id', $laco_task_date_id)->first();
        $data2 = array();
        $datainout = array();
        $data3 = array();
        $staffjoblist = LacoTransaction::where('laco_task_date_id', $laco_task_date_id)
            ->groupBy('laco_staff_id', 'laco_task_date_detail_id')
            ->select('laco_staff_id', 'laco_task_date_detail_id')
            ->get();
        //var_dump($staffjoblist);
        foreach ($staffjoblist as $staffjobObj) {
            $staffTemp = LacoStaff::where('id', $staffjobObj->laco_staff_id)->first();
            $lacoTaskDateDetailTmp = LacoTaskDateDetail::where('id', $staffjobObj->laco_task_date_detail_id)->first();

            //if (!empty($lacoTaskDateDetailTmp)) {

                // echo $staffTemp->name . ' | ' ;
                //  echo $lacoTaskDateDetailTmp->lacotaskjob->id . ' - ' ;
                //  echo $lacoTaskDateDetailTmp->lacotaskjob->name . '<br>';
                $tmpdata = LacoTransaction::where('laco_task_date_id', $laco_task_date_id)
                    ->where('laco_staff_id', $staffjobObj->laco_staff_id)
                    ->where('laco_task_date_detail_id', $staffjobObj->laco_task_date_detail_id)
                    ->orderBy('created_at')
                    ->get();
                $flag = "In";
                foreach ($tmpdata as $transactionObj) {

                    $tmpTransaction = array();
                    $tmpTransaction['staff_id'] = $staffTemp->id;
                    $tmpTransaction['staff_name'] = $staffTemp->name;
                    $tmpTransaction['laco_task_date_detail_id'] = $staffjobObj->laco_task_date_detail_id;
                    $tmpTransaction['db_process'] = $transactionObj->process;
                    $tmpTransaction['auto_process'] = $flag;
                    $tmpTransaction['log_time'] = $transactionObj->created_at;
                if (!empty($lacoTaskDateDetailTmp)) {
                    $data2[$staffTemp->name][$lacoTaskDateDetailTmp->lacotaskjob->name][] = $tmpTransaction;
                    $data3[$staffTemp->name][date("Y-m-d H:i:s", strtotime($transactionObj->created_at))][$lacoTaskDateDetailTmp->lacotaskjob->name] = $tmpTransaction;
                }else{
                    $data2[$staffTemp->name][date("Y-m-d H:i:s", strtotime($transactionObj->created_at))][] = $tmpTransaction;
                 //   $data3[$staffTemp->name][$transactionObj->created_at][$staffjobObj->laco_task_date_detail_id] = $tmpTransaction;
                }
                    //   echo $transactionObj->created_at." ". $transactionObj->process." [Auto] ".$flag. "<br/>";
                    if ($flag == "In") {
                        $flag = "Out";
                    } else {
                        $tmp2 = array();

                        $flag = "In";
                    }
                }
                //  echo "<br/>";
            //}
        }
     //dd($data3);
        return view('process.processtimeline', compact('data2', 'maindata'));
    }
}
