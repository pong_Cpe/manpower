<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\LacoTaskDate;
use App\LacoTask;
use App\LacoTaskDateDetail;
use App\Shift;
use App\LacoStaff;
use App\LacoTaskJob;
use App\LacoTransaction;
use App\LogPrepareM;
use App\PreProd;
use App\ShiftFt;
use App\StampM;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use DateTime;

class LacoTaskDatesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $lacotaskdates = LacoTaskDate::orderBy('process_date','DESC')->paginate($perPage);
        } else {
            $lacotaskdates = LacoTaskDate::orderBy('process_date', 'DESC')->paginate($perPage);
        }
        return view('laco-task-dates.index', compact('lacotaskdates'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('laco-task-dates.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();

        LacoTaskDate::create($requestData);

        return redirect('laco-task-dates')->with('flash_message', ' added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $lacotaskdate = LacoTaskDate::findOrFail($id);

        $taskjobArr = array();
        foreach ($lacotaskdate->lacotaskdatedetail()->get() as $taskposObj) {
            $taskjobArr[$taskposObj->lacotaskjob->name][] = $taskposObj;
        }

         


        
        return view('laco-task-dates.show', compact('lacotaskdate', 'taskjobArr'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $lacotaskdate = LacoTaskDate::findOrFail($id);

        $stampms = StampM::where('process_date', $lacotaskdate->process_date)->get();

        $stamplist = array();
        foreach ($stampms as $stampmobj) {
            $stamplist[$stampmobj->id] =  $stampmobj->matpack->matname . ' (' . $stampmobj->stampmachine->name .')';
        }
        //dd($stamplist);

        return view('laco-task-dates.edit', compact('lacotaskdate', 'stamplist'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();

        $lacotaskdate = LacoTaskDate::findOrFail($id);
        $lacotaskdate->update($requestData);

        return redirect('laco-task-dates')->with('flash_message', ' updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        LacoTaskDate::destroy($id);

        return redirect('laco-task-dates')->with('flash_message', ' deleted!');
    }

    public function generatePlan($laco_task_id){


        $shiftlist = Shift::pluck('name', 'id');
        $lacotask = LacoTask::findOrFail($laco_task_id);

        return view('laco-task-dates.generateplan', compact('lacotask', 'shiftlist'));
    }

    public function generatePlanAction(Request $request, $laco_task_id)
    {

        $lacotask = LacoTask::findOrFail($laco_task_id);

        $requestData = $request->all();
        $tmp = array();
        $tmp['laco_task_id'] = $laco_task_id;
        $tmp['process_date'] = $requestData['process_date'];
        $tmp['shift_id'] = $requestData['shift_id'];
        $tmp['plan_staff'] = $requestData['plan_staff'];
        $tmp['desc'] = $requestData['desc'];
        $tmp['status'] = 'Create';

        $lastid = LacoTaskDate::create($tmp)->id;

        foreach ($lacotask->taskpos()->get() as $taskposObj) {
            $tmpdetail = array();

            $tmpdetail['laco_task_date_id'] = $lastid;
            $tmpdetail['laco_task_job_id'] = $taskposObj->laco_task_job_id;
            $tmpdetail['laco_task_pos_id'] = $taskposObj->id;

            LacoTaskDateDetail::create($tmpdetail);

        }

        return redirect('laco-task-dates/'. $lastid)->with('flash_message', ' updated!');
    }

    public function resetPlan($laco_task_date_id){

    }

    public function scanstaff(Request $request,$laco_task_date_id){


        $lacostaffcode = $request->get('lacostaffcode');

        if(!empty($lacostaffcode)){
            $lacostaff = LacoStaff::where('lacoid',trim($lacostaffcode))->first();

            if (!empty($lacostaff)) {

                $lacotaskdatedetail = LacoTaskDateDetail::where('laco_task_date_id', $laco_task_date_id)->where('laco_staff_id', $lacostaff->id)->count();

                if($lacotaskdatedetail == 0){
                    return redirect('laco-task-dates/scanjob/' . $laco_task_date_id . "/" . $lacostaff->id)->with('flash_message', ' updated!');
                }else{
                    return redirect('laco-task-dates/scanstaff/' . $laco_task_date_id)->with('flash_message', ' Already add updated!');
                }

                
            }
        }

        $lacotaskdate = LacoTaskDate::findOrFail($laco_task_date_id);

        $taskjobArr = array();
        foreach ($lacotaskdate->lacotaskdatedetail()->get() as $taskposObj) {
            $taskjobArr[$taskposObj->lacotaskjob->name][] = $taskposObj;
        }

        return view('laco-task-dates.scanstaff', compact('lacotaskdate', 'taskjobArr'));
    }

    public function scanjob(Request $request, $laco_task_date_id,$laco_staff_id){
        $lacojobcode = $request->get('lacojobcode');

        $lacostaff = LacoStaff::findOrFail($laco_staff_id);
        if (empty($lacostaff)) {
            return redirect('laco-task-dates/scanstaff/' . $laco_task_date_id )->with('flash_message', ' updated!');
            
        }

        $lacotaskdate = LacoTaskDate::findOrFail($laco_task_date_id);

        $taskjobArr = array();
        $taskposArr = array();
        foreach ($lacotaskdate->lacotaskdatedetail()->get() as $taskposObj) {
            $taskjobArr[$taskposObj->lacotaskjob->name][] = $taskposObj;
            if(isset($taskposObj->lacotaskpos->position_code)){
                $taskposArr[$taskposObj->lacotaskjob->main_code][$taskposObj->lacotaskpos->position_code] = $taskposObj;
            }
            
        }

        if(!empty($lacojobcode)){
            if(strlen($lacojobcode)>4){

            
            $maincode = substr(trim($lacojobcode),0,4);
            //checkmain job
            if(isset($taskposArr[$maincode]) && isset($taskposArr[$maincode][trim($lacojobcode)])){                                
                if(empty($taskposArr[$maincode][trim($lacojobcode)]->laco_staff_id)){
                        $idcheck = $taskposArr[$maincode][trim($lacojobcode)]->id;
                        $lacotaskdatedetail = LacoTaskDateDetail::findOrFail($idcheck);
                        $lacotaskdatedetail->laco_staff_id = $laco_staff_id;
                        $lacotaskdatedetail->status = 'In';
                        $lacotaskdatedetail->update();

                        $tmptransact = array();
                        $tmptransact['laco_staff_id'] = $laco_staff_id;
                        $tmptransact['laco_task_date_id'] = $laco_task_date_id;
                        $tmptransact['laco_task_date_detail_id'] = $idcheck;
                        $tmptransact['process'] = 'In';
                        LacoTransaction::create($tmptransact);

                        (new LacoTaskDate())->updatestatus($laco_task_date_id, 'Start');
                        
                        return redirect('laco-task-dates/scanstaff/' . $laco_task_date_id)->with('flash_message', ' Add Staff to Job Complete!');
                }else{
                        return redirect('laco-task-dates/scanjob/' . $laco_task_date_id . "/" . $laco_staff_id)->with('flash_message', ' Already Position Code!');
                }
                
            }else{
                return redirect('laco-task-dates/scanjob/' . $laco_task_date_id . "/" . $laco_staff_id)->with('flash_message', ' Incorrect Code!');
            }
            
            }elseif (strlen($lacojobcode) == 4) {
                if (isset($taskposArr[$lacojobcode])) {

                    foreach ($taskposArr[$lacojobcode] as $keypos => $takpostobj) {
                        if(empty($takpostobj->laco_staff_id)){
                        
                            $lacotaskdatedetail = LacoTaskDateDetail::findOrFail($takpostobj->id);
                            $lacotaskdatedetail->laco_staff_id = $laco_staff_id;
                            $lacotaskdatedetail->status = 'In';
                            $lacotaskdatedetail->update();

                            $tmptransact = array();
                            $tmptransact['laco_staff_id'] = $laco_staff_id;
                            $tmptransact['laco_task_date_id'] = $laco_task_date_id;
                            $tmptransact['laco_task_date_detail_id'] = $takpostobj->id;
                            $tmptransact['process'] = 'In';
                            LacoTransaction::create($tmptransact);

                            (new LacoTaskDate())->updatestatus($laco_task_date_id, 'Start');

                            return redirect('laco-task-dates/scanstaff/' . $laco_task_date_id)->with('flash_message', ' Add Staff to Job Complete!');
                        }
                    }


                    $lacotaskjob = LacoTaskJob::where('main_code',trim($lacojobcode))->first();

                    if (!empty($lacotaskjob)) {

                        $tmpdetail = array();

                        $tmpdetail['laco_task_date_id'] = $laco_task_date_id;
                        $tmpdetail['laco_task_job_id'] = $lacotaskjob->id;
                        $tmpdetail['laco_staff_id'] = $laco_staff_id;
                        $tmpdetail['status'] = 'In';

                        $iddetail = LacoTaskDateDetail::create($tmpdetail)->id;

                        $tmptransact = array();
                        $tmptransact['laco_staff_id'] = $laco_staff_id;
                        $tmptransact['laco_task_date_id'] = $laco_task_date_id;
                        $tmptransact['laco_task_date_detail_id'] = $iddetail;
                        $tmptransact['process'] = 'In';
                        LacoTransaction::create($tmptransact);

                        (new LacoTaskDate())->updatestatus($laco_task_date_id, 'Start');

                        return redirect('laco-task-dates/scanstaff/' . $laco_task_date_id)->with('flash_message', ' Add Staff to Job Complete!');
                    }else{
                        return redirect('laco-task-dates/scanjob/' . $laco_task_date_id . "/" . $laco_staff_id)->with('flash_message', ' Incorrect Code!');
                    }
                }else{
                    return redirect('laco-task-dates/scanjob/' . $laco_task_date_id . "/" . $laco_staff_id)->with('flash_message', ' Incorrect Code!');
                }
            } elseif ($lacojobcode == 'non') {

                $lacotaskjob = LacoTaskJob::where('main_code', 'share')->first();

                $tmpdetail = array();

                $tmpdetail['laco_task_date_id'] = $laco_task_date_id;
                $tmpdetail['laco_task_job_id'] = $lacotaskjob->id;
                $tmpdetail['laco_staff_id'] = $laco_staff_id;
                $tmpdetail['status'] = 'In';
                

                $iddetail = LacoTaskDateDetail::create($tmpdetail)->id;

                $tmptransact = array();
                $tmptransact['laco_staff_id'] = $laco_staff_id;
                $tmptransact['laco_task_date_id'] = $laco_task_date_id;
                $tmptransact['laco_task_date_detail_id'] = $iddetail;
                $tmptransact['process'] = 'In';
                LacoTransaction::create($tmptransact);

                (new LacoTaskDate())->updatestatus($laco_task_date_id, 'Start');

                return redirect('laco-task-dates/scanstaff/' . $laco_task_date_id)->with('flash_message', ' Add Staff to Job Complete!');
            }
        }

        return view('laco-task-dates.scanjob', compact('lacotaskdate', 'taskjobArr', 'lacostaff'));
    }

    public function removestaffjob($laco_task_date_id,$laco_staff_id){
        $removedata = LacoTaskDateDetail::where('laco_task_date_id', $laco_task_date_id)->where('laco_staff_id', $laco_staff_id)->first();
        if(!empty($removedata->laco_task_pos_id)){
            $removedata->laco_staff_id = null;
            $removedata->status = null;
            $removedata->update();

            $rmLacoTransaction = LacoTransaction::where('laco_task_date_detail_id',$removedata->id)->first();

            LacoTransaction::destroy($rmLacoTransaction->id);
        }else{
            LacoTaskDateDetail::destroy($removedata->id);
        }
        

        return redirect('laco-task-dates/scanstaff/' . $laco_task_date_id)->with('flash_message', ' Remove Staff from Job Complete!');
               
    }

    public function changestatus($laco_task_date_id,$status){

        $lacotaskdate = LacoTaskDate::findOrFail($laco_task_date_id);
        $lacotaskdate->status = $status;
        $lacotaskdate->update();
        if($status == 'process'){
            return redirect('laco-task-dates/scanprocess/' . $laco_task_date_id)->with('flash_message', ' Start job!');       
        }elseif ($status == 'END') {
            foreach ($lacotaskdate->lacotaskdatedetail as $lacotaskdatedetailObj) {
                if($lacotaskdatedetailObj->status == 'In'){
                    
                    $tmptransact = array();
                    $tmptransact['laco_staff_id'] = $lacotaskdatedetailObj->laco_staff_id;
                    $tmptransact['laco_task_date_id'] = $lacotaskdatedetailObj->laco_task_date_id;
                    $tmptransact['laco_task_date_detail_id'] = $lacotaskdatedetailObj->id;
                    $tmptransact['process'] = 'Out';
                    LacoTransaction::create($tmptransact);

                    $lacotaskdatedetailObj->status = 'Out';
                    $lacotaskdatedetailObj->update();
                }   
            }
            return redirect('laco-task-dates')->with('flash_message', ' Stop job!');
        }else{
            return redirect('laco-task-dates')->with('flash_message', ' Stop job!');
        }
        
    }

    public function dashboard($laco_task_date_id){
        $lacotaskdate = LacoTaskDate::findOrFail($laco_task_date_id);

        $dataft = DB::connection('sqlsrv2')
        ->table('log_prepare_ms')
        ->join('log_prepare_ds', 'log_prepare_ms.id', '=', 'log_prepare_ds.log_prepare_m_id')
        ->join('pre_prods', 'pre_prods.id', '=', 'log_prepare_ms.pre_prod_id')
        ->join('shifts', 'shifts.id', '=', 'log_prepare_ds.shift_id')
        ->join('std_pre_prods', 'std_pre_prods.id', '=', 'log_prepare_ms.std_pre_prod_id')
        ->select(DB::raw('[log_prepare_ms].process_date,
[log_prepare_ms].targetperhr,
[log_prepare_ms].target_result,
[log_prepare_ms].target_workhours,
[log_prepare_ms].staff_target,
[log_prepare_ms].staff_operate,
[log_prepare_ms].staff_pf,
[log_prepare_ms].staff_pk,
[log_prepare_ms].staff_pst,
[log_prepare_ms].status,
[log_prepare_ds].process_datetime,
[log_prepare_ds].targets,
[log_prepare_ds].workhours,
[log_prepare_ds].input,
[log_prepare_ds].input_sum,
[log_prepare_ds].output,
[log_prepare_ds].output_sum,
[log_prepare_ds].num_pre,
[log_prepare_ds].num_iqf,
[log_prepare_ds].num_all,
[log_prepare_ds].note,
[log_prepare_ds].problem'))
        ->where('log_prepare_ms.process_date', $lacotaskdate->process_date)
        ->where('pre_prods.name', $lacotaskdate->lacotask->product)
        ->where('shifts.name', $lacotaskdate->shift->name)
        ->get();


        /*     $preprod = PreProd::where('name', $lacotaskdate->lacotask->product)->first();
        $shiftft = ShiftFt::where('name', $lacotaskdate->shift->name)->first();
        
        $logpreparem = LogPrepareM::where('process_date', $lacotaskdate->process_date)
            ->where('pre_prod_id', $preprod->id)
            ->first();
            */

        //var_dump($dataft);

        return view('laco-task-dates.dashboard',compact('lacotaskdate'));

    }

    public function scanprocess(Request $request, $laco_task_date_id)
    {

        $lacotaskdate = LacoTaskDate::findOrFail($laco_task_date_id);

        $taskjobArr = array();
        foreach ($lacotaskdate->lacotaskdatedetail()->get() as $taskposObj) {
            $taskjobArr[$taskposObj->lacotaskjob->name][] = $taskposObj;
        }

        return view('laco-task-dates.scanprocess', compact('lacotaskdate', 'taskjobArr'));
    }

    public function scanprocessaction(Request $request, $laco_task_date_id)
    {
        $strmessage = "รหัสไม่ถูกต้อง!";
        $lacostaffcode = $request->get('lacostaffcode');

        if (!empty($lacostaffcode)) {
            $lacostaff = LacoStaff::where('lacoid', trim($lacostaffcode))->first();

            if (!empty($lacostaff)) {

                $lacotaskdatedetail = LacoTaskDateDetail::where('laco_task_date_id', $laco_task_date_id)->where('laco_staff_id', $lacostaff->id)->first();

                if (!empty($lacotaskdatedetail)) {
                    $tmptransact = array();
                    $tmptransact['laco_staff_id'] = $lacostaff->id;
                    $tmptransact['laco_task_date_id'] = $laco_task_date_id;
                    $tmptransact['laco_task_date_detail_id'] = $lacotaskdatedetail->id;
                    if($lacotaskdatedetail->status == 'In'){

                        $strmessage = $lacotaskdatedetail->lacostaff->name . " Scan ออกห้อง";
                        $lacotaskdatedetail->status = 'Out';
                        $tmptransact['process'] = 'In';
                        
                    }else{
                        $strmessage = $lacotaskdatedetail->lacostaff->name . " Scan เข้าห้อง";
                        $lacotaskdatedetail->status = 'In';
                        $tmptransact['process'] = 'Out';

                    }
                    LacoTransaction::create($tmptransact);

                    $lacotaskdatedetail->update();
                    return redirect('laco-task-dates/scanprocess/' . $laco_task_date_id)->with('flash_message', $strmessage);
                } else {
                    return redirect('laco-task-dates/scanprocess/' . $laco_task_date_id)->with('flash_message', $strmessage);
                }
            }
        }
        return redirect('laco-task-dates/scanprocess/' . $laco_task_date_id )->with('flash_message', ' รหัสไม่ถูกต้อง!');
    }

    public function scaninout(Request $request, $laco_task_date_id)
    {

        $lacotaskdate = LacoTaskDate::findOrFail($laco_task_date_id);

        $taskjobArr = array();
        foreach ($lacotaskdate->lacotaskdatedetail()->get() as $taskposObj) {
            $taskjobArr[$taskposObj->lacotaskjob->name][] = $taskposObj;
        }

        return view('laco-task-dates.scaninout', compact('lacotaskdate', 'taskjobArr'));
    }

    public function scaninoutaction(Request $request, $laco_task_date_id)
    {
        $strmessage = "รหัสไม่ถูกต้อง!";
        $lacostaffcode = $request->get('lacostaffcode');

        if (!empty($lacostaffcode)) {
            $lacostaff = LacoStaff::where('lacoid', trim($lacostaffcode))->first();

            if (!empty($lacostaff)) {

                $lacotaskdatedetail = LacoTaskDateDetail::where('laco_task_date_id', $laco_task_date_id)->where('laco_staff_id', $lacostaff->id)->first();

                if (!empty($lacotaskdatedetail)) {
                    $tmptransact = array();
                    $tmptransact['laco_staff_id'] = $lacostaff->id;
                    $tmptransact['laco_task_date_id'] = $laco_task_date_id;
                    $tmptransact['laco_task_date_detail_id'] = $lacotaskdatedetail->id;
                    if ($lacotaskdatedetail->status == 'In') {

                        $strmessage = $lacotaskdatedetail->lacostaff->name . " Scan ออกห้อง";
                        $lacotaskdatedetail->status = 'Out';
                        $tmptransact['process'] = 'In';
                    } else {
                        $strmessage = $lacotaskdatedetail->lacostaff->name . " Scan เข้าห้อง";
                        $lacotaskdatedetail->status = 'In';
                        $tmptransact['process'] = 'Out';
                    }
                    LacoTransaction::create($tmptransact);

                    $lacotaskdatedetail->update();
                    return redirect('laco-task-dates/scaninout/' . $laco_task_date_id)->with('flash_message', $strmessage);
                } else {
                    return redirect('laco-task-dates/scaninout/' . $laco_task_date_id)->with('flash_message', $strmessage);
                }
            }
        }
        return redirect('laco-task-dates/scaninout/' . $laco_task_date_id)->with('flash_message', ' รหัสไม่ถูกต้อง!');
    }

    public function export($laco_task_date_id){
        $task_main = LacoTaskDate::join('laco_tasks', 'laco_task_dates.laco_task_id', '=', 'laco_tasks.id')
                        ->join('shifts', 'laco_task_dates.shift_id', '=', 'shifts.id')
                        ->where('laco_task_dates.id', $laco_task_date_id)
                        ->select('laco_tasks.name', 'laco_tasks.product', 'laco_task_dates.process_date', 'shifts.name AS shift_name', 'laco_task_dates.plan_staff')
                        ->get();
        // dd($task_main);
        $task_staff = LacoTaskDateDetail::leftJoin('laco_task_jobs', 'laco_task_date_details.laco_task_job_id', '=', 'laco_task_jobs.id')
                        ->leftJoin('laco_staffs', 'laco_task_date_details.laco_staff_id', '=', 'laco_staffs.id')
                        ->where('laco_task_date_details.laco_task_date_id', $laco_task_date_id)
                        ->select('laco_task_date_details.id', 'laco_task_jobs.name', 'laco_staffs.lacoid', 'laco_staffs.name AS staff_name', 'laco_task_date_details.laco_task_job_id', 'laco_task_date_details.laco_staff_id')
                        ->orderBy('laco_task_jobs.main_code')->orderBy('laco_task_date_details.id')
                        ->get();   
        // dd($task_staff);     
        foreach($task_staff as $key){
            $task_data[$key->laco_staff_id]['s_code'] = $key->lacoid;
            $task_data[$key->laco_staff_id]['s_name'] = $key->staff_name;
            $job_data[$key->id] = $key->name;
        }
        // dd($job_data);
        $tran_minmax = LacoTransaction::where('laco_task_date_id', $laco_task_date_id)
                        ->selectRaw('laco_staff_id, COUNT(id) AS count_tran, MIN(created_at) AS min_time, MAX(created_at) AS max_time')
                        ->groupBy('laco_staff_id')
                        ->get();
        foreach($tran_minmax as $key){
            $task_data[$key->laco_staff_id]['count_tran'] = $key->count_tran;
            // $datetime1 = new DateTime($key->min_time);
            // $datetime2 = new DateTime($key->max_time);
            // $interval = $datetime1->diff($datetime2);
            // $task_data[$key->laco_staff_id]['time_all'] = $interval->format('%H:%i:%s');
        }
        // dd($task_data);
        
        $tran_staff = LacoTransaction::where('laco_task_date_id', $laco_task_date_id)
                        ->select('id', 'laco_staff_id', 'laco_task_date_id', 'laco_task_date_detail_id', 'process', 'created_at', 'updated_at')
                        ->orderBy('laco_staff_id')->orderBy('created_at')
                        ->get();
        // dd($tran_staff);
        $i = 0;     
        $c = 0;     //count foreach
        $flag = "";
        $staff_id = "";
        $job_id = "";
        $st_work = "";
        $ed_work = "";
        $last_time = "";
        $this_time = "";
        $check_tran = array(); //0=เวลาในห้อง, 1=เวลานอกห้อง
        foreach($tran_staff as $key){
            $c++;
            if(!empty($job_data[$key->laco_task_date_detail_id])){              
                $use_job[$key->laco_task_date_detail_id] = $job_data[$key->laco_task_date_detail_id];
                $this_time = new DateTime($key->created_at);       
                if($i==0){
                    $st_work = new DateTime($key->created_at);
                    $flag = "In";
                    $staff_id = $key->laco_staff_id;
                    $job_id = $key->laco_task_date_detail_id;
                    $i=1;
                }else{              
                    if($staff_id == $key->laco_staff_id){ 
                        if($job_id == $key->laco_task_date_detail_id){ 
                            if($flag == "In"){
                                $flag = "Out";
                                if($last_time<>""){
                                    $interval = $last_time->diff($this_time);
                                    $check_tran[0][$staff_id][$job_id][] = $interval->format('%H:%i:%s');   //เวลาในห้อง
                                }  
                            }else{                            
                                $flag = "In";                            
                                if($last_time<>""){
                                    $interval = $last_time->diff($this_time);
                                    $check_tran[1][$staff_id][$job_id][] = $interval->format('%H:%i:%s');   //เวลานอกห้อง
                                }                            
                            }                           
                        }else{
                            $flag = "In";    
                            if($last_time<>""){
                                $interval = $last_time->diff($this_time);
                                $check_tran[0][$staff_id][$job_id][] = $interval->format('%H:%i:%s');   //เวลาในห้อง
                            }
                            $job_id = $key->laco_task_date_detail_id;                        
                        }  
                        $ed_work =  new DateTime($key->created_at);                  
                    }else{ 
                        $interval = $st_work->diff($ed_work);
                        $task_data[$staff_id]['time_all'] = $interval->format('%H:%i:%s');   //เวลาทั้งหมด
                        $st_work = new DateTime($key->created_at);

                        $staff_id = $key->laco_staff_id;
                        $job_id = $key->laco_task_date_detail_id;
                        $flag = "In"; 
                    }                     
                     
                }                             
                 
                $last_time = new DateTime($key->created_at);
                $tran_log[$key->laco_staff_id][$key->laco_task_date_detail_id][][$flag] = $key->created_at; 
                if($c==count($tran_staff)){                 
                    $interval = $st_work->diff($ed_work);
                    $task_data[$staff_id]['time_all'] = $interval->format('%H:%i:%s');   //เวลาทั้งหมด 
                }                 
            }
        }        
        // dd($tran_log);        

        // dd($check_tran);
        foreach($check_tran as $key=>$value){
            foreach($value as $kstaff=>$vstaff){
                foreach($vstaff as $kjob=>$vjob){
                    $h = 0;
                    $m = 0;
                    $s = 0;
                    foreach($vjob as $karr=>$varr){
                        $a = explode(":",$varr);  
                        $h = $h+$a[0];
                        $m = $m+$a[1];
                        $s = $s+$a[2];
                    }
                    if($s>60){
                        $s_add = intval($s / 60);
                        $m = $m + $s_add;
                        $s = $s-($s_add * 60);
                    }
                    if($m>60){
                        $m_add = intval($m / 60);
                        $h = $h + $m_add;
                        $m = $m-($m_add * 60);
                    }
                    $d[0]=$h;
                    $d[1]=$m;
                    $d[2]=$s;
                    $time[$key][$kstaff][$kjob] = implode(":", $d);
                }
            }
        }
        // dd($time); 

        foreach($time as $key=>$value){
            foreach($value as $kstaff=>$vstaff){
                $h = 0;
                $m = 0;
                $s = 0;
                foreach($vstaff as $kjob=>$vjob){
                    $a = explode(":",$vjob);  
                    $h = $h+$a[0];
                    $m = $m+$a[1];
                    $s = $s+$a[2];
                }
                if($s>60){
                    $s_add = intval($s / 60);
                    $m = $m + $s_add;
                    $s = $s-($s_add * 60);
                }
                if($m>60){
                    $m_add = intval($s / 60);
                    $h = $h + $m_add;
                    $m = $m-($m_add * 60);
                }
                $d[0]=$h;
                $d[1]=$m;
                $d[2]=$s;
                $time_work[$key][$kstaff] = implode(":", $d);
            }
        }
        // dd($time_work); 

        $filename = "report_sum_task_" . date('ymdHi');

        // return view('export.sum_task',compact('task_main','task_data','tran_log','use_job','time','time_work'))->with('success','Import successfully');

        Excel::create($filename, function ($excel) use ($task_main, $task_data, $tran_log, $use_job, $time, $time_work) {
            $excel->sheet('sheet1', function ($sheet) use ($task_main, $task_data, $tran_log, $use_job, $time, $time_work) {
                $sheet->loadView('export.sum_task')->with('task_main', $task_main)->with('task_data', $task_data)
                ->with('tran_log', $tran_log)->with('use_job', $use_job)->with('time', $time)->with('time_work', $time_work);
            });
        })->export('xlsx');
    }

}
