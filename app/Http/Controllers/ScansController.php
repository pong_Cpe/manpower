<?php

namespace App\Http\Controllers;

use App\ScanLog;
use App\Staff;
use Illuminate\Http\Request;

class ScansController extends Controller
{
    public function index(){
        return view('scans.create');
    }

    public function savedata(Request $request){
        $requestData = $request->all();

        $chk = Staff::where('lacoid', $requestData['user_no'])->first();
        $mssg = '';
        if(!empty($chk)){
            $requestData['mechine_id'] = "Station:scan";
            $requestData['log_id'] = "-";
            $requestData['status_no'] = "1";
            $requestData['log_time'] = date("Y-m-d H:i:s");
            ScanLog::create($requestData);
            $mssg = ' ' . $chk->name . ' Scan added!';
        }else{
            $mssg = 'User ' . $requestData['user_no'] . ' incorrect id ';
        }

        

        return redirect('scans/index')->with('flash_message', $mssg);
    }
}
