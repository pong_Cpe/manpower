<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;

use App\QcStaff;
use App\MainDep;
use App\Shift;
use Illuminate\Http\Request;

class QcStaffsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $qcstaffs = QcStaff::latest()->paginate($perPage);
        } else {
            $qcstaffs = QcStaff::latest()->paginate($perPage);
        }

        return view('qc-staffs.index', compact('qcstaffs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('qc-staffs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        QcStaff::create($requestData);

        return redirect('qc-staffs')->with('flash_message', 'QcStaff added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $qcstaff = QcStaff::findOrFail($id);

        return view('qc-staffs.show', compact('qcstaff'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $qcstaff = QcStaff::findOrFail($id);

        return view('qc-staffs.edit', compact('qcstaff'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $qcstaff = QcStaff::findOrFail($id);
        $qcstaff->update($requestData);

        return redirect('qc-staffs')->with('flash_message', 'QcStaff updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        QcStaff::destroy($id);

        return redirect('qc-staffs')->with('flash_message', 'QcStaff deleted!');
    }

    public function upload()
    {
        return view('qc-staffs.upload');
    }

    public function uploadAction(Request $request)
    {

        $maparray = array(
            'B' => '2',
            'C' => '3'
        );

        $typestaff = array(
            'พนักงานปฎิบัติการรายวัน' => '20'
        );

        $departmentlistRaw = MainDep::pluck('name', 'id');
        foreach ($departmentlistRaw as $key => $value) {
            $departmentlist[$value] = $key;
        }
        $shiftlistRaw = Shift::pluck('name', 'id');
        foreach ($shiftlistRaw as $key => $value) {
            $shiftlist[$value] = $key;
        }

        if ($request->hasFile('uploadfile')) {
            // var_dump($requestData['uploadfile']);

            $filename = "fileName_" . time() . '_' . $request->file('uploadfile')->getClientOriginalName();

            //echo $filename;

            $uploadfilepath = $request->file('uploadfile')->storeAs('public/excel', $filename);

            // echo $uploadfilepath;

            $realPathFile =  Storage::disk('public')->path('excel');


            $realfile = $realPathFile . "\\" . $filename;

            Excel::load($realfile, function ($reader) use ($departmentlist, $shiftlist, $maparray, $typestaff) {

                $reader->each(function ($row) use ($departmentlist, $shiftlist, $maparray, $typestaff) {

                    //var_dump($row);
                    //if (isset($typestaff[$row->code])) {
                       // if (isset($maparray[$row->dep])) {
                            if (!empty($row->code)) {

                                $tmp = array();


                                $tmp['lacocode'] = substr(trim($row->code), -7);
                                $tmp['name'] = trim($row->name);
                                $tmp['type'] = $row->type;
                                $tmp['pos'] = trim($row->pos);
                                $tmp['pos_code'] = trim($row->poscode);
                                $tmp['status'] = 'Active';

                                $tmp['shift_id'] = null;
                                if(isset($maparray[trim($row->shift)])){
                                    $tmp['shift_id'] = $maparray[trim($row->shift)];
                                }

                               // var_dump($tmp);

                                $chk = QcStaff::where('lacocode', substr(trim($row->code), -7))->first();

                                if (!empty($chk)) {
                                    $chk->update($tmp);
                                } else {
                                    QcStaff::create($tmp);
                                }
                            }
                     //   }
                   // }
                });
            });
        }



        return redirect('qc-staffs')->with('flash_message', 'Staff added!');
    }
}
