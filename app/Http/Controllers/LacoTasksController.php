<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\LacoTask;
use App\LacoTaskDate;
use App\LacoTaskJob;
use App\LacoTaskPos;
use Illuminate\Http\Request;

class LacoTasksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $lacotasks = LacoTask::latest()->paginate($perPage);
        } else {
            $lacotasks = LacoTask::latest()->paginate($perPage);
        }

        return view('laco-tasks.index', compact('lacotasks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('laco-tasks.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();


        if ($request->hasFile('map_file')) {
            $image = $request->file('map_file');
            $name = md5($image->getClientOriginalName() . time()) . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('images/maps');
            $image->move($destinationPath, $name);

            $requestData['map'] = 'images/maps/' . $name;
        }

        LacoTask::create($requestData);

        return redirect('laco-tasks')->with('flash_message', ' added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $lacotask = LacoTask::findOrFail($id);

        $taskjobArr = array();
        foreach ($lacotask->taskpos()->get() as $taskposObj) {
            $taskjobArr[$taskposObj->lacojob->name][] = $taskposObj;
        }

        $caneditflag = true;
        $chk = LacoTaskDate::where('laco_task_id',$id)->count();
        if($chk > 0){
            $caneditflag = false;
        }

        return view('laco-tasks.show', compact('lacotask', 'taskjobArr', 'caneditflag'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $lacotask = LacoTask::findOrFail($id);

        return view('laco-tasks.edit', compact('lacotask'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();

        if ($request->hasFile('map_file')) {
            $image = $request->file('map_file');
            $name = md5($image->getClientOriginalName() . time()) . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('images/maps');
            $image->move($destinationPath, $name);

            $requestData['map'] = 'images/maps/' . $name;
        }

        $lacotask = LacoTask::findOrFail($id);
        $lacotask->update($requestData);

        return redirect('laco-tasks')->with('flash_message', ' updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        LacoTask::destroy($id);

        return redirect('laco-tasks')->with('flash_message', ' deleted!');
    }

    public function generateJobs($id){
        $lacotask = LacoTask::findOrFail($id);
        $lacotaskjoblist = LacoTaskJob::pluck('name','id');
        return view('laco-tasks.generate', compact('lacotask', 'lacotaskjoblist'));
    }

    private function _updatestaffplan($id){
        $lacotask = LacoTask::findOrFail($id);

        $lacotask->plan_staff =  $lacotask->taskpos()->count();

        $lacotask->update();
        
    }

    public function generateJobsAction(Request $request, $id)
    {

        $requestData = $request->all();
        $lacotaskjob = LacoTaskJob::findOrFail($requestData['laco_task_job_id']);

        if(!empty($lacotaskjob)){

            $chk = LacoTaskPos::where('laco_task_id',$id)->where('laco_task_job_id', $requestData['laco_task_job_id'])->count();
        
            if( $chk == 0 ){
                $run_no = 1;
                while ($run_no <= $requestData['job_num']) {
                    $tmp =array();
                    $strposcode = $lacotaskjob->main_code.str_pad($run_no, 4, '0', STR_PAD_LEFT);

                    $tmp['laco_task_id'] = $id;
                    $tmp['laco_task_job_id'] = $requestData['laco_task_job_id'];
                    $tmp['position_code'] = $strposcode; 
                    $tmp['position_x'] = "1";
                    $tmp['position_y'] = "1";

                    LacoTaskPos::create($tmp);

                    $run_no++;
                }
                $this->_updatestaffplan($id);
                return redirect('laco-tasks/' . $id)->with('flash_message', ' Genrate Complete!');
            }else{
                $this->_updatestaffplan($id);
                return redirect('laco-tasks/' . $id)->with('flash_message', ' already Job!');
            }
        }else{
            return redirect('laco-tasks/' . $id)->with('flash_message', ' Incorrect Job!');
        }
        
    }

    public function adddetail($laco_task_id,$laco_task_job_id){

        $lacotaskjob = LacoTaskJob::findOrFail($laco_task_job_id);

        if (!empty($lacotaskjob)) {

            //str_replace("world", "Peter", "Hello world!");
            $lastestObj = LacoTaskPos::where('laco_task_id', $laco_task_id)->where('laco_task_job_id', $laco_task_job_id)->orderBy('position_code','DESC')->first();


            $tmp = array();
            $tmp['laco_task_id'] = $laco_task_id;
            $tmp['laco_task_job_id'] = $laco_task_job_id;
            $tmp['position_x'] = "1";
            $tmp['position_y'] = "1";

            if(!empty($lastestObj) && !empty($lastestObj->position_code)){
                $lastrunno = intval(str_replace($lacotaskjob->main_code, "", $lastestObj->position_code));
                $newruno =  $lacotaskjob->main_code.str_pad($lastrunno + 1, 4, '0', STR_PAD_LEFT);
    
                $tmp['position_code'] = $newruno; 

            }else{
                $newruno =  $lacotaskjob->main_code.str_pad(1, 4, '0', STR_PAD_LEFT);
    
                $tmp['position_code'] = $newruno;
            }

            LacoTaskPos::create($tmp);

            $this->_updatestaffplan($laco_task_id);
            return redirect('laco-tasks/' . $laco_task_id)->with('flash_message', ' Job Pos add!');
        }else{
            return redirect('laco-tasks/' . $laco_task_id)->with('flash_message', ' Job Pos error add!');
        }

    }

    public function detetepost($laco_task_id, $laco_job_pos_id){

        LacoTaskPos::destroy($laco_job_pos_id);
        $this->_updatestaffplan($laco_task_id);
        return redirect('laco-tasks/'.$laco_task_id)->with('flash_message', ' Job Pos deleted!');
    }

    public function editpos($laco_job_pos_id)
    {
        $lacojobpos = LacoTaskPos::findOrFail($laco_job_pos_id);
        return view('laco-tasks.editpos',compact('lacojobpos'));
    }

    public function editposAction(Request $request, $laco_job_pos_id)
    {
        $requestData = $request->all();
        $lacojobpos = LacoTaskPos::findOrFail($laco_job_pos_id);
        $lacojobpos->position_x = $requestData['position_x'];
        $lacojobpos->position_y = $requestData['position_y'];
        $lacojobpos->update();
        return redirect('laco-tasks/' . $lacojobpos->laco_task_id)->with('flash_message', ' Edit Job Pos complete!');
    }

    public function printbarcode($id)
    {
        $lacotask = LacoTask::findOrFail($id);

        $taskjobArr = array();
        foreach ($lacotask->taskpos()->get() as $taskposObj) {
            $taskjobArr[$taskposObj->lacojob->name][] = $taskposObj;
        }

        return view('laco-tasks.printbarcode', compact('lacotask', 'taskjobArr'));
    }
}
