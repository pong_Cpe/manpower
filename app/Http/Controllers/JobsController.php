<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Job;
use App\Department;
use App\JobType;
use Illuminate\Http\Request;

class JobsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $jobs = Job::latest()->paginate($perPage);
        } else {
            $jobs = Job::latest()->paginate($perPage);
        }

        return view('jobs.index', compact('jobs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $departmentlist = Department::pluck('name', 'id');
        $departmentlist->prepend('===== Select =====', '');
        $jobtypelist = JobType::pluck('name', 'id');
        $jobtypelist->prepend('===== Select =====', '');
        return view('jobs.create',compact('departmentlist', 'jobtypelist'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        Job::create($requestData);

        return redirect('jobs')->with('flash_message', 'Job added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $job = Job::findOrFail($id);

        return view('jobs.show', compact('job'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $job = Job::findOrFail($id);

        $departmentlist = Department::pluck('name', 'id');
        $departmentlist->prepend('===== Select =====', '');
        $jobtypelist = JobType::pluck('name', 'id');
        $jobtypelist->prepend('===== Select =====', '');

        return view('jobs.edit', compact('job', 'departmentlist', 'jobtypelist'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $job = Job::findOrFail($id);
        $job->update($requestData);

        return redirect('jobs')->with('flash_message', 'Job updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Job::destroy($id);

        return redirect('jobs')->with('flash_message', 'Job deleted!');
    }
}
