<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\LacoTaskJob;
use Illuminate\Http\Request;

class LacoTaskJobsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $lacotaskjobs = LacoTaskJob::latest()->paginate($perPage);
        } else {
            $lacotaskjobs = LacoTaskJob::latest()->paginate($perPage);
        }

        return view('laco-task-jobs.index', compact('lacotaskjobs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('laco-task-jobs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();

        LacoTaskJob::create($requestData);

        return redirect('laco-task-jobs')->with('flash_message', ' added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $lacotaskjob = LacoTaskJob::findOrFail($id);

        return view('laco-task-jobs.show', compact('lacotaskjob'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $lacotaskjob = LacoTaskJob::findOrFail($id);

        return view('laco-task-jobs.edit', compact('lacotaskjob'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();

        $lacotaskjob = LacoTaskJob::findOrFail($id);
        $lacotaskjob->update($requestData);

        return redirect('laco-task-jobs')->with('flash_message', ' updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        LacoTaskJob::destroy($id);

        return redirect('laco-task-jobs')->with('flash_message', ' deleted!');
    }
}
