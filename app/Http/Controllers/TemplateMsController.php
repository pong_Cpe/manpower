<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\TemplateM;
use App\TemplateD;
use App\Department;
use App\Job;

use Illuminate\Http\Request;

class TemplateMsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {

        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $templatems = TemplateM::latest()->paginate($perPage);
        } else {
            $templatems = TemplateM::latest()->paginate($perPage);
        }

        return view('templatems.index', compact('templatems'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $departmentlist = Department::pluck('name', 'id');
        $departmentlist->prepend('===== Select =====', '');
        return view('templatems.create',compact('departmentlist'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        TemplateM::create($requestData);

        return redirect('templatems')->with('flash_message', 'TemplateM added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $templatem = TemplateM::findOrFail($id);

        return view('templatems.show', compact('templatem'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $departmentlist = Department::pluck('name', 'id');
        $departmentlist->prepend('===== Select =====', '');
        $templatem = TemplateM::findOrFail($id);

        return view('templatems.edit', compact('templatem', 'departmentlist'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $templatem = TemplateM::findOrFail($id);
        $templatem->update($requestData);

        return redirect('templatems')->with('flash_message', 'TemplateM updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $templatem = TemplateM::findOrFail($id);

        foreach ($templatem->templated as $subitem) {
            TemplateD::destroy($subitem->id);
        }

        TemplateM::destroy($id);

        return redirect('templatems')->with('flash_message', 'TemplateM deleted!');
    }

    public function createdetail($template_m_id){

        $templatem = TemplateM::findOrFail($template_m_id);

        $jobrw = Job::where('department_id', $templatem->department_id)->orWhere('job_type_id',3)->get();
        $joblist = array();
        foreach ($jobrw as $jobobj) {
            $joblist[$jobobj->id] = $jobobj->name;
        }

        return view('templatems.createdetail', compact('templatem', 'joblist'));
    }

    public function createdetailAction(Request $request, $template_m_id)
    {
        $requestData = $request->all();
        $check = TemplateD::where('template_m_id', $template_m_id)->where('job_id', $requestData['job_id'])->count();
        if($check==0){
            $tmpTemplateD = array();
            $tmpTemplateD['template_m_id'] = $template_m_id;
            $tmpTemplateD['job_id'] = $requestData['job_id'];
            $tmpTemplateD['default_number'] = $requestData['default_number'];

            TemplateD::create($tmpTemplateD);
        }

        return redirect('templatems/'. $template_m_id)->with('flash_message', 'TemplateD added!');
    }

    public function deletedetail($template_d_id){
        $templatemId = TemplateD::findOrFail($template_d_id)->template_m_id;

        TemplateD::destroy($template_d_id);

        return redirect('templatems/' . $templatemId)->with('flash_message', 'TemplateD Delete!');
    }

    public function changeSeq($fromId,$toId){

    }

    public function setJob($id)
    {
        $templatem = TemplateM::findOrFail($id);
        //$jobslist = Job::orderBy('department_id')->get();
        $jobslist = Job::where('department_id', $templatem->department_id)->orWhere('job_type_id', 3)->orderBy('department_id')->get();

        $jobvalList = array();
        foreach ($templatem->templated as $templatedObj) {
            $jobvalList[$templatedObj->job_id] = $templatedObj;
        }

        return view('templatems.setjob', compact('templatem', 'jobslist', 'jobvalList'));
    }

    public function setJobAction(Request $request, $id)
    {
        $requestData = $request->all();
        $templatem = TemplateM::findOrFail($id);
        $jobslist = Job::where('department_id', $templatem->department_id)->orWhere('job_type_id', 3)->orderBy('department_id')->pluck('name', 'id');

        foreach ($jobslist as $key => $value) {
            if (isset($requestData['job' . $key]) && $requestData['job' . $key] > 0) {
                $checkAndSave = TemplateD::where('template_m_id', $id)
                    ->where('job_id', $key)
                    ->first();
                if (empty($checkAndSave)) {
                    $tmp = array();
                    $tmp['template_m_id'] = $id;
                    $tmp['job_id'] = $key;
                    $tmp['default_number'] = $requestData['job' . $key];

                    TemplateD::create($tmp);
                } else {
                    $checkAndSave->default_number = $requestData['job' . $key];
                    $checkAndSave->update();
                }
            }
        }

        return redirect('templatems/' . $id)->with('flash_message', ' added!');
    }
}
