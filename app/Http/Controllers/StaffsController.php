<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;

use App\Staff;
use App\MainDep;
use App\Exp;
use App\Job;
use App\Department;
use App\Shift;
use App\ProcessLog;
use Illuminate\Http\Request;

class StaffsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $departmentid = $request->get('departmentid');
        $perPage = 25;

        $departmentlist = MainDep::pluck('name', 'id');

        $staffObj = new Staff();

        if (!empty($departmentid)) {
            $staffObj = $staffObj->where('department_id', $departmentid);
        } 


        if (!empty($keyword)) {
            $staffObj = $staffObj->where('lacoid','like','%'.trim($keyword). '%')->Orwhere('name','like','%'.trim($keyword). '%');
        } 
        
        $staffs = $staffObj->orderBy('lacoid')->paginate($perPage);
        

        return view('staffs.index', compact('staffs', 'departmentlist'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $departmentlist = MainDep::pluck('name', 'id');
        $shiftlist = Shift::pluck('name', 'id');
        return view('staffs.create', compact('departmentlist', 'shiftlist'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        Staff::create($requestData);

        return redirect('staffs')->with('flash_message', 'Staff added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $staff = Staff::findOrFail($id);

        return view('staffs.show', compact('staff'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $staff = Staff::findOrFail($id);
        $departmentlist = MainDep::pluck('name', 'id');
        $shiftlist = Shift::pluck('name', 'id');

        return view('staffs.edit', compact('staff', 'departmentlist', 'shiftlist'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $staff = Staff::findOrFail($id);
        $staff->update($requestData);

        return redirect('staffs')->with('flash_message', 'Staff updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Staff::destroy($id);

        return redirect('staffs')->with('flash_message', 'Staff deleted!');
    }

    public function import()
    {
        return view('staffs.import');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function importAction(Request $request)
    {

        $departmentlistRaw = MainDep::pluck('name', 'id');
        foreach ($departmentlistRaw as $key => $value) {
            $departmentlist[$value] = $key;
        }
        $shiftlistRaw = Shift::pluck('name', 'id');
        foreach ($shiftlistRaw as $key => $value) {
            $shiftlist[$value] = $key;
        }

        if ($request->hasFile('uploadfile')) {
            // var_dump($requestData['uploadfile']);

            $filename = "fileName_" . time() . '_' . $request->file('uploadfile')->getClientOriginalName();

            //echo $filename;

            $uploadfilepath = $request->file('uploadfile')->storeAs('public/excel', $filename);

            // echo $uploadfilepath;

            $realPathFile =  Storage::disk('public')->path('excel');


            $realfile = $realPathFile . "\\" . $filename;

            Excel::load($realfile, function ($reader) use ($departmentlist, $shiftlist ){

                $reader->each(function ($row) use ($departmentlist, $shiftlist){


                    if(!empty($row->lacocode)){

                        $tmp = array();

                        $tmp['lacoid'] = trim($row->lacocode);
                        $tmp['name'] = trim($row->name);
                        $tmp['department_id'] = $departmentlist[trim($row->dep)];
                        $tmp['shift_id'] = $shiftlist[trim($row->shift)];
                        $tmp['status'] = 'Active';

                        $chk = Staff::where('lacoid', trim($row->lacocode))->first();

                        if (!empty($chk)) {
                            $chk->update($tmp);
                        } else {
                            Staff::create($tmp);
                        }
                    }
                });
            });

        }



        return redirect('staffs')->with('flash_message', 'Staff added!');
    }

    public function setExp($id){
        $staff = Staff::findOrFail($id);
        $departmentlist = Department::where('main_dep_id', $staff->department_id)->pluck('id', 'id');
        $departmentlistname = Department::where('main_dep_id', $staff->department_id)->pluck('name', 'id');
        $jobslist = Job::whereIn('department_id', $departmentlist)->orderBy('department_id')->get();
        $jobslistDep = array();
        foreach ($jobslist as $jobsObj) {
            $jobslistDep[$jobsObj->department_id][] = $jobsObj;
        }


        $expvalList = array();
        foreach ($staff->exp as $expObj) {
            $expvalList[$expObj->job_id] = $expObj;
        }

        return view('staffs.setexp', compact('staff', 'jobslist', 'expvalList', 'jobslistDep', 'departmentlistname'));
    }

    public function setExpAction(Request $request, $id){
        $requestData = $request->all();
        $staff = Staff::findOrFail($id);
        $jobslist = Job::pluck('name', 'id');

        foreach ($jobslist as $key => $value) {
            if (isset($requestData['exp' . $key]) && $requestData['exp' . $key] > 0) {
                $checkAndSave = Exp::where('staff_id', $id)
                    ->where('job_id', $key)
                    ->first();
                if (empty($checkAndSave)) {
                    $tmp = array();
                    $tmp['staff_id'] = $id;
                    $tmp['job_id'] = $key;
                    $tmp['priority'] = $requestData['exp' . $key];

                    Exp::create($tmp);
                } else {
                    $checkAndSave->priority = $requestData['exp' . $key];
                    $checkAndSave->update();
                }
            }else{
                
                $checkAndSave = Exp::where('staff_id', $id)
                    ->where('job_id', $key)
                    ->first();
                if (!empty($checkAndSave)) {
                    Exp::destroy($checkAndSave->id);
                }
            }
        }

        return redirect('staffs/')->with('flash_message', ' added!');
    }

    public function importExp(){
        return view('staffs.import_exp');
    }

    public function importExpAction(Request $request){
        if ($request->hasFile('uploadfile')) {
            $filename = "fileName_exp_" . time() . '_' . $request->file('uploadfile')->getClientOriginalName();

            //echo $filename;

            $uploadfilepath = $request->file('uploadfile')->storeAs('public/excel', $filename);

            // echo $uploadfilepath;

            $realPathFile =  Storage::disk('public')->path('excel');


            $realfile = $realPathFile . "\\" . $filename;

            Excel::load($realfile, function ($reader) {

                $firstRow = $reader->first();

                $mapping = array();

                foreach ($firstRow as $key => $value) {
                    if($key == "code" or $key == "name"){

                    }else{
                        $mapping[] = $key;
                    }
                    
                }

                var_dump($mapping);

                $reader->each(function ($row) use ($mapping) {

                    foreach ($mapping as $mapp) {
                        if($row->$mapp != null){
                            echo $row->code." ". $row->name ." Job id : ". $mapp." - ". $row->$mapp."<br/>";

                            $staffObj = Staff::where('lacoid', $row->code)->first();

                            if(!empty($staffObj)){

                                $staffid = $staffObj->id;

                            echo "Staff ID : ". $staffid."<br/>";

                            $chkexp = Exp::where('staff_id', $staffid)->where('job_id', $mapp)->first();

                            if(!empty($staffid)){
                                if (empty($chkexp)) {
                                    $tmpexp = array();
                                    $tmpexp['staff_id'] = $staffid;
                                    $tmpexp['job_id'] = $mapp;
                                    $tmpexp['priority'] = $row->$mapp;
                                    Exp::create($tmpexp);

                                } else {
                                    $chkexp->priority = $row->$mapp;
                                    $chkexp->update();
                                }
                            }
                        }
                        } 
                    }

                });
            });
        }
    }

    public function cometowork($date,$shiftid,$maindepid){
        $stafflist = Staff::where('status', 'Active')
        ->where('department_id', $maindepid)
        ->where('shift_id', $shiftid)
        ->pluck('id');

        $processLogList = ProcessLog::where('log_date', $date)
      //  ->where('status','Absent')
        ->whereIn('staff_id',$stafflist)
        ->get();

        $shiftObj = Shift::findOrFail($shiftid);
        $maindepObj = MainDep::findOrFail($maindepid);

        return view('staffs.cometowork', compact('processLogList','date','maindepObj', 'shiftObj'));
    }

    public function manualcome($processlogid,$date,$shiftid,$depid){
        $processlogobj = ProcessLog::findOrFail($processlogid);

        $processlogobj->status = 'Active';
        $processlogobj->note = 'Manual';

        $processlogobj->update();


        return redirect('/staffs/cometowork/'. $date.'/'. $shiftid . '/' . $depid)->with('flash_message', 'PlanD updated!');
    }

    public function uploadHr()
    {
        return view('staffs.upload_hr');
    }

    public function uploadHrAction(Request $request)
    {

        $maparray = array(
            'ผลิตแช่แข็ง' => 'PF',
            'คัดบรรจุ' => 'PK',
            'ผลิตเพสต์' => 'PST'
        );

        $typestaff = array(
            'พนักงานปฎิบัติการรายวัน' => '20'
        );

        $departmentlistRaw = MainDep::pluck('name', 'id');
        foreach ($departmentlistRaw as $key => $value) {
            $departmentlist[$value] = $key;
        }
        $shiftlistRaw = Shift::pluck('name', 'id');
        foreach ($shiftlistRaw as $key => $value) {
            $shiftlist[$value] = $key;
        }

        if ($request->hasFile('uploadfile')) {
            // var_dump($requestData['uploadfile']);

            $filename = "fileName_" . time() . '_' . $request->file('uploadfile')->getClientOriginalName();

            //echo $filename;

            $uploadfilepath = $request->file('uploadfile')->storeAs('public/excel', $filename);

            // echo $uploadfilepath;

            $realPathFile =  Storage::disk('public')->path('excel');


            $realfile = $realPathFile . "\\" . $filename;

            Excel::load($realfile, function ($reader) use ($departmentlist, $shiftlist, $maparray, $typestaff) {

                $reader->each(function ($row) use ($departmentlist, $shiftlist, $maparray, $typestaff) {

                    var_dump($row);
                    if(isset($typestaff[$row->position])){
                        if(isset($maparray[$row->dep])){
                            if (!empty($row->lacoid)) {

                                $tmp = array();
                                

                                $tmp['lacoid'] = substr($typestaff[$row->position].trim($row->lacoid),-7);
                                $tmp['name'] = trim($row->init)." ". trim($row->fname) . " " . trim($row->lname);
                                $tmp['department_id'] = $departmentlist[$maparray[$row->dep]];
                                $tmp['shift_id'] = $shiftlist[trim($row->shift)];
                                $tmp['status'] = 'Active';

                                $chk = Staff::where('lacoid', trim($tmp['lacoid']))->first();

                                if (!empty($chk)) {
                                    $chk->update($tmp);
                                } else {
                                    Staff::create($tmp);
                                }
                                
                            }
                        }
                    }
                });
            });
        }



        return redirect('staffs')->with('flash_message', 'Staff added!');
    }

}
