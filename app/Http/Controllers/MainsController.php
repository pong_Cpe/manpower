<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MainsController extends Controller
{
    public function index(){
        return view('mains.index');
    }

    public function verion2()
    {
        return view('mains.ver2');
    }

}
