<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\LacoStaff;
use App\MainDep;
use App\Shift;
use Illuminate\Http\Request;

use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;

class LacoStaffsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $lacostaffs = LacoStaff::where('name','like','%'.$keyword.'%')->orWhere('lacoid', 'like', '%' . $keyword . '%')->latest()->paginate($perPage);
        } else {
            $lacostaffs = LacoStaff::latest()->paginate($perPage);
        }

        return view('laco-staffs.index', compact('lacostaffs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $departmentlist = MainDep::pluck('name', 'id');
        $shiftlist = Shift::pluck('name', 'id');
        return view('laco-staffs.create',compact('departmentlist', 'shiftlist'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();

        $chk = LacoStaff::where('lacoid', trim($requestData['lacoid']))->first();

        if (!empty($chk)) {
            return redirect('laco-staffs')->with('flash_message', ' รหัสซ้ำ!');
        } else {
            LacoStaff::create($requestData);
            return redirect('laco-staffs')->with('flash_message', ' Success!');
        }
                            

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $lacostaff = LacoStaff::findOrFail($id);

        return view('laco-staffs.show', compact('lacostaff'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $departmentlist = MainDep::pluck('name', 'id');
        $shiftlist = Shift::pluck('name', 'id');

        $lacostaff = LacoStaff::findOrFail($id);

        return view('laco-staffs.edit', compact('lacostaff','departmentlist', 'shiftlist'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();

        $lacostaff = LacoStaff::findOrFail($id);

        if($requestData['lacoid'] <> $lacostaff->lacoid){
            $chk = LacoStaff::where('lacoid', trim($requestData['lacoid']))->first();

            if (!empty($chk)) {
                return redirect('laco-staffs')->with('flash_message', ' รหัสซ้ำ!');
            } else {
                $lacostaff->update($requestData);
                return redirect('laco-staffs')->with('flash_message', ' Success!');
            }
        }else{
            $lacostaff->update($requestData);

            return redirect('laco-staffs')->with('flash_message', ' updated!');
        }

        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        LacoStaff::destroy($id);

        return redirect('laco-staffs')->with('flash_message', ' deleted!');
    }

    public function upload()
    {
        return view('laco-staffs.upload');
    }

    public function uploadAction(Request $request)
    {
        $defaultshift = 'B';
        $defaultdep = 'TEST';
        $departmentlistRaw = MainDep::pluck('name', 'id');
        foreach ($departmentlistRaw as $key => $value) {
            $departmentlist[$value] = $key;
        }
        $shiftlistRaw = Shift::pluck('name', 'id');
        foreach ($shiftlistRaw as $key => $value) {
            $shiftlist[$value] = $key;
        }

        if ($request->hasFile('uploadfile')) {
            // var_dump($requestData['uploadfile']);

            $filename = "fileName_" . time() . '_' . $request->file('uploadfile')->getClientOriginalName();

            //echo $filename;

            $uploadfilepath = $request->file('uploadfile')->storeAs('public/excel', $filename);

            // echo $uploadfilepath;

            $realPathFile =  Storage::disk('public')->path('excel');


            $realfile = $realPathFile . "\\" . $filename;

            Excel::load($realfile, function ($reader) use ($departmentlist, $shiftlist, $defaultshift, $defaultdep) {

                $reader->each(function ($row) use ($departmentlist,$shiftlist, $defaultshift, $defaultdep) {

                    //var_dump($row);
                    if (!empty($row->lacoid) && !empty($row->name) ) {

                        $dep_n_shift = explode(':', $row->dep);
                        if(sizeof($dep_n_shift) == 2){


                            $tmp = array();                            

                            $tmp['lacoid'] = $row->lacoid;
                            $tmp['name'] = $row->name;
                            $tmp['main_dep_id'] = $departmentlist[trim($dep_n_shift[0])];
                            $tmp['shift_id'] = $shiftlist[trim($dep_n_shift[1])];
                            $tmp['status'] = 'Active';

                            var_dump($tmp);
                            
                            $chk = LacoStaff::where('lacoid', trim($tmp['lacoid']))->first();

                            if (!empty($chk)) {
                                $chk->update($tmp);
                            } else {
                                LacoStaff::create($tmp);
                            }
                            
                        }else{
                            if(!empty($row->dep)){
                                $tmp = array();

                                $tmp['lacoid'] = $row->lacoid;
                                $tmp['name'] = $row->name;
                                $tmp['main_dep_id'] = $departmentlist[trim($row->dep)];
                                $tmp['shift_id'] = $shiftlist[trim($defaultshift)];
                                $tmp['status'] = 'Active';

                                var_dump($tmp);

                                $chk = LacoStaff::where('lacoid', trim($tmp['lacoid']))->first();

                                if (!empty($chk)) {
                                    $chk->update($tmp);
                                } else {
                                    LacoStaff::create($tmp);
                                }
                            }else{
                                $tmp = array();

                                $tmp['lacoid'] = $row->lacoid;
                                $tmp['name'] = $row->name;
                                $tmp['main_dep_id'] = $departmentlist[trim($defaultdep)];
                                $tmp['shift_id'] = $shiftlist[trim($defaultshift)];
                                $tmp['status'] = 'Active';

                                var_dump($tmp);

                                $chk = LacoStaff::where('lacoid', trim($tmp['lacoid']))->first();

                                if (!empty($chk)) {
                                    $chk->update($tmp);
                                } else {
                                    LacoStaff::create($tmp);
                                }
                            }
                        }
                    }
                });
            });
        }



       return redirect('laco-staffs')->with('flash_message', 'Staff added!');
    }
}
