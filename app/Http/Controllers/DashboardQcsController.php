<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\QcProcessLog;
use App\Shift;

use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class DashboardQcsController extends Controller
{
    public function index(Request $request){

        $date = $request->get('date');
        $perPage = 25;

        if(!empty($date)){
            $qcprocesslogs = QcProcessLog::where('status', 'Active')
                ->where('log_date',$date)
                ->groupBy('log_date')
                ->select('log_date', DB::raw('count(id) as numberactive'))
                ->orderBy('log_date', 'DESC')
                ->paginate($perPage);
        }else{
            $qcprocesslogs = QcProcessLog::where('status','Active')
                ->groupBy('log_date')
                ->select('log_date',DB::raw('count(id) as numberactive'))
                ->orderBy('log_date','DESC')
                ->paginate($perPage);
        }

        return view('dashboard-qcs.index', compact('qcprocesslogs'));
    }

    public function view($date){

        $typearr = array(
            "D" => "รายวัน",
            "T" => "ชั่วคราว",
            "M" => "รายเดือน",
        );

        $shiftlist = Shift::pluck('name','id');
        $shiftlist->prepend('ALL', '6');


        $qcprocesslogsRaw = DB::table('qc_process_logs')
            ->leftJoin('qc_staffs', 'qc_staffs.id', 'qc_process_logs.staff_id')
            ->where('qc_process_logs.log_date', $date)
            ->groupBy('qc_process_logs.log_date',  'qc_staffs.shift_id', 'qc_staffs.pos_code',  'qc_process_logs.status')
            ->select('qc_process_logs.log_date',  DB::raw('isnull(qc_staffs.shift_id,6) as postxt'), 'qc_staffs.pos_code',  'qc_process_logs.status' , DB::raw('count(qc_process_logs.id) as numberactive'))
            ->orderBy('qc_staffs.shift_id','ASC')
            ->get();

        foreach ($qcprocesslogsRaw as $qcprocesslogObj) {
            $qcprocesslogs[$qcprocesslogObj->postxt][$qcprocesslogObj->pos_code][$qcprocesslogObj->status] = $qcprocesslogObj->numberactive;
        }

        return view('dashboard-qcs.view', compact('qcprocesslogs', 'typearr','date', 'shiftlist'));
    }

    public function viewdetail($pos_code,$pos_txt,$date){
        $typearr = array(
            "D" => "รายวัน",
            "T" => "ชั่วคราว",
            "M" => "รายเดือน",
        );


        $qcprocesslogsRaw = DB::table('qc_process_logs')
            ->leftJoin('qc_staffs', 'qc_staffs.id', 'qc_process_logs.staff_id')
            ->where('qc_process_logs.log_date', $date)
            ->where('qc_staffs.shift_id', $pos_txt)
            ->where('qc_staffs.pos_code', $pos_code)
            ->select('qc_staffs.name', 'qc_staffs.lacocode', 'qc_staffs.type', 'qc_staffs.pos',  'qc_staffs.shift_id', 'qc_staffs.pos_code', 'qc_process_logs.status')
            // ->orderBy('qc_staffs.pos_code', 'ASC')
            //  ->orderBy('qc_staffs.type', 'ASC')
            //  ->orderBy('qc_staffs.status', 'DESC')
            ->get();

        //var_dump($qcprocesslogsRaw);

        return view('dashboard-qcs.viewdetail', compact('qcprocesslogsRaw', 'typearr','date'));
    }

    public function mapview($date,$shiftid){
        $typearr = array(
            "D" => "รายวัน",
            "T" => "ชั่วคราว",
            "M" => "รายเดือน",
        );

        $shiftlist = Shift::pluck('name','id');
        $shiftlist->prepend('ALL', '6');


        $qcprocesslogsRaw = DB::table('qc_process_logs')
            ->leftJoin('qc_staffs', 'qc_staffs.id', 'qc_process_logs.staff_id')
            ->where('qc_process_logs.log_date', $date)
            ->groupBy('qc_process_logs.log_date',  'qc_staffs.shift_id', 'qc_staffs.pos_code',  'qc_process_logs.status')
            ->select('qc_process_logs.log_date',  DB::raw('isnull(qc_staffs.shift_id,6) as postxt'), 'qc_staffs.pos_code',  'qc_process_logs.status' , DB::raw('count(qc_process_logs.id) as numberactive'))
            ->orderBy('qc_staffs.shift_id','ASC')
            ->get();

        foreach ($qcprocesslogsRaw as $qcprocesslogObj) {
            $qcprocesslogs[$qcprocesslogObj->postxt][$qcprocesslogObj->pos_code][$qcprocesslogObj->status] = $qcprocesslogObj->numberactive;
        }

        return view('dashboard-qcs.mapview', compact('qcprocesslogs', 'typearr','date', 'shiftlist', 'shiftid'));
    }
}
