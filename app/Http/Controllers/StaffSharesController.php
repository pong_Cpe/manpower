<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Staff;
use App\StaffShare;
use App\MainDep;
use App\Shift;
use Illuminate\Http\Request;

class StaffSharesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $staffshares = StaffShare::latest()->paginate($perPage);
        } else {
            $staffshares = StaffShare::latest()->paginate($perPage);
        }

        return view('staff-shares.index', compact('staffshares'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('staff-shares.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        StaffShare::create($requestData);

        return redirect('staff-shares/listshare/'. $requestData['staff_id'])->with('flash_message', 'StaffShare added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $staffshare = StaffShare::findOrFail($id);
        $staffObj = Staff::findOrFail($staffshare->staff_id);
        return view('staff-shares.show', compact('staffshare', 'staffObj'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $staffshare = StaffShare::findOrFail($id);
        $staffObj = Staff::findOrFail($staffshare->staff_id);
        $departmentlist = MainDep::where('id', '<>', $staffObj->department_id)->pluck('name', 'id');
        $shiftlist = Shift::pluck('name', 'id');

        return view('staff-shares.edit', compact('staffshare','staffObj', 'departmentlist', 'shiftlist'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $staffshare = StaffShare::findOrFail($id);
        $staffshare->update($requestData);

        return redirect('staff-shares/listshare/'. $staffshare->staff_id)->with('flash_message', 'StaffShare updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $staffshare = StaffShare::findOrFail($id);
        $staffid = $staffshare->staff_id;

        StaffShare::destroy($id);

        return redirect('staff-shares/listshare/'. $staffid)->with('flash_message', 'StaffShare deleted!');
    }

    public function listshare(Request $request,$staff_id)
    {
        $perPage = 25;

        $staffObj = Staff::findOrFail($staff_id);

        $staffshares = StaffShare::where('staff_id',$staff_id)->orderBy('date_share','desc')->paginate($perPage);

        return view('staff-shares.index', compact('staffshares', 'staff_id', 'staffObj'));
    }

    public function add($staff_id)
    {
        $staffObj = Staff::findOrFail($staff_id);
        $departmentlist = MainDep::where('id','<>', $staffObj->department_id)->pluck('name', 'id');
        $shiftlist = Shift::pluck('name', 'id');
        return view('staff-shares.create',compact('departmentlist', 'shiftlist', 'staffObj'));
    }
}
