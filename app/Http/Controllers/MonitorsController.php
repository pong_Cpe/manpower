<?php

namespace App\Http\Controllers;

use App\MainDep;
use Illuminate\Http\Request;
 
use App\Shift;
use App\ProcessLog;
use App\PlanM;

class MonitorsController extends Controller
{
    public function index(Request $request){
    }

    public function main($date, Request $request){

        if($date == 'selected'){
            $date = $request->get('date');
        }

        $shiftlist = Shift::pluck('name', 'id');
        $deplist = MainDep::pluck('name','id');

        $dataRw = ProcessLog::where('log_date',$date)->orderBy('staff_id')->get();
        $planmrw = PlanM::where('plandate',$date)->orderBy('shift_id')->get();

        $data = array();
        foreach ($dataRw as $dataObj) {
            
            $dep = 0;
            if(!empty($dataObj->staff->id)){
                $dep = $dataObj->staff->department_id;
            }

            $data[$dataObj->shift_id][$dep][$dataObj->status][] = $dataObj;
        }

        $planmlist = array();
        foreach($planmrw as $planmobj){
            $planmlist[$planmobj->shift_id][$planmobj->templatem->department->maindep->id][] = $planmobj;
        }

        //print_r($data);

        return view('monitors.main', compact('data', 'deplist', 'shiftlist','date', 'planmlist'));
    }
}
