<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\PlanM;
use App\PlanD;
use App\PlanDUser;
use App\TemplateM;
use App\Job;
use App\Shift;
use App\Exp;
use App\Staff;
use App\Department;
use App\ProcessLog;
use App\ScanLog;
use App\StaffShare;
use ZKLibrary;
use Illuminate\Http\Request;

class PlanMsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $date = $request->get('date');
        $perPage = 25;

        $planmsObj = new PlanM();
        
        if (!empty($keyword)) {
            $planmsObj = $planmsObj->where('name','like','%'. $keyword.'%');
        }

        if (!empty($date)) {
            $planmsObj = $planmsObj->where('plandate',  $date );
        }

        $planms = $planmsObj->orderby('plandate','desc')->paginate($perPage);


        return view('planms.index', compact('planms'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('planms.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();

        $requestData['startdatetime'] = \Carbon\Carbon::parse($requestData['startdatetime'])->format('Y-m-d H:i');
        $requestData['enddatetime'] = \Carbon\Carbon::parse($requestData['enddatetime'])->format('Y-m-d H:i');
        PlanM::create($requestData);

        return redirect('planms')->with('flash_message', 'PlanM added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $planm = PlanM::findOrFail($id);

        return view('planms.show', compact('planm'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $planm = PlanM::findOrFail($id);

        return view('planms.edit', compact('planm'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();

        $requestData['startdatetime'] = \Carbon\Carbon::parse($requestData['startdatetime'])->format('Y-m-d H:i');
        $requestData['enddatetime'] = \Carbon\Carbon::parse($requestData['enddatetime'])->format('Y-m-d H:i');
        
        $planm = PlanM::findOrFail($id);
        $planm->update($requestData);

        return redirect('planms')->with('flash_message', 'PlanM updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {

        $planm = PlanM::findOrFail($id);

        foreach ($planm->pland as $subitem) {
            PlanD::destroy($subitem->id);
        }


        PlanM::destroy($id);

        return redirect('planms')->with('flash_message', 'PlanM deleted!');
    }

    public function createdetail($plan_m_id)
    {

        $planm = PlanM::findOrFail($plan_m_id);

        $jobrw = Job::where('department_id', $planm->templatem->department_id)->orWhere('job_type_id', 3)->get();
        $joblist = array();
        foreach ($jobrw as $jobobj) {
            $joblist[$jobobj->id] = $jobobj->name;
        }

        return view('planms.createdetail', compact('planm', 'joblist'));
    }

    public function createdetailAction(Request $request, $plan_m_id)
    {
        $requestData = $request->all();
        $check = PlanD::where('plan_m_id', $plan_m_id)->where('job_id', $requestData['job_id'])->count();
        if ($check == 0) {
            $tmpPlanD = array();
            $tmpPlanD['plan_m_id'] = $plan_m_id;
            $tmpPlanD['job_id'] = $requestData['job_id'];
            $tmpPlanD['plan_number'] = $requestData['plan_number'];

            PlanD::create($tmpPlanD);
        }

        return redirect('planms/' . $plan_m_id)->with('flash_message', 'TemplateD added!');
    }

    public function deletedetail($template_d_id)
    {
        $templatemId = TemplateD::findOrFail($template_d_id)->template_m_id;

        TemplateD::destroy($template_d_id);

        return redirect('planms/' . $templatemId)->with('flash_message', 'TemplateD Delete!');
    }

    public function template2Plan($template_m_id){
        $templatem = TemplateM::findOrFail($template_m_id);
        $shiftlist = Shift::pluck('name','id');

        return view('planms.template2plan', compact('templatem', 'shiftlist'));
    }

    public function template2PlanAction(Request $request, $template_m_id){
        $requestData = $request->all();

        $templatem = TemplateM::findOrFail($template_m_id);

        $tmpPlan = array();
        $tmpPlan['template_m_id'] = $template_m_id;
        $tmpPlan['name'] = $requestData['name'];
        $tmpPlan['shift_id'] = $requestData['shift_id'];
        $tmpPlan['plandate'] = $requestData['plandate'];
        
        //$tmpPlan['startdatetime'] = \Carbon\Carbon::parse($requestData['startdatetime'])->format('Y-m-d H:i');
        //$tmpPlan['enddatetime'] = \Carbon\Carbon::parse($requestData['enddatetime'])->format('Y-m-d H:i');
        $tmpPlan['desc'] = $requestData['desc'];
        $tmpPlan['status'] = 'Active'; 

        $planMId = PlanM::create($tmpPlan)->id;

        foreach ($templatem->templated as $templatedobj) {
            $tmppland = array();
            $tmppland['plan_m_id'] = $planMId;
            $tmppland['job_id'] = $templatedobj->job_id;
            $tmppland['plan_number'] = $templatedobj->default_number;

            PlanD::create($tmppland);
        }

        return redirect('planms')->with('flash_message', 'Clone Template 2 Plan complete!');
    }

    public function dashboard($plan_m_id){

        $planm = PlanM::findOrFail($plan_m_id);

        return view('planms.dashboard',compact('planm'));
    }

    public function editdetail($plan_d_id){
        $pland = PlanD::findOrFail($plan_d_id);

        return view('planms.editdetail', compact('pland'));
    }

    public function editdetailAction(Request $request, $plan_d_id)
    {
        
        $requestData = $request->all();

        $pland = PlanD::findOrFail($plan_d_id);

        $pland->update($requestData);

        return redirect('planms/'. $pland->plan_m_id)->with('flash_message', 'PlanD updated!');
    }

    public function clearmap($plan_m_id){
        PlanDUser::where('plan_m_id', $plan_m_id)->delete();

        PlanM::where('id', $plan_m_id)
            ->update(['status' => 'Active']);

        return redirect('planms')->with('flash_message', 'Clone Template 2 Plan complete!');

    }

    public function remap($plan_m_id){
        $planm = PlanM::findOrFail($plan_m_id);

        ini_set('memory_limit', '256M');
        $date = $planm->plandate;

        $shiftobj = $planm->shift;

        $planmsTmp = PlanM::where('plandate', $date)
            ->where('shift_id', $shiftobj->id)->get();
        $planms = PlanM::where('id', $plan_m_id)->get();
        $planmsList = $planmsTmp->pluck('id');

        foreach ($planms as $planmObj) {
            foreach ($planmObj->pland as $plandObj) {
                
                $expList = Exp::where('job_id', $plandObj->job_id)->orderBy('priority')->get();
                $countLoop = 1;
                foreach ($expList as $expObj) {
                    
                    if($expObj->staff->shift_id == $planmObj->shift_id ){
                        
                        $tmpStaffExp = array();
                        $tmpStaffExp['plan_m_id'] = $planmObj->id;
                        $tmpStaffExp['plan_d_id'] = $plandObj->id;
                        $tmpStaffExp['staff_id'] = $expObj->staff_id;

                        $chk = PlanDUser::whereIn('plan_m_id', $planmsList)
                            ->where('staff_id', $expObj->staff_id)
                            ->first();

                        if (empty($chk->id)) {
                            
                            PlanDUser::create($tmpStaffExp);
                            if ($countLoop == $plandObj->plan_number) {
                                break;
                            } else {
                                $countLoop++;
                            }
                        } 
                    }
                    
                }

                if ($countLoop < $plandObj->plan_number) {
                    for ($i = $countLoop; $i <= $plandObj->plan_number; $i++) {
                        $tmpStaffExp = array();
                        $tmpStaffExp['plan_m_id'] = $planmObj->id;
                        $tmpStaffExp['plan_d_id'] = $plandObj->id;
                        $tmpStaffExp['staff_id'] = null;

                        PlanDUser::create($tmpStaffExp);
                    }
                }
            }

            PlanM::where('id', $planmObj->id)
                ->update(['status' => 'Mapped']);
        }
        return redirect('planms')->with('flash_message', 'Clone Template 2 Plan complete!');
    }

    public function remapall($date,$shift)
    {
        ini_set('memory_limit', '256M');

        $shiftobj = Shift::where('name', $shift)->first();

        $planmsTmp = PlanM::where('plandate', $date)
            ->where('shift_id', $shiftobj->id);
        $planms = $planmsTmp->get();
        $planmsList = $planms->pluck('id');


        //echo " Shift ID : " . $shiftobj->id;
      //  echo " Plan ID : ";
       // print_r($planmsList);
       // echo "\n";

        $listAllJob = array();


        foreach ($planms as $planmObj) {
            foreach ($planmObj->pland as $plandObj) {
               // echo $plandObj->job_id . '-' . $plandObj->plan_number . "\n";

                // $countLoop = 1;

                $numberplan = $plandObj->planduser->count();

                //if ($countLoop <= $plandObj->plan_number) {
                for ($i = $numberplan; $i < $plandObj->plan_number; $i++) {
                    $tmpStaffExp = array();
                    $tmpStaffExp['plan_m_id'] = $planmObj->id;
                    $tmpStaffExp['plan_d_id'] = $plandObj->id;
                    $tmpStaffExp['staff_id'] = null;

                    PlanDUser::create($tmpStaffExp);
                }
                //}
            }

            PlanM::where('id', $planmObj->id)
                ->update(['status' => 'Mapped']);
        }


        $planmsTmp = PlanM::where('plandate', $date)
            ->where('shift_id', $shiftobj->id);
        $planms = $planmsTmp->get();
        $planmsList = $planms->pluck('id');

        foreach ($planms as $planmObj) {
            foreach ($planmObj->pland as $plandObj) {
                foreach ($plandObj->planduser as $planduserObj) {
                    $listAllJob[$plandObj->job_id][] = $planduserObj->id;
                }
            }
        }

        //var_dump($listAllJob);

        $stafflist = Staff::where('shift_id', $shiftobj->id)->pluck('id');

        for ($i = 1; $i <= 5; $i++) {
            $expdatas = Exp::whereIn('staff_id', $stafflist)->where('priority', $i)->orderBy('job_id')->get();
            foreach ($expdatas as $expobj) {
                //echo "Exp Level : " . $i . " Staff " . $expobj->staff_id . " Job Id " . $expobj->job_id . "\n";

                if (isset($listAllJob[$expobj->job_id])) {
                    //$this->checkAndSave($expobj->staff_id, $expobj->job_id, $planmsList, $listAllJob[$expobj->job_id]);

                    $chk = PlanDUser::whereIn('plan_m_id', $planmsList)->where('staff_id', $expobj->staff_id)->first();

                    if (empty($chk)) {
                        //echo "Staff Available\n";
                        $planuserobj = PlanDUser::whereIn('plan_m_id', $planmsList)->whereIn('id', $listAllJob[$expobj->job_id])->where('staff_id')->first();
                        if (!empty($planuserobj)) {
                            $planuserobj->staff_id = $expobj->staff_id;
                            $planuserobj->update();
                        //    echo "Job Available\n";
                        } else {
                        //    echo "Job Not Available\n";
                        }
                    } else {
                       // echo "Staff Not Available\n";
                    }
                }
            }
        }

        return redirect('planms')->with('flash_message', 'Clone Template 2 Plan complete!');
    }

    public function assignmanual($plan_d_user_id){
        $planDUserObj = PlanDUser::findOrFail($plan_d_user_id);
        $date = $planDUserObj->planm->plandate;
        $shift_id = $planDUserObj->planm->shift_id;
        $departmentObj = Department::findOrFail($planDUserObj->planm->templatem->department_id);
        $department_id = $departmentObj->main_dep_id;

        $planmsTmp = PlanM::where('plandate', $date)
            ->where('shift_id', $shift_id);
        $planms = $planmsTmp->get();
        $planmsList = $planms->pluck('id');
        
        $staffinused = PlanDUser::whereIn('plan_m_id', $planmsList)->whereNotNull('staff_id')->groupBy('staff_id')->pluck('staff_id');

        $staffAvailable = Staff::where('shift_id', $shift_id)
        ->where('status', 'Active')
        ->where('department_id', $department_id)
        ->whereNotIn('id', $staffinused)
        ->orderBy('name')
        ->get();

        $staffAvailableList = array();

        foreach ($staffAvailable as $staffObj) {
            $tmptxt =  "(". $staffObj->maindep->name.")".$staffObj->name." - ";
            if(isset($staffObj->explist[0])){
                foreach ($staffObj->explist as $expobj) {
                    $tmptxt .=  "[" . $expobj->priority . "] ". $expobj->job->name." ";
                }
            }else{
                $tmptxt .= "No Exp";
            }
            $staffAvailableList[$staffObj->id] = $tmptxt;
        }

        $staffShare = StaffShare::where('date_share',$date)
            ->where('to_main_dep_id', $department_id)
            ->get();

        foreach ($staffShare as $staffShareObj) {
            $tmptxt =  "(" . $staffShareObj->staff->maindep->name . ")" . $staffShareObj->staff->name . " - ";
            if (isset($staffShareObj->staff->explist[0])) {
                foreach ($staffShareObj->staff->explist as $expobj) {
                    $tmptxt .=  "[" . $expobj->priority . "] " . $expobj->job->name . " ";
                }
            } else {
                $tmptxt .= "No Exp";
            }
            $staffAvailableList[$staffShareObj->staff->id] = $tmptxt;
        }

        return view('planms.assignmanual', compact('planDUserObj', 'staffAvailableList'));
    }

    public function assignmanualAction(Request $request, $plan_d_user_id)
    {

        $requestData = $request->all();

        $planduser = PlanDUser::findOrFail($plan_d_user_id);
        if(!empty($planduser->staff_id)){
            $planduser->m_staff_id = $planduser->staff_id;
        }
        
        $planduser->staff_id = $requestData['staff_id'];
        

        $processlog = ProcessLog::where('log_date',$planduser->planm->plandate)->where('staff_id', $planduser->staff_id)->first();

        if($processlog->status = 'Active'){
            $planduser->status = 'Active';
        }

        $planduser->update();

        

        return redirect('planms/dashboard/' . $planduser->plan_m_id)->with('flash_message', 'PlanD updated!');
    }


    public function cometowork($plan_d_user_id){
        $planDUserObj = PlanDUser::findOrFail($plan_d_user_id);
        $planDUserObj->status = 'Active';
        $planDUserObj->update();
        echo $planDUserObj->staff->name ." มาทำงาน";
    }

    public function processdateagain($date,$shift,$redirect= "planms"){
        ini_set('memory_limit', '256M');

        //$date = date('Y-m-d');
        //$shift = $this->argument('shift');

        $shiftObj = Shift::where('name', $shift)->first();

        $stafflist = Staff::where('shift_id', $shiftObj->id)->where('status', 'Active')->get();

        if ($shift == 'B') {
            $startdatetime = $date . " 06:00:00.000";
            $enddatetime = $date . " 08:00:00.000";

            $listusercome = ScanLog::whereBetween('log_time', [$startdatetime, $enddatetime])->pluck('user_no', 'user_no');
        } elseif ($shift == 'C') {
            $startdatetime = $date . " 16:00:00.000";
            $enddatetime = $date . " 18:30:00.000";

            $listusercome = ScanLog::whereBetween('log_time', [$startdatetime, $enddatetime])->pluck('user_no', 'user_no');
        }

        $planmsTmp = PlanM::where('plandate', $date)
            ->where('shift_id', $shiftObj->id);
        $planms = $planmsTmp->get();
        $planmsList = $planms->pluck('id');

        $planduserlist = PlanDUser::whereIn('plan_m_id', $planmsList)->get();

        $planduserarr = array();

        foreach ($planduserlist as $planduserobj) {
            $planduserarr[$planduserobj->staff_id] = $planduserobj->id;
        }


        //print_r($listusercome);

        foreach ($stafflist as $staffObj) {
            $tmp = array();
            $tmp['log_date'] = $date;
            $tmp['shift_id'] = $shiftObj->id;
            $tmp['user_no'] = substr($staffObj->lacoid, -7);


            $mystaff = Staff::where('lacoid', $tmp['user_no'])->where('status', 'Active')->first();




            if (isset($listusercome[substr($staffObj->lacoid, -7)])) {
                $tmp['status'] = 'Active';

                if (!empty($mystaff)) {
                    if (isset($planduserarr[$mystaff->id])) {
                        $planduserobj = PlanDUser::findOrFail($planduserarr[$mystaff->id]);
                        $planduserobj->status = 'Active';
                        $planduserobj->update();
                    }
                }
            } else {
                $tmp['status'] = 'Absent';
            }

            if (!empty($mystaff)) {
                $tmp['staff_id'] = $mystaff->id;

                $chk = ProcessLog::where('log_date', $date)
                    ->where('shift_id', $shiftObj->id)
                    ->where('user_no', substr($staffObj->lacoid, -7))
                    ->first();


                if (empty($chk)) {
                    ProcessLog::create($tmp);
                } else {
                    $chk->update($tmp);
                }
            }

            
        }

        if($redirect == "planms"){
            return redirect('planms')->with('flash_message', 'PlanD updated!');
        }else{
            return redirect('monitors/main/'.$date)->with('flash_message', 'PlanD updated!');
        }

        
    }

    public function getscan($date){
        ini_set('memory_limit', '256M');

        require app_path() . '/tad/zklibrary.php';

        $mechlist = array(
            "4373" => "10.44.65.192",
            "4371" => "10.44.65.190",
            "4370" => "10.44.65.187",
            "4375" => "10.44.65.186",
            "4374" => "10.44.65.189",
            "4376" => "10.44.65.188",
        );


        foreach ($mechlist as $key => $value) {

            $ip = $value;
            $port = $key;

            echo $ip . " : " . $port . "\n";
            $zk = new ZKLibrary();
            //$zk->connect();
            //$zk->getSerialNumber(true);

            $zk->connect($ip, $port);
            $loop = 1;
            foreach ($zk->getAttendance() as $att) {
                $loop++;
               // echo ".";
                $tmp = array();
                $tmp['mechine_id'] = $ip . ":" . $port;
                $tmp['log_id'] = $att[0];
                $tmp['user_no'] = $att[1];
                $tmp['status_no'] = $att[2];
                $tmp['log_time'] = $att[3];


                //print_r($att);
                //echo "\n";

                if ($tmp['user_no'] != "") {

                    $chk = ScanLog::where('user_no', $att[1])->where('log_time', $att[3])->first();

                    if (empty($chk)) {
                        ScanLog::create($tmp);
                    }
                }
            }

            // foreach ($zk->getUser() as $user) {
            //    print_r($user);
            //};

            //print_r($zk);
           // echo $loop;
           // echo "\n";
            $zk->disconnect();
        }
        return redirect('/')->with('flash_message', 'PlanD updated!');
    }

    public function scanlist(){
        $mechlist = array(
            "4373" => "10.44.65.192",
            "4371" => "10.44.65.190",
            "4370" => "10.44.65.187",
            "4375" => "10.44.65.186",
            "4374" => "10.44.65.189",
            "4376" => "10.44.65.188",
        );

        return view('planms.scanlist',compact('mechlist'));
    }

    public function getselectscan($date,$ip,$port){
        ini_set('memory_limit', '256M');

        require app_path() . '/tad/zklibrary.php';

            $txtmessage = $ip . " : " . $port;
            $zk = new ZKLibrary();
            //$zk->connect();
            //$zk->getSerialNumber(true);

            $zk->connect($ip, $port);
            $loop = 1;
            foreach ($zk->getAttendance() as $att) {
                $loop++;
                
                $tmp = array();
                $tmp['mechine_id'] = $ip . ":" . $port;
                $tmp['log_id'] = $att[0];
                $tmp['user_no'] = $att[1];
                $tmp['status_no'] = $att[2];
                $tmp['log_time'] = $att[3];


                //print_r($att);
                //echo "\n";

                if ($tmp['user_no'] != "") {

                    $chk = ScanLog::where('user_no', $att[1])->where('log_time', $att[3])->first();

                    if (empty($chk)) {
                        ScanLog::create($tmp);
                        $txtmessage .=  "I";
                    }
                }
            }

        // foreach ($zk->getUser() as $user) {
        //    print_r($user);
        //};

        //print_r($zk);
            $txtmessage .= $loop. " Records imported!!";
           // echo "\n";
            $zk->disconnect();
        //}
        return redirect('/planms/scanlist/select')->with('flash_message', $txtmessage);
    }
}
