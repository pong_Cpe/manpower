<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StaffShare extends Model
{
    public $fillable = [
        'staff_id', 'date_share', 'to_main_dep_id', 'note'
    ];
    
    public function staff()
    {
        return $this->belongsTo('App\Staff', 'staff_id', 'id');
    }

    public function maindep()
    {
        return $this->belongsTo('App\MainDep', 'to_main_dep_id', 'id');
    }
}
