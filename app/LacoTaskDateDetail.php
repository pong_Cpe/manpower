<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LacoTaskDateDetail extends Model
{
    protected $fillable = ['laco_task_date_id','laco_task_job_id','laco_task_pos_id','laco_staff_id','status'];

    public function lacotaskdate()
    {
        return $this->belongsTo('App\LacoTaskDate', 'laco_task_date_id', 'id');
    }

    public function lacotaskjob()
    {
        return $this->belongsTo('App\LacoTaskJob', 'laco_task_job_id', 'id');
    }

    public function lacotaskpos()
    {
        return $this->belongsTo('App\LacoTaskPos', 'laco_task_pos_id', 'id');
    }

    public function lacostaff()
    {
        return $this->belongsTo('App\LacoStaff', 'laco_staff_id', 'id');
    }
}
