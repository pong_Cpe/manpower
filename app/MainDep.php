<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MainDep extends Model
{
    protected $fillable = ['name', 'desc'];
}
