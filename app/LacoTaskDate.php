<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LacoTaskDate extends Model
{
    protected $fillable = ['laco_task_id','process_date','shift_id','plan_staff','desc','status', 'stamp_m_id'];
    
    public function lacotask()
    {
        return $this->belongsTo('App\LacoTask', 'laco_task_id', 'id');
    }

    public function shift()
    {
        return $this->belongsTo('App\Shift', 'shift_id', 'id');
    }

    public function stampm()
    {
        return $this->belongsTo('App\StampM', 'stamp_m_id', 'id');
    }

    public function lacotaskdatedetail()
    {
        return $this->hasMany('App\LacoTaskDateDetail', 'laco_task_date_id');
    }

    public function updatestatus($id,$status){
        $data = self::findOrFail($id);
        $data->status = $status;
        $data->update();
    }
}
