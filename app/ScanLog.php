<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ScanLog extends Model
{
    protected $fillable = ['mechine_id','log_id','user_no','status_no','log_time'];

    
}
