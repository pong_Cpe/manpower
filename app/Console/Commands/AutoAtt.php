<?php

namespace App\Console\Commands;


use Illuminate\Console\Command;

use App\Shift;
use App\Staff;
use App\ScanLog;
use App\ProcessLog;
use App\PlanM;
use App\PlanDUser;

class AutoAtt extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'auto:att {shift}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check for Come or not with date and shift';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ini_set('memory_limit', '256M');

        $date = date('Y-m-d');
        //$date = "2020-03-13";
        $shift = $this->argument('shift');

        $shiftObj = Shift::where('name',$shift)->first();

        $stafflist = Staff::where('shift_id',$shiftObj->id)->get();

        if($shift == 'B'){
            $startdatetime = $date." 06:00:00.000";
            $enddatetime = $date . " 08:00:00.000";

            $listusercome = ScanLog::whereBetween('log_time',[$startdatetime,$enddatetime])->pluck('user_no', 'user_no');
        }elseif($shift == 'C'){
            $startdatetime = $date . " 16:00:00.000";
            $enddatetime = $date . " 18:30:00.000";

            $listusercome = ScanLog::whereBetween('log_time', [$startdatetime, $enddatetime])->pluck('user_no', 'user_no');
        }

        $planmsTmp = PlanM::where('plandate', $date)
            ->where('shift_id', $shiftObj->id);
        $planms = $planmsTmp->get();
        $planmsList = $planms->pluck('id');

        $planduserlist = PlanDUser::whereIn('plan_m_id', $planmsList)->get();

        $planduserarr =array();

        foreach ($planduserlist as $planduserobj) {
            $planduserarr[$planduserobj->staff_id] = $planduserobj->id;
        }


        //print_r($listusercome);

        foreach ($stafflist as $staffObj) {
            $tmp = array();
            $tmp['log_date'] = $date;
            $tmp['shift_id'] = $shiftObj->id;
            $tmp['user_no'] = substr($staffObj->lacoid, -7);


            $mystaff = Staff::where('lacoid', $tmp['user_no'])->where('status', 'Active')->first();

            


            if(isset($listusercome[substr($staffObj->lacoid,-7)])){
                $tmp['status'] = 'Active';

                if (!empty($mystaff)) {
                    if(isset($planduserarr[$mystaff->id])){
                        $planduserobj = PlanDUser::findOrFail($planduserarr[$mystaff->id]);
                        $planduserobj->status = 'Active';
                        $planduserobj->update();
                    }
                }

            }else{
                $tmp['status'] = 'Absent';
            }

            if (!empty($mystaff)) {
                $tmp['staff_id'] = $mystaff->id;

                $chk = ProcessLog::where('log_date', $date)
                    ->where('shift_id', $shiftObj->id)
                    ->where('user_no', substr($staffObj->lacoid, -7))
                    ->first();


                if (empty($chk)) {
                    ProcessLog::create($tmp);
                } else {
                    $chk->update($tmp);
                }
            }

            
            


        }



    }
}
