<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use ZKLibrary;

use App\ScanLog;


class AutoScan extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'auto:scan {ip} {port}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Finger Scan to DB';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ini_set('memory_limit', '512M');

        require app_path() . '/tad/zklibrary.php';

        $mechlist = array(
            "4373" => "10.44.65.192",
            "4371" => "10.44.65.190",
            "4370" => "10.44.65.187",
            "4375" => "10.44.65.186",
            "4374" => "10.44.65.189",
            "4376" => "10.44.65.188",
        );


        $ip = $this->argument('ip');
        $port = $this->argument('port');

        //foreach ($mechlist as $key => $value) {
            try {
               
                $zk = new ZKLibrary();
                $connectionchk = $zk->connect($ip, $port);
                echo "connect\n";
                $zk->disableDevice();
                echo "disableDevice\n";
                //if($connectionchk){
                   
                foreach ($zk->getAttendance() as $att) {
                echo "can connect";
                    $tmp = array();
                    $tmp['mechine_id'] = $ip.":".$port;
                    $tmp['log_id'] = $att[0];
                    $tmp['user_no'] = $att[1];
                    $tmp['status_no'] = $att[2];
                    $tmp['log_time'] = $att[3];

                    if(
                        $tmp['user_no'] != "" 
                        //&& $tmp['log_id'] != ""
                        //&& $tmp['status_no'] != ""
                        //&& $tmp['log_time'] != "" 
                       /// && date('Y') == date('Y',strtotime($tmp['log_time']))
                        ){

                        $chk = ScanLog::where('user_no', $att[1])->where('log_time', $att[3])->first();

                        if(empty($chk)){
                            ScanLog::create($tmp);
                        }
                    
                    }
                    
                }
                
              
               // }
            $zk->enableDevice();
            echo "enableDevice\n";
            $zk->disconnect();
            echo "disconnect\n";
            } catch (Exception $e) {
                echo 'Caught exception: ',  $e->getMessage(), "\n";
            }
       
       // }
    }
}
