<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Shift;
use App\QcStaff;
use App\ScanLog;
use App\QcProcessLog;

class AutoQcAtt extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'auto:attqc {shift}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'QC Check for Come or not with date and shift';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ini_set('memory_limit', '256M');

        $date = date('Y-m-d');
        $shift = $this->argument('shift');

        $shiftObj = Shift::where('name', $shift)->first();

        $stafflist = QcStaff::get();

        if ($shift == 'B') {
            $startdatetime = $date . " 06:00:00.000";
            $enddatetime = $date . " 08:00:00.000";

            $listusercome = ScanLog::whereBetween('log_time', [$startdatetime, $enddatetime])->pluck('user_no', 'user_no');
        } elseif ($shift == 'C') {
            $startdatetime = $date . " 16:00:00.000";
            $enddatetime = $date . " 18:30:00.000";

            $listusercome = ScanLog::whereBetween('log_time', [$startdatetime, $enddatetime])->pluck('user_no', 'user_no');
        }

        //print_r($listusercome);

        foreach ($stafflist as $staffObj) {
            $tmp = array();
            $tmp['log_date'] = $date;
            $tmp['shift_id'] = $shiftObj->id;
            $tmp['user_no'] = substr($staffObj->lacocode, -7);


            $mystaff = QcStaff::where('lacocode', $tmp['user_no'])->where('status', 'Active')->first();

            if (isset($listusercome[substr($staffObj->lacocode, -7)])) {
                $tmp['status'] = 'Active';
            } else {
                $tmp['status'] = 'Absent';
            }

            if (!empty($mystaff)) {
                $tmp['staff_id'] = $mystaff->id;
            }

            $chk = QcProcessLog::where('log_date', $date)
                ->where('shift_id', $shiftObj->id)
                ->where('user_no', substr($staffObj->lacocode, -7))
                ->first();


            if (empty($chk)) {
                QcProcessLog::create($tmp);
            } else {
                $chk->update($tmp);
            }
        }
    }
}
