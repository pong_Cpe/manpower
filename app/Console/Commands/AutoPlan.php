<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;


use App\PlanM;
use App\Exp;
use App\PlanDUser;
use App\Shift;
use App\Staff;

class AutoPlan extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'auto:plan {date} {shift} {type}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Auto Create Plan Man for Jobs';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ini_set('memory_limit', '256M');
        $date = $this->argument('date');
        $shift = $this->argument('shift');
        $type = $this->argument('type');


        if($type == '1'){

            $shiftobj = Shift::where('name',$shift)->first();

            $planmsTmp = PlanM::where('plandate',$date)
                        ->where('shift_id', $shiftobj->id);
            $planms = $planmsTmp->get();
            $planmsList = $planms->pluck('id');
            
            echo "Plan ID : ";
            print_r($planmsList);
            echo "\n";

            foreach ($planms as $planmObj) {
                foreach ($planmObj->pland as $plandObj) {
                    echo $plandObj->job_id .'-'. $plandObj->plan_number. "\n";
                    $expList = Exp::where('job_id', $plandObj->job_id)->orderBy('priority')->get();
                    $countLoop = 1;
                    foreach ($expList as $expObj) {

                        $tmpStaffExp = array();
                        $tmpStaffExp['plan_m_id'] = $planmObj->id;
                        $tmpStaffExp['plan_d_id'] = $plandObj->id;
                        $tmpStaffExp['staff_id'] = $expObj->staff_id;

                        $chk = PlanDUser::whereIn('plan_m_id', $planmsList)
                                ->where('staff_id', $expObj->staff_id)
                                ->first();

                        if(empty($chk->id)){
                            echo "match staff id : ". $expObj->staff_id;
                            PlanDUser::create($tmpStaffExp);
                            if ($countLoop == $plandObj->plan_number) {
                                break;
                            } else {
                                $countLoop++;
                            }
                        }else{

                            echo "Reject staff id : " . $expObj->staff_id;
                        }
                        echo "\n";
                    }

                    if($countLoop < $plandObj->plan_number){
                        for ($i= $countLoop; $i <= $plandObj->plan_number; $i++) {
                            $tmpStaffExp = array();
                            $tmpStaffExp['plan_m_id'] = $planmObj->id;
                            $tmpStaffExp['plan_d_id'] = $plandObj->id;
                            $tmpStaffExp['staff_id'] = null;

                            PlanDUser::create($tmpStaffExp);
                        }
                    }

                        

                }

                PlanM::where('id', $planmObj->id)
                    ->update(['status' => 'Mapped']);

            }
        }elseif ($type == '2') {

            $shiftobj = Shift::where('name', $shift)->first();

            $planmsTmp = PlanM::where('plandate', $date)
                ->where('shift_id', $shiftobj->id);
            $planms = $planmsTmp->get();
            $planmsList = $planms->pluck('id');

           
            echo " Shift ID : ".$shiftobj->id;
            echo " Plan ID : ";
            print_r($planmsList);
            echo "\n";

            $listAllJob = array();


            foreach ($planms as $planmObj) {
                foreach ($planmObj->pland as $plandObj) {
                    echo $plandObj->job_id . '-' . $plandObj->plan_number . "\n";
                    
                   // $countLoop = 1;

                    $numberplan = $plandObj->planduser->count();

                    //if ($countLoop <= $plandObj->plan_number) {
                        for ($i = $numberplan; $i < $plandObj->plan_number; $i++) {
                            $tmpStaffExp = array();
                            $tmpStaffExp['plan_m_id'] = $planmObj->id;
                            $tmpStaffExp['plan_d_id'] = $plandObj->id;
                            $tmpStaffExp['staff_id'] = null;

                        PlanDUser::create($tmpStaffExp);

                        }
                    //}
                }

                PlanM::where('id', $planmObj->id)
                    ->update(['status' => 'Mapped']);
            }

            $planmsTmp = PlanM::where('plandate', $date)
                ->where('shift_id', $shiftobj->id);
            $planms = $planmsTmp->get();
            $planmsList = $planms->pluck('id');

            foreach ($planms as $planmObj) {
                foreach ($planmObj->pland as $plandObj) {
                    foreach ($plandObj->planduser as $planduserObj){
                        $listAllJob[$plandObj->job_id][] = $planduserObj->id;
                    }
                }
            }

            //var_dump($listAllJob);

            $stafflist = Staff::where('shift_id', $shiftobj->id)->where('status','Active')->pluck('id');

            for ($i=1; $i <= 5; $i++) { 
                $expdatas = Exp::whereIn('staff_id', $stafflist)->where('priority', $i)->orderBy('job_id')->get();
                foreach ($expdatas as $expobj) {
                    echo "Exp Level : ".$i." Staff ". $expobj->staff_id. " Job Id ".$expobj->job_id."\n";

                    if(isset($listAllJob[$expobj->job_id])){
                        $this->checkAndSave($expobj->staff_id, $expobj->job_id,$planmsList, $listAllJob[$expobj->job_id]);
                    }

                }
            }

/*
            $staffs = Staff::where('shift_id', $shiftobj->id)->get();

            foreach ($staffs as $staffObj) {
                $chk = PlanDUser::whereIn('plan_m_id', $planmsList)
                    ->where('staff_id', $staffObj->id)
                    ->first();
                echo  $staffObj->id ;
                if (empty($chk->id)) {
                    echo "Run\n";
                    foreach ($staffObj->explist(2) as $expObj) {
                        echo  $expObj->job_id ." = ". $expObj->priority ."\n";
                    }
                }
            }
            */
        }
    }

    public function checkAndSave($staff_id,$job_id,$planmsList,$listAllJob){
        $chk = PlanDUser::whereIn('plan_m_id', $planmsList)->where('staff_id', $staff_id)->first();

        if(empty($chk)){
            echo "Staff Available\n";
            $planuserobj = PlanDUser::whereIn('plan_m_id', $planmsList)->whereIn('id', $listAllJob)->where('staff_id')->first();
            if (!empty($planuserobj)) {
                $planuserobj->staff_id = $staff_id;
                $planuserobj->update();
                echo "Job Available\n";
            }else{
                echo "Job Not Available\n";
            }
        }else{
            echo "Staff Not Available\n";
        }

    }
}
