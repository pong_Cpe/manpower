<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TemplateD extends Model
{
    protected $fillable = [
        'template_m_id', 'job_id','default_number'
    ];

    public function templatem()
    {
        return $this->belongsTo('App\TemplateM', 'template_m_id', 'id');
    }

    public function job()
    {
        return $this->belongsTo('App\Job', 'job_id', 'id');
    }
}
