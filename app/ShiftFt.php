<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShiftFt extends Model
{
    protected $connection = 'sqlsrv2';

    protected $table = 'shifts';
    protected $fillable = [
        'name', 'desc',
    ];
}
