<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    protected $fillable = ['department_id','job_type_id','name', 'desc'];

    public function department()
    {
        return $this->belongsTo('App\Department', 'department_id', 'id');
    }

    public function jobtype()
    {
        return $this->belongsTo('App\JobType', 'job_type_id', 'id');
    }
}
