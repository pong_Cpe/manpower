<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LacoTaskJob extends Model
{
    protected $fillable = ['name','main_code','desc'];
}
