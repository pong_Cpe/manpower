<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LacoTransaction extends Model
{
    protected $fillable = ['laco_staff_id', 'laco_task_date_id', 'laco_task_date_detail_id','process'];
    
}
