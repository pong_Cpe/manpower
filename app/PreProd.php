<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PreProd extends Model
{
    protected $connection = 'sqlsrv2';

    protected $table = 'pre_prods';

    protected $fillable = [
        'name', 'desc'
    ];
}
