<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QcStaff extends Model
{
    protected $fillable = ['lacocode', 'name', 'type', 'pos', 'pos_code','shift_id','status'];
}
