<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LacoTaskPos extends Model
{
    protected $fillable = ['laco_task_id','laco_task_job_id','position_code','position_x','position_y'];

    public function lacotask()
    {
        return $this->belongsTo('App\LacoTask', 'laco_task_id', 'id');
    }

    public function lacojob()
    {
        return $this->belongsTo('App\LacoTaskJob', 'laco_task_job_id', 'id');
    }
}
