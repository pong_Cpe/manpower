<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TemplateM extends Model
{
    protected $fillable = [
        'department_id', 'name', 'desc', 
    ];

    public function department()
    {
        return $this->belongsTo('App\Department', 'department_id', 'id');
    }

    public function templated()
    {
        return $this->hasMany('App\TemplateD', 'template_m_id');
    }
}
