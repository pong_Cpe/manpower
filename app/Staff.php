<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    public $fillable = [
        'lacoid','name','department_id', 'shift_id','status'
    ];

    public function maindep()
    {
        return $this->belongsTo('App\MainDep', 'department_id', 'id');
    }

    public function shift()
    {
        return $this->belongsTo('App\Shift', 'shift_id', 'id');
    }

    public function exp()
    {
        return $this->hasMany('App\Exp', 'staff_id');
    }

    public function explist(){
        return $this->exp()->orderBy('priority');
    }

    public function expByPriority($priority_level)
    {
        return $this->exp()->where('priority', $priority_level);
    }

    public function processlog()
    {
        return $this->hasMany('App\ProcessLog', 'staff_id');
    }

}
