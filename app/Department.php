<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $fillable = ['main_dep_id','name', 'desc'];

    public function maindep()
    {
        return $this->belongsTo('App\MainDep', 'main_dep_id', 'id');
    }
}
