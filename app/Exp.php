<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exp extends Model
{
    public $fillable = [
        'staff_id','job_id','priority'
    ];

    public function job()
    {
        return $this->belongsTo('App\Job', 'job_id', 'id');
    }

    public function staff()
    {
        return $this->belongsTo('App\Staff', 'staff_id', 'id');
    }

    public function staffinshift($shift_id)
    {
        return $this->staff()->where('shift_id', $shift_id);
    }
}
