<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LacoStaff extends Model
{
    protected $fillable = ['lacoid'
      ,'name'
      ,'main_dep_id'
      ,'shift_id'
    ,'status'];

    public function maindep()
    {
        return $this->belongsTo('App\MainDep', 'main_dep_id', 'id');
    }

    public function shift()
    {
        return $this->belongsTo('App\Shift', 'shift_id', 'id');
    }
}
