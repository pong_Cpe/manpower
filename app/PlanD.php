<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlanD extends Model
{
    public $fillable = [
        'plan_m_id','job_id','plan_number'
    ];

    public function planm()
    {
        return $this->belongsTo('App\PlanM', 'plan_m_id', 'id');
    }

    public function job()
    {
        return $this->belongsTo('App\Job', 'job_id', 'id');
    }

    public function planduser()
    {
        return $this->hasMany('App\PlanDUser', 'plan_d_id');
    }

    public function available_planduser()
    {
        return $this->planduser()->where('staff_id', '<>', '0');
    }

    public function comtowork_planduser()
    {
        return $this->planduser()->where('staff_id', '<>', '0')->where('status','Active');
    }
}
