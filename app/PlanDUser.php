<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlanDUser extends Model
{
    public $fillable = [
            'plan_m_id','plan_d_id','staff_id','status','time_log_id','m_staff_id'
    ];

    public function planm()
    {
        return $this->belongsTo('App\PlanM', 'plan_m_id', 'id');
    }

    public function pland()
    {
        return $this->belongsTo('App\PlanD', 'plan_d_id', 'id');
    }

    public function staff()
    {
        return $this->belongsTo('App\Staff', 'staff_id', 'id');
    }

    public function mstaff()
    {
        return $this->belongsTo('App\Staff', 'm_staff_id', 'id');
    }

    public function timelog()
    {
        return $this->belongsTo('App\TimeLog', 'time_log_id', 'id');
    }
}
