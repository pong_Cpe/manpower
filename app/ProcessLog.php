<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProcessLog extends Model
{
    protected $fillable = ['log_date','shift_id','user_no','status','staff_id'];

    public function staff()
    {
        return $this->belongsTo('App\Staff', 'staff_id', 'id');
    }

}
