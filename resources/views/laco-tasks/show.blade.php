@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">LacoTask {{ $lacotask->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/laco-tasks') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/laco-tasks/' . $lacotask->id . '/edit') }}" title="Edit LacoTask"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                        <a href="{{ url('/laco-tasks/generate/' . $lacotask->id ) }}" title="Edit LacoTask"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Generate Job</button></a>

                        <form method="POST" action="{{ url('lacotasks' . '/' . $lacotask->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete LacoTask" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                        </form>
                        <br/>
                        @if(Session::has('flash_message'))
                        <p class="alert alert-info">{{ Session::get('flash_message') }}</p>
                        @endif 
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $lacotask->id }}</td>
                                    </tr>
                                    <tr>
                                        <th>รายการ</th><td>{{ $lacotask->name }}</td>
                                    </tr>
                                    <tr>
                                        <th>สินค้า</th><td>{{ $lacotask->product }}</td>
                                    </tr>
                                    <tr>
                                        <th>แผนพนักงาน</th><td>{{ $lacotask->plan_staff }}</td>
                                    </tr>
                                    <tr>
                                        <th>รายละเอียด</th><td>{{ $lacotask->desc }}</td>
                                    </tr>
                                    <tr>
                                        <th>แผนผัง</th><td>
                                        @if (!empty($lacotask->map))
                                            <img src="{{ url($lacotask->map) }}" height="150px" alt="">
                                        @endif</td>
                                    </tr>                                  
                                </tbody>
                            </table>


                            <div class="col-md-12">
                                @foreach ($taskjobArr as $key=>$itemObj)
                                <div class="row" style="border: solid 1px black; padding: 1px;" >
                                    

                                    <div class="col-md-3 text-center"><b>{{ $key }}</b>

                                        <div class="barcode39">
                                            *{{ $itemObj[0]->lacojob->main_code }}*
                                            </div>
                                            {{ $key }} 
                                            @if ($caneditflag)
                                                <a href="{{ url('laco-tasks/adddetail/'.$lacotask->id.'/'.$itemObj[0]->laco_task_job_id) }}">Add</a>                                         
                                            @endif

                                    </div>
                                    <div class="col-md-9">

                                                            
                                                                @foreach ($itemObj as $item2)
                                                                <div  class="col-md-4 text-center" style="border: solid 1px black; ">
                                            <div class="barcode39">
                                            *{{ $item2->position_code }}*
                                            </div>
                                            <a href="{{ url('laco-tasks/editpos/'.$item2->id) }}">[{{ $item2->position_x }},{{ $item2->position_y }}]</a>
                                            {{ $item2->position_code }}
                                                @if ($caneditflag){}
                                                    <a href="{{ url('laco-tasks/detetepost/'.$lacotask->id.'/'.$item2->id) }}">X</a>
                                                @endif
                                             </div>
                                            @endforeach  
                                    </div>
                                </div>
                                                @endforeach   
                                
                            </div>



                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <br/>
    <br/>
@endsection
