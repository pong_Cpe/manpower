@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Create New LacoTask</div>
                    <div class="card-body">
                        <a href="{{ url('/laco-tasks/'.$lacotask->id) }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>รายการ</th>
                                        <th>สินค้า</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>{{ $lacotask->id }}</td>
                                        <td>{{ $lacotask->name }}</td>
                                        <td>{{ $lacotask->product }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <form method="POST" action="{{ url('/laco-tasks/generateAction/'.$lacotask->id) }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                            {{ csrf_field() }}

                            <div class="col-md-4  {{ $errors->has('laco_task_job_id') ? 'has-error' : ''}}">
                                    {!! Form::label('laco_task_job_id', 'งาน', ['class' => 'control-label']) !!}
                                    {!! Form::select('laco_task_job_id', $lacotaskjoblist,null, ['class' => 'form-control']) !!}                                   
                                    {!! $errors->first('laco_task_job_id', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="col-md-4  {{ $errors->has('job_num') ? 'has-error' : ''}}">
                                <label for="job_num" class="control-label">{{ 'จำนวนคน' }}</label>
                                <input class="form-control" name="job_num" type="number" id="job_num" required >
                                {!! $errors->first('job_num', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="col-md-4">
                                <input class="btn btn-primary" type="submit" value="Generate">
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
