@extends('layouts.printbarcode')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ $lacotask->name }} {{ $lacotask->product }}</div>
                    <div class="card-body">
                        <DIV class="row">
                         @foreach ($taskjobArr as $key=>$itemObj)
                                         <div class="col-md-3">
                                         <div class="barcode39">
                                            *{{ $itemObj[0]->lacojob->main_code }}*
                                            </div>
                                            {{ $key }} <br/>
                                         @foreach ($itemObj as $item2)
                                         <div class="barcode39">
 *{{ $item2->position_code }}*
</div><br/>
                                            @endforeach
                            </div> @endforeach   
                        </DIV>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
