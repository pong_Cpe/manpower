@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Edit LacoTask #{{ $lacojobpos->id }}</div>
                    <div class="card-body">
                        <a href="{{ url('/laco-tasks') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        <form method="POST" action="{{ url('/laco-tasks/editposAction/' . $lacojobpos->id) }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                            
                            {{ csrf_field() }}
                            <div class="col-md-6  {{ $errors->has('position_x') ? 'has-error' : ''}}">
                                <label for="position_x" class="control-label">{{ 'แกน X' }}</label>
                                <input class="form-control" name="position_x" type="text" id="position_x" required value="{{ $lacojobpos->position_x or ''}}" >
                                {!! $errors->first('position_x', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="col-md-6  {{ $errors->has('position_y') ? 'has-error' : ''}}">
                                <label for="position_y" class="control-label">{{ 'แกน Y' }}</label>
                                <input class="form-control" name="position_y" type="text" id="position_y" required value="{{ $lacojobpos->position_y or ''}}" >
                                {!! $errors->first('position_y', '<p class="help-block">:message</p>') !!}
                            </div>
                            
                            <div class="col-md-12 form-group">
                                <input class="btn btn-primary" type="submit" value="Update">
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
