<div class="col-md-6  {{ $errors->has('name') ? 'has-error' : ''}}">
    <label for="name" class="control-label">{{ 'รายการ' }}</label>
    <input class="form-control" name="name" type="text" id="name" required value="{{ $lacotask->name or ''}}" >
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-6  {{ $errors->has('product') ? 'has-error' : ''}}">
    <label for="product" class="control-label">{{ 'สินค้า' }}</label>
    <input class="form-control" name="product" type="text" id="product" required value="{{ $lacotask->product or ''}}" >
    {!! $errors->first('product', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-6  {{ $errors->has('plan_staff') ? 'has-error' : ''}}">
    <label for="plan_staff" class="control-label">{{ 'แผนกำลังคน' }}</label>
    <input class="form-control" name="plan_staff" type="number" id="plan_staff" required value="{{ $lacotask->plan_staff or '0'}}" >
    {!! $errors->first('plan_staff', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-6  {{ $errors->has('map_file') ? 'has-error' : ''}}">
    <label for="map_file" class="control-label">{{ 'แผนผัง' }}</label>
    <input class="form-control" name="map_file" type="file" id="map_file">
    @if (isset($lacotask->map))
    <a href="{{ url($lacotask->map) }}" target="_blank"><img src="{{ url($lacotask->map) }}" height="50px" alt=""></a>

    @endif
    {!! $errors->first('map_file', '<p class="help-block">:message</p>') !!}
</div>

<div class="col-md-12  {{ $errors->has('desc') ? 'has-error' : ''}}">
    <label for="desc" class="control-label">{{ 'รายละเอียด' }}</label>
    <textarea class="form-control" name="desc" type="text" id="desc">{{ $lacotaskjob->desc or ''}}</textarea>
    {!! $errors->first('desc', '<p class="help-block">:message</p>') !!}
</div>

<div class="col-md-12 form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
