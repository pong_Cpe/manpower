<div class="col-md-4  {{ $errors->has('name') ? 'has-error' : ''}}">
    <label for="name" class="control-label">{{ 'ชื่อ' }}</label>
    <input class="form-control" name="name" type="text" id="name" required value="{{ $department->name or ''}}" >
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-4  {{ $errors->has('main_dep_id') ? 'has-error' : ''}}">
        {!! Form::label('main_dep_id', 'แผนก', ['class' => 'control-label']) !!}
        @if (isset($department->main_dep_id))
            {!! Form::select('main_dep_id', $departmentlist,$department->main_dep_id, ['class' => 'form-control']) !!}   
        @else
            {!! Form::select('main_dep_id', $departmentlist,null, ['class' => 'form-control']) !!}
        @endif
        {!! $errors->first('main_dep_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-12  {{ $errors->has('desc') ? 'has-error' : ''}}">
    <label for="desc" class="control-label">{{ 'รายละเอียด' }}</label>
    <textarea class="form-control" name="desc" type="text" id="desc">{{ $department->desc or ''}}</textarea>
    {!! $errors->first('desc', '<p class="help-block">:message</p>') !!}
</div>

<div class="col-md-12">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
