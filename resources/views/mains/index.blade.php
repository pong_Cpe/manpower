@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header"><h3>Menu</h3></div>
                    <div class="card-body">
                        <h4>ข้อมูลพื้นฐาน</h4>
                        <div class="row">
                            <div class="col-md-3">
                                <a href="{{ url('/main-deps') }}" class="btn btn-success btn-block" title="Add New MainDep">
                                    <i class="fa fa-codepen" aria-hidden="true"></i> แผนก
                                </a>
                            </div>
                            <div class="col-md-3">
                                <a href="{{ url('/departments') }}" class="btn btn-success btn-block" title="Add New MainDep">
                                    <i class="fa fa-codepen" aria-hidden="true"></i> ส่วนงาน
                                </a>
                            </div>
                            <div class="col-md-3">
                                <a href="{{ url('/jobs') }}" class="btn btn-success btn-block" title="Add New MainDep">
                                    <i class="fa fa-codepen" aria-hidden="true"></i> งาน
                                </a>
                            </div>
                        </div>
                        <h4>ข้อมูลหลัก</h4>
                        <div class="row">
                            <div class="col-md-3">
                                <a href="{{ url('/staffs') }}" class="btn btn-success btn-block" title="Add New MainDep">
                                    <i class="fa fa-codepen" aria-hidden="true"></i> พนักงาน + ความสามารถ
                                </a>
                            </div>
                            <div class="col-md-3">
                                <a href="{{ url('/templatems') }}" class="btn btn-success btn-block" title="Add New MainDep">
                                    <i class="fa fa-codepen" aria-hidden="true"></i> แม่พิมพ์ตารางงาน
                                </a>
                            </div>
                        </div>
                         <h4>ข้อมูลแผนงาน และ การปฏิบัติงาน</h4>
                         <div class="row">
                            <div class="col-md-3">
                                <a href="{{ url('/planms') }}" class="btn btn-success btn-block" title="Add New MainDep">
                                    <i class="fa fa-codepen" aria-hidden="true"></i> แผนงาน
                                </a>
                            </div>
                            <div class="col-md-3">
                                <a href="{{ url('/monitors/main/'.date('Y-m-d')) }}" class="btn btn-success btn-block" title="Add New MainDep">
                                    <i class="fa fa-codepen" aria-hidden="true"></i> จำนวนคนมาทำงาน
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
