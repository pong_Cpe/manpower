@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header"><h3>Menu</h3></div>
                    <div class="card-body">
                        <h4>ข้อมูลพื้นฐาน</h4>
                        <div class="row">
                            <div class="col-md-3">
                                <a href="{{ url('/main-deps') }}" class="btn btn-success btn-block" title="Add New MainDep">
                                    <i class="fa fa-codepen" aria-hidden="true"></i> แผนก
                                </a>
                            </div>
                            <div class="col-md-3">
                                <a href="{{ url('/shifts') }}" class="btn btn-success btn-block" title="Add New MainDep">
                                    <i class="fa fa-codepen" aria-hidden="true"></i> กะ
                                </a>
                            </div>
                        </div>
                        <h4>ข้อมูลหลัก</h4>
                        <div class="row">
                            <div class="col-md-3">
                                <a href="{{ url('/laco-staffs') }}" class="btn btn-success btn-block" title="Add New MainDep">
                                    <i class="fa fa-codepen" aria-hidden="true"></i> พนักงาน + แผนก + กะ
                                </a>
                            </div>
                            <div class="col-md-3">
                                <a href="{{ url('/laco-task-jobs') }}" class="btn btn-success btn-block" title="Add New MainDep">
                                    <i class="fa fa-codepen" aria-hidden="true"></i> งานต่างๆ
                                </a>
                            </div>
                            <div class="col-md-3">
                                <a href="{{ url('/laco-tasks') }}" class="btn btn-success btn-block" title="Add New MainDep">
                                    <i class="fa fa-codepen" aria-hidden="true"></i> แม่พิมพ์งาน
                                </a>
                            </div>
                        </div>
                         <h4>ข้อมูลแผนงาน และ การปฏิบัติงาน</h4>
                         <div class="row">
                            <div class="col-md-3">
                                <a href="{{ url('/laco-task-dates') }}" class="btn btn-success btn-block" title="Add New MainDep">
                                    <i class="fa fa-codepen" aria-hidden="true"></i> แผนงาน
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
