<div class="col-md-4  {{ $errors->has('name') ? 'has-error' : ''}}">
    <label for="name" class="control-label">{{ 'ชื่อ' }}</label>
    <input class="form-control" name="name" type="text" id="name" required value="{{ $lacotaskjob->name or ''}}" >
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-4  {{ $errors->has('main_code') ? 'has-error' : ''}}">
    <label for="main_code" class="control-label">{{ 'Main Code' }}</label>
    <input class="form-control" name="main_code" type="text" id="main_code" required value="{{ $lacotaskjob->main_code or ''}}" >
    {!! $errors->first('main_code', '<p class="help-block">:message</p>') !!}
</div>

<div class="col-md-12  {{ $errors->has('desc') ? 'has-error' : ''}}">
    <label for="desc" class="control-label">{{ 'รายละเอียด' }}</label>
    <textarea class="form-control" name="desc" type="text" id="desc">{{ $lacotaskjob->desc or ''}}</textarea>
    {!! $errors->first('desc', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
