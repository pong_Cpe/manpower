@extends('layouts.scan')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Create New QcStaff</div>
                    <div class="card-body">
<form method="POST" action="{{ url('/scans/savedata') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                        
                        <div class="row">
                            <div class='col-12' >{{ csrf_field() }}
                            <div class="form-group col-12">
                                <input type="text" id="user_no" name ="user_no" autofocus >
                                <input class="btn btn-primary" type="submit" value="Create">
                            </div></div>
                        </div>
                            

                        </form>
                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif
                        @if(Session::has('flash_message'))
                        <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('flash_message') }}</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
