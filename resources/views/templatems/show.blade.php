@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">templatem {{ $templatem->id }}</div>
                    <div class="card-body">
                        <br/>

                        <a href="{{ url('/templatems') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/templatems/' . $templatem->id . '/edit') }}" title="Edit templatem"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                        <form method="POST" action="{{ url('templatems' . '/' . $templatem->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete templatem" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                        </form>
                        <br/>
                        <br/>
                        <div class="row">
                            <div class="col-md-4" >
                                ID : {{ $templatem->id }} , 
                                Department : {{ $templatem->department->name }} , 
                                name : {{ $templatem->name }}
                            </div>
                            <div class="col-md-8" >
                                desc : {{ $templatem->desc }}
                            </div>
                        </div>

                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Department</th>
                                        <th>Job</th>
                                        <th>Number of person</th>
                                        <th><a href="{{ url('/templatems/setJob/'.$templatem->id) }}" class="btn btn-success btn-sm" title="Add New templatem">
                            <i class="fa fa-plus" aria-hidden="true"></i> Set Job
                        </a></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if ($templatem->templated->count() > 0)
                                        @foreach ($templatem->templated as $subitem)
                                        <tr>
                                            <td>{{ $subitem->job->department->name or ''}}</td>
                                            <td>{{ $subitem->job->name or ''}}</td>
                                            <td>{{ $subitem->default_number }}</td>
                                            <td>
                                                <a href="{{ url('/templatems/deletedetail/' . $subitem->id ) }}" title="Edit templatem" onclick="return confirm(&quot;Confirm delete?&quot;)">
                                                <button type="button" class="btn btn-danger btn-sm" title="Delete templatem"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                                            </a></td>
                                        </tr> 
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="4" style="text-align: center;"><h3>ไม่มีข้อมูล</h3></td>
                                        </tr> 
                                    @endif
                                    
                                    
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
