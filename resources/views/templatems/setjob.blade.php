@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">แม่แบบ {{ $templatem->id }}</div>
                    <div class="card-body">
                        <br/>

                        <a href="{{ url('/templatems') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/templatems/' . $templatem->id . '/edit') }}" title="Edit templatem"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                        <form method="POST" action="{{ url('templatems' . '/' . $templatem->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete templatem" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                        </form>
                        <br/>
                        <br/>
                        <div class="row">
                            <div class="col-md-4" >
                                ID : {{ $templatem->id }} , 
                                Department : {{ $templatem->department->name }} , 
                                name : {{ $templatem->name }}
                            </div>
                            <div class="col-md-8" >
                                desc : {{ $templatem->desc }}
                            </div>
                        </div>
                        <form method="POST" action="{{ url('/templatems/setJobAction/' . $templatem->id) }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                            {{ csrf_field() }}
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>ส่วนงาน</th>
                                        <th>งาน</th>
                                        <th>จำนวนคน</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($jobslist as $item)
                                        <tr>
                                            <td>{{ $item->department->name }}</td>
                                            <td>{{ $item->name }}</td>
                                            <td><input class="form-control" name="job{{$item->id}}" type="text" id="job{{$item->id}}" value="{{ $jobvalList[$item->id]->default_number or ''}}" ></td>
                                        </tr>
                                    @endforeach                
                                </tbody>
                            </table>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <input class="btn btn-primary" type="submit" value="Set Job">
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
