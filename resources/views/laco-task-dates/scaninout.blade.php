@extends('layouts.appscan2')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header"><h1>SCAN พนักงาน {{ $lacotaskdate->process_date }} / {{ $lacotaskdate->shift->name }} งาน {{ $lacotaskdate->lacotask->name or '' }} / {{ $lacotaskdate->lacotask->product or ''  }}</h1></div>
                    <div class="card-body">
                        <h2>
                        @if(Session::has('flash_message'))
                            <p class="alert alert-info">{{ Session::get('flash_message') }}</p>
                        @endif
                        </h2>
                        <div class="row">
                            <form method="GET" action="{{ url('/laco-task-dates/scaninoutaction/'.$lacotaskdate->id) }}" accept-charset="UTF-8" class="my-2 my-lg-0 float-right" role="search">
                                <div class="col-md-12">
                                    <input type="text" class="input-lg" name="lacostaffcode" tabindex="-1"  placeholder="Scan Staff Barcode..." value="{{ request('lacostaffcode') }}" style="Width:500px;" autofocus>
                                    <span class="form-group-append">
                                        <button class="btn btn-secondary btn-lg" type="submit">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </span>
                                </div>
                            </form>
                            <div class="col-md-12">
                                <h2><div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>แผนจำนวนคน</th>
                                                <th>มาเข้างาน</th>
                                                <th>อยู่ในห้อง</th>
                                                <th>อยู่นอกห้อง</th>
                                                <th>สถานะ</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>{{ $lacotaskdate->plan_staff or ''  }}</td>
                                                <td>{{ $lacotaskdate->lacotaskdatedetail()->where('laco_staff_id','<>','')->count() }}</td>
                                                <td>{{ $lacotaskdate->lacotaskdatedetail()->where('laco_staff_id','<>','')->where('status','In')->count() }}</td>
                                                <td>{{ $lacotaskdate->lacotaskdatedetail()->where('laco_staff_id','<>','')->where('status','<>','In')->count() }}</td>                                                
                                                <td>{{ $lacotaskdate->status }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div></h2>
                                
                            </div>
                            <div class="col-md-12">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
