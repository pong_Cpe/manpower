@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">LacoTaskDate {{ $lacotaskdate->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/laco-task-dates') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/laco-task-dates/scanstaff/' . $lacotaskdate->id)  }}" title="Edit LacoTaskDate"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Scan Staff</button></a>
                        <a href="{{ url('/laco-task-dates/' . $lacotaskdate->id . '/edit') }}" title="Edit LacoTaskDate"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>


                        <a href="{{ url('/laco-task-dates/' . $lacotaskdate->id . '/edit') }}" title="Edit LacoTaskDate"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                        <form method="POST" action="{{ url('lacotaskdates' . '/' . $lacotaskdate->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete LacoTaskDate" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                        </form>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>วันที่ / กะ</th>
                                        <th>รายการ</th>
                                        <th>สินค้า</th>
                                        <th>แผนจำนวนคน</th>
                                        <th>สถานะ</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>{{ $lacotaskdate->id }}</td>
                                        <td>{{ $lacotaskdate->process_date }} / {{ $lacotaskdate->shift->name }}</td>
                                        <td>{{ $lacotaskdate->lacotask->name or '' }}</td>
                                        <td>{{ $lacotaskdate->lacotask->product or ''  }}</td>
                                        <td>{{ $lacotaskdate->plan_staff or ''  }}</td>
                                        <td>{{ $lacotaskdate->status }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        
                        <table class="table">
                                <thead>
                                    <tr>
                                        @foreach ($taskjobArr as $key=>$itemObj)
                                        <th>{{ $key }} 
                                       </th>
                                        @endforeach   
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>

                                        @foreach ($taskjobArr as $key=>$itemObj)
                                        <td>
                                         @foreach ($itemObj as $item2)
                                                {{ $item2->lacotaskpos->position_code or '' }} / 
                                                {{ $item2->lacostaff->name or 'ไม่ได้ตั้งคน' }}
                                                <br/>
                                            @endforeach
                                        @endforeach   
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
