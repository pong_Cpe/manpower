@extends('layouts.appscan')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">SCAN พนักงาน {{ $lacotaskdate->process_date }} / {{ $lacotaskdate->shift->name }} งาน {{ $lacotaskdate->lacotask->name or '' }} / {{ $lacotaskdate->lacotask->product or ''  }}</div>
                    <div class="card-body">
                        @if(Session::has('flash_message'))
                            <p class="alert alert-info">{{ Session::get('flash_message') }}</p>
                        @endif
                        <div class="row">
                            <div class="col-md-4"><form method="GET" action="{{ url('/laco-task-dates/scanprocessaction/'.$lacotaskdate->id) }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="lacostaffcode" tabindex="-1"  placeholder="Scan Staff Barcode..." value="{{ request('lacostaffcode') }}" autofocus>
                                    <span class="form-group-append">
                                        <button class="btn btn-secondary" type="submit">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </span>
                                </div>
                            </form></div>
                            <div class="col-md-8">
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>แผนจำนวนคน</th>
                                                <th>มาเข้างาน</th>
                                                <th>อยู่ในห้อง</th>
                                                <th>อยู่นอกห้อง</th>
                                                <th>สถานะ</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>{{ $lacotaskdate->plan_staff or ''  }}</td>
                                                <td>{{ $lacotaskdate->lacotaskdatedetail()->where('laco_staff_id','<>','')->count() }}</td>
                                                <td>{{ $lacotaskdate->lacotaskdatedetail()->where('laco_staff_id','<>','')->where('status','In')->count() }}</td>
                                                <td>{{ $lacotaskdate->lacotaskdatedetail()->where('laco_staff_id','<>','')->where('status','<>','In')->count() }}</td>                                                
                                                <td>{{ $lacotaskdate->status }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="col-md-12">
                                @foreach ($taskjobArr as $key=>$itemObj)
                                <div class="row" style="border: solid 1px black; padding: 1px;" >
                                    

                                    <div class="col-md-3"><b>{{ $key }}</b>
                                    </div>
                                    <div class="col-md-9">
                                        @foreach ($itemObj as $item2)                                                            
                                            @if (!empty($item2->laco_staff_id))
                                                @if ($item2->status == 'In')
                                                    <div  class="col-md-3 bg-success" style="border: solid 1px black; height : 50px;">
                                                        {{ $item2->lacotaskpos->position_code or 'เติม'}}<br/> 
                                                        {{ $item2->lacostaff->name or 'ไม่ได้ตั้งคน' }}
                                                        @if (!empty($item2->laco_staff_id))
                                                            <a href="{{ url('/laco-task-dates/removestaffjob/'.$lacotaskdate->id."/".$item2->laco_staff_id) }}">X</a>
                                                        @endif
                                                    </div>
                                                @else
                                                    <div  class="col-md-3 bg-danger" style="border: solid 1px black; height : 50px;">
                                                        {{ $item2->lacotaskpos->position_code or 'เติม'}}<br/>
                                                        {{ $item2->lacostaff->name or 'ไม่ได้ตั้งคน' }}
                                                        @if (!empty($item2->laco_staff_id))
                                                            <a href="{{ url('/laco-task-dates/removestaffjob/'.$lacotaskdate->id."/".$item2->laco_staff_id) }}">X</a>
                                                        @endif
                                                    </div> 
                                                @endif  
                                            @else
                                                    <div  class="col-md-3" style="border: solid 1px black; height : 50px;">
                                                        {{ $item2->lacotaskpos->position_code or 'เติม'}}<br/>
                                                        {{ $item2->lacostaff->name or 'ไม่ได้ตั้งคน' }}
                                                        @if (!empty($item2->laco_staff_id))
                                                            <a href="{{ url('/laco-task-dates/removestaffjob/'.$lacotaskdate->id."/".$item2->laco_staff_id) }}">X</a>
                                                        @endif
                                                    </div>                
                                            @endif      
                                                            @endforeach
                                    </div>
                                </div>
                                                @endforeach   
                                
                            </div>

                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                            <th class="text-center" colspan="{{ sizeof($taskjobArr) }}">งาน                                            
                                            </th>                                       
                                            </tr>
                                            <tr>
                                                @foreach ($taskjobArr as $key=>$itemObj)
                                                <th>{{ $key }}</th>
                                                @endforeach   
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                @foreach ($taskjobArr as $key=>$itemObj)
                                                    <td>                                                
                                                    <table  class="table table-bordered">
                                                        <tbody>
                                                            @foreach ($itemObj as $item2)
                                                            <tr>
                                                                @if (!empty($item2->laco_staff_id))
                                                                    @if ($item2->status == 'In')
                                                                        <td  class="bg-success">
                                                                        {{ $item2->lacotaskpos->position_code or 'เติม'}} / 
                                                                        {{ $item2->lacostaff->name or 'ไม่ได้ตั้งคน' }}
                                                                        @if (!empty($item2->laco_staff_id))
                                                                            <a href="{{ url('/laco-task-dates/removestaffjob/'.$lacotaskdate->id."/".$item2->laco_staff_id) }}">X</a>
                                                                        @endif
                                                                    </td>
                                                                    @else
                                                                        <td  class="bg-danger">
                                                                        {{ $item2->lacotaskpos->position_code or 'เติม'}} / 
                                                                        {{ $item2->lacostaff->name or 'ไม่ได้ตั้งคน' }}
                                                                        @if (!empty($item2->laco_staff_id))
                                                                            <a href="{{ url('/laco-task-dates/removestaffjob/'.$lacotaskdate->id."/".$item2->laco_staff_id) }}">X</a>
                                                                        @endif
                                                                    </td>
                                                                    @endif
                                                                    
                                                                @else
                                                                    <td>
                                                                        {{ $item2->lacotaskpos->position_code or 'เติม'}} / 
                                                                        {{ $item2->lacostaff->name or 'ไม่ได้ตั้งคน' }}
                                                                        @if (!empty($item2->laco_staff_id))
                                                                            <a href="{{ url('/laco-task-dates/removestaffjob/'.$lacotaskdate->id."/".$item2->laco_staff_id) }}">X</a>
                                                                        @endif
                                                                    </td>
                                                                @endif                                                        
                                                            </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>                                                    
                                                    </td>
                                                @endforeach   
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
