@extends('layouts.realtime')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                <div class="card-header">งาน {{ $lacotaskdate->lacotask->name }} วันที่  {{ $lacotaskdate->process_date }} ({{ $lacotaskdate->id }})  Update ล่าสุด {{ date("Y-m-d H:i:s") }}</div>
                    <div class="card-body">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th class="text-center">แผนที่ตั้งไว้</th>
                                    <th class="text-center">มาเข้างาน</th>
                                    <th class="text-center">อยู่ในห้อง</th>
                                    <th class="text-center">อยู่นอกห้อง</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                <td class="text-center"><h2>{{ $lacotaskdate->plan_staff }}</h2></td>
                                    <td class="text-center"><h2>{{ $lacotaskdate->lacotaskdatedetail()->where('laco_staff_id','<>','')->count() }}</h2></td>
                                    <td class="text-center"><h2>{{ $lacotaskdate->lacotaskdatedetail()->where('laco_staff_id','<>','')->where('status','In')->count() }}</h2></td>
                                    <td class="text-center"><h2>{{ $lacotaskdate->lacotaskdatedetail()->where('laco_staff_id','<>','')->where('status','<>','In')->count() }}</h2></td>
                                </tr>
                            </tbody>
                        </table>
                        <canvas id="myCanvas" width="1000" height="600"
                        style="border:1px solid #d3d3d3;">
                        Your browser does not support the canvas element.
                        </canvas>
                        <img id="mymap" width="0" height="0"
                    src="{{ url("/".$lacotaskdate->lacotask->map) }}" alt="The Scream" >
                    <br/>
                     <a href="{{ url('/laco-tasks/' . $lacotaskdate->laco_task_id) }}" target="_blank" title="View LacoTaskDate"><button class="btn btn-info btn-sm"><i class="fa fa-search" aria-hidden="true"></i> Edit Position</button></a>                                            
                                           
                    </div>
                </div>
            </div>
        </div>
    </div>
    @php
        $nopositon_x = 25;
        $nopositon_y = 20;
    @endphp
    <script>


$( document ).ready(function() {

    $('canvas').addLayer({
    type: 'image',
    source: '{{ url("/".$lacotaskdate->lacotask->map) }}',
    x: 500, y: 300,
    width: 1000, height: 600
    })
    @foreach ($lacotaskdate->lacotaskdatedetail as $item)
    .addLayer({
    type: 'arc',
    @if (!empty($item->laco_staff_id))
    @if ($item->status == 'In')    
    fillStyle: '#00FF00', 
    @else
    fillStyle: '#FF0000',
    @endif 
    @else
    fillStyle: '#FFFFFF',
    @endif    
    strokeStyle: '#000000',
    strokeWidth: 1,
    @if (isset($item->lacotaskpos->position_x))
        x: {{ $item->lacotaskpos->position_x or '10' }}, y: {{ $item->lacotaskpos->position_y or '10' }},
    @else
        x: {{ $nopositon_x }}, y: {{ $nopositon_y }},
        @php
        $nopositon_x += 25;
    @endphp
    @endif
    radius: 10,
    click: function(layer) {
        @if (!empty($item->laco_staff_id))
        alert('{{ $item->lacotaskjob->name }} / {{ $item->lacostaff->name }}');
        @else
        alert('{{ $item->lacotaskjob->name }} / ว่าง');
        @endif
    }
    })
    @endforeach
    .drawLayers();
});

   setTimeout(function(){
       location.reload();
   },(60000 * 3));
    </script>
@endsection
