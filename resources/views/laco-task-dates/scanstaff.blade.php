@extends('layouts.appscan')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">SCAN พนักงาน {{ $lacotaskdate->process_date }} / {{ $lacotaskdate->shift->name }} งาน {{ $lacotaskdate->lacotask->name or '' }} / {{ $lacotaskdate->lacotask->product or ''  }}</div>
                    <div class="card-body">
                        @if(Session::has('flash_message'))
                            <p class="alert alert-info">{{ Session::get('flash_message') }}</p>
                        @endif
                        <div class="row">
                            <div class="col-md-4"><form method="GET" action="{{ url('/laco-task-dates/scanstaff/'.$lacotaskdate->id) }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="lacostaffcode" tabindex="-1"  placeholder="Scan Staff Barcode..." value="{{ request('lacostaffcode') }}" autofocus>
                                    <span class="form-group-append">
                                        <button class="btn btn-secondary" type="submit">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </span>
                                </div>
                            </form></div>
                            <div class="col-md-8">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>แผนจำนวนคน / กรอบ / เข้างาน</th>
                                                <th>สถานะ</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>{{ $lacotaskdate->plan_staff or ''  }} / {{ $lacotaskdate->lacotaskdatedetail->count() }} / {{ $lacotaskdate->lacotaskdatedetail()->where('laco_staff_id','<>','')->count() }}</td>
                                                <td>{{ $lacotaskdate->status }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            
                            <div class="col-md-12">
                                @foreach ($taskjobArr as $key=>$itemObj)
                                <div class="row" style="border: solid 1px black; padding: 1px;" >
                                    

                                    <div class="col-md-3"><b>{{ $key }}</b>
                                    </div>
                                    <div class="col-md-9">
                                        @foreach ($itemObj as $item2)
                                                            
                                                                @if (!empty($item2->laco_staff_id))
                                                                    <div  class="col-md-3 bg-success" style="border: solid 1px black; height : 50px;">
                                                                        {{ $item2->lacotaskpos->position_code or 'เติม'}}<br/> 
                                                                        {{ $item2->lacostaff->name or 'ไม่ได้ตั้งคน' }}
                                                                        @if (!empty($item2->laco_staff_id))
                                                                            <a href="{{ url('/laco-task-dates/removestaffjob/'.$lacotaskdate->id."/".$item2->laco_staff_id) }}">X</a>
                                                                        @endif
                                                                    </div>
                                                                @else
                                                                    <div  class="col-md-3 bg-primary" style="border: solid 1px black; height : 50px;">
                                                                        {{ $item2->lacotaskpos->position_code or 'เติม'}}<br/>
                                                                        {{ $item2->lacostaff->name or 'ไม่ได้ตั้งคน' }}
                                                                        @if (!empty($item2->laco_staff_id))
                                                                            <a href="{{ url('/laco-task-dates/removestaffjob/'.$lacotaskdate->id."/".$item2->laco_staff_id) }}">X</a>
                                                                        @endif
                                                                    </div>
                                                                @endif      
                                                            @endforeach
                                    </div>
                                </div>
                                                @endforeach   
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
