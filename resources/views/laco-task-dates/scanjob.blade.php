@extends('layouts.appscan')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Scan เข้างานของ {{ $lacostaff->name }} {{ $lacostaff->maindep->name }}/{{ $lacostaff->shift->name }} งาน#{{ $lacotaskdate->id }}</div>
                    <div class="card-body">
                        @if(Session::has('flash_message'))
                        <div class="alert alert-warning alert-dismissible fade show" role="alert">
                        {{ Session::get('flash_message') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        </div>
                        @endif
                        <div class="row">
                            <div class="col-md-4">
                                <form method="GET" action="{{ url('/laco-task-dates/scanjob/'.$lacotaskdate->id."/".$lacostaff->id) }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">                            
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="lacojobcode" placeholder="Scan รหัสงาน ..." autofocus value="{{ request('lacojobcode') }}">
                                        <span class="form-group-append">
                                            <button class="btn btn-secondary" type="submit">
                                                <i class="fa fa-search"></i>
                                            </button>
                                        </span>
                                    </div>
                                </form>
                             </div>
                            <div class="col-md-8">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>วันที่ / กะ</th>
                                                <th>รายการ</th>
                                                <th>สินค้า</th>
                                                <th>แผนจำนวนคน</th>
                                                <th>สถานะ</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>{{ $lacotaskdate->id }}</td>
                                                <td>{{ $lacotaskdate->process_date }} / {{ $lacotaskdate->shift->name }}</td>
                                                <td>{{ $lacotaskdate->lacotask->name or '' }}</td>
                                                <td>{{ $lacotaskdate->lacotask->product or ''  }}</td>
                                                <td>{{ $lacotaskdate->plan_staff or ''  }}</td>
                                                <td>{{ $lacotaskdate->status }}</td>
                                            </tr>
                                        </tbody>
                                    </table>   
                                </div>
                            </div>


                            <div class="col-md-12">
                                <a href="{{ url('/laco-task-dates/scanjob/'.$lacotaskdate->id."/".$lacostaff->id."?lacojobcode=non") }}" title="Edit LacoTaskDate"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> ไม่มีงาน</button></a> 
                                @foreach ($taskjobArr as $key=>$itemObj)
                                <div class="row" style="border: solid 1px black; padding: 1px;" >
                                    

                                    <div class="col-md-3">
                                        @if ($itemObj[0]->lacotaskjob->main_code != 'share')
                                        <a href="{{ url('/laco-task-dates/scanjob/'.$lacotaskdate->id."/".$lacostaff->id."?lacojobcode=".$itemObj[0]->lacotaskjob->main_code) }}" title="Edit LacoTaskDate"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> {{ $key }}</button></a>    
                                    @endif  
                                    </div>
                                    <div class="col-md-9">
                                        @foreach ($itemObj as $item2)
                                                            
                                                                @if (!empty($item2->laco_staff_id))
                                                                    <div  class="col-md-3 bg-success" style="border: solid 1px black; height : 50px;">
                                                                        {{ $item2->lacotaskpos->position_code or 'เติม'}}<br/> 
                                                                        {{ $item2->lacostaff->name or 'ไม่ได้ตั้งคน' }}
                                                                        @if (!empty($item2->laco_staff_id))
                                                                            <a href="{{ url('/laco-task-dates/removestaffjob/'.$lacotaskdate->id."/".$item2->laco_staff_id) }}">X</a>
                                                                        @endif
                                                                    </div>
                                                                @else
                                                                    <div  class="col-md-3 bg-primary" style="border: solid 1px black; height : 50px;">
                                                                        {{ $item2->lacotaskpos->position_code or 'เติม'}}<br/>
                                                                        {{ $item2->lacostaff->name or 'ไม่ได้ตั้งคน' }}
                                                                        @if (!empty($item2->laco_staff_id))
                                                                            <a href="{{ url('/laco-task-dates/removestaffjob/'.$lacotaskdate->id."/".$item2->laco_staff_id) }}">X</a>
                                                                        @endif
                                                                    </div>
                                                                @endif      
                                                            @endforeach
                                    </div>
                                </div>
                                                @endforeach   
                                
                            </div>

                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
