 <div class="col-md-6 {{ $errors->has('process_date') ? 'has-error' : ''}}">
                                    <label for="process_date" class="control-label">{{ 'วันที่ผลิต' }}</label>
                                    <input class="form-control" name="process_date" type="date" id="process_date" required 
                                    @if (isset($lacotaskdate->process_date))
                                        value="{{$lacotaskdate->process_date}}"
                                    @endif
                                    >
                                    {!! $errors->first('process_date', '<p class="help-block">:message</p>') !!}
                                </div>


<div class="col-md-6  {{ $errors->has('stamp_m_id') ? 'has-error' : ''}}">
        {!! Form::label('stamp_m_id', 'Type', ['class' => 'control-label']) !!}
        @if (isset($lacotaskdate->stamp_m_id))
            {!! Form::select('stamp_m_id', $stamplist,$lacotaskdate->stamp_m_id, ['class' => 'form-control','placeholder'=>'==เลือก==']) !!}   
        @else
            {!! Form::select('stamp_m_id', $stamplist,null, ['class' => 'form-control','placeholder'=>'==เลือก==']) !!}
        @endif
        {!! $errors->first('stamp_m_id', '<p class="help-block">:message</p>') !!}
</div>

<div class="col-md-12  {{ $errors->has('desc') ? 'has-error' : ''}}">
    <label for="desc" class="control-label">{{ 'รายละเอียด' }}</label>
    <textarea class="form-control" name="desc" type="text" id="desc">{{ $lacotaskdate->desc or ''}}</textarea>
    {!! $errors->first('desc', '<p class="help-block">:message</p>') !!}
</div>



<div class="col-md-6  form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
