@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Lacotaskdates</div>
                    <div class="card-body">
                        <a href="{{ url('/laco-task-dates/create') }}" class="btn btn-success btn-sm" title="Add New LacoTaskDate">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>

                        <form method="GET" action="{{ url('/laco-task-dates') }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                            <div class="input-group">
                                <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
                                <span class="input-group-append">
                                    <button class="btn btn-secondary" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </form>

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>วันที่ / กะ</th>
                                        <th>Job / Product</th>
                                        <th>งาน</th>
                                        <th>แผนกำลังคน</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($lacotaskdates as $item)
                                    <tr>
                                        <td>{{ $item->id }}</td>
                                         <td>{{ $item->process_date }} / {{ $item->shift->name }}</td>
                                          <td>{{ $item->lacotask->name or '' }} / {{ $item->lacotask->product or '' }}</td>
                                          <td>{{ $item->stampm->matpack->matname or '' }}</td> 
                                          <td>{{ $item->plan_staff }}</td>
                                            <td>@if ($item->status == 'Start')
                                                <a href="{{ url('/laco-task-dates/changestatus/' . $item->id.'/process') }}" title="View LacoTaskDate"><button class="btn btn-primary btn-sm"><i class="fa fa-play" aria-hidden="true"></i> รอเริ่มงาน</button></a>                                                                                        
                                            @else
                                                @if ($item->status == 'Create')
                                                <a href="{{ url('/laco-task-dates/scanstaff/' . $item->id) }}" title="View LacoTaskDate"><button class="btn btn-primary btn-sm"><i class="fa fa-users" aria-hidden="true"></i> รอจัดคน</button></a>                                                                                        
                                                @else
                                                    @if ($item->status == 'process')
                                                    <a href="{{ url('/laco-task-dates/scanprocess/' . $item->id) }}" title="View LacoTaskDate"><button class="btn btn-primary btn-sm"><i class="fa fa-barcode" aria-hidden="true"></i> จัดการการทำงาน</button></a>                                                                                        
                                                    
                                                     <a href="{{ url('/laco-task-dates/scaninout/' . $item->id) }}" title="View LacoTaskDate"><button class="btn btn-primary btn-sm"><i class="fa fa-barcode" aria-hidden="true"></i> Scan Only</button></a>                                                                                        
                                                    <a href="{{ url('/laco-task-dates/changestatus/' . $item->id.'/END') }}" title="View LacoTaskDate"><button class="btn btn-primary btn-sm"><i class="fa fa-stop" aria-hidden="true"></i> จบงาน</button></a>    
                                                    @else
                                                        {{ $item->status }}
                                                    @endif
                                                @endif
                                            @endif
                                                
                                            </td>
                                        <td>
                                           
                                            <a href="{{ url('/laco-task-dates/scanstaff/' . $item->id) }}" title="View LacoTaskDate"><button class="btn btn-info btn-sm"><i class="fa fa-search" aria-hidden="true"></i> Scan</button></a>                                            
                                            <a href="{{ url('/laco-task-dates/dashboard/' . $item->id) }}" title="View LacoTaskDate"><button class="btn btn-info btn-sm"><i class="fa fa-search" aria-hidden="true"></i> Dashboard</button></a>                                            
                                           
                                            <a href="{{ url('/laco-task-dates/' . $item->id) }}" title="View LacoTaskDate"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a>
                                            <a href="{{ url('/laco-task-dates/' . $item->id . '/edit') }}" title="Edit LacoTaskDate"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>


                                             <a href="{{ url('/Process/job/' . $item->id) }}" title="View LacoTaskDate" target='_blank'><button class="btn btn-primary btn-sm"><i class="fa fa-search" aria-hidden="true"></i> Job</button></a>                                            
                                            <a href="{{ url('Process/timeline/' . $item->id) }}" title="View LacoTaskDate" target='_blank'><button class="btn btn-primary btn-sm"><i class="fa fa-search" aria-hidden="true"></i> Timeline</button></a>                                            
                                           
                                            <form method="POST" action="{{ url('/laco-task-dates' . '/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-danger btn-sm" title="Delete LacoTaskDate" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                                            </form>
                                            @if($item->status=='END')
                                                <a href="{{ url('/laco-task-dates/export/' . $item->id) }}" title="View LacoTaskDate" target='_blank'><button class="btn btn-primary btn-sm">
                                                    <i class="fa fa-table" aria-hidden="true"></i> Excel</button>
                                                </a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $lacotaskdates->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
