@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Generate New LacoTaskDate</div>
                    <div class="card-body">
                        <a href="{{ url('/laco-tasks') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <br />
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>รายการ</th>
                                        <th>สินค้า</th>
                                        <th>แผนพนักงาน</th>
                                    </tr>                              
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>{{ $lacotask->id }}</td>
                                        <td>{{ $lacotask->name }}</td>
                                        <td>{{ $lacotask->product }}</td>
                                        <td>{{ $lacotask->plan_staff }}</td>
                                    </tr>                           
                                </tbody>
                            </table>
                    </div>

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        <form method="POST" action="{{ url('/laco-task-dates/generatePlanAction/'.$lacotask->id) }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            
                                <div class="col-md-4 {{ $errors->has('process_date') ? 'has-error' : ''}}">
                                    <label for="process_date" class="control-label">{{ 'วันที่ผลิต' }}</label>
                                    <input class="form-control" name="process_date" type="date" id="process_date" required >
                                    {!! $errors->first('process_date', '<p class="help-block">:message</p>') !!}
                                </div>

                                <div class="col-md-4  {{ $errors->has('shift_id') ? 'has-error' : ''}}">
        {!! Form::label('shift_id', 'กะ', ['class' => 'control-label']) !!}

            {!! Form::select('shift_id', $shiftlist,null, ['class' => 'form-control']) !!}
        {!! $errors->first('shift_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-4  {{ $errors->has('plan_staff') ? 'has-error' : ''}}">
    <label for="plan_staff" class="control-label">{{ 'แผนกำลังคน' }}</label>
    <input class="form-control" name="plan_staff" type="number" id="plan_staff" required value="{{ $lacotask->plan_staff or '0'}}" >
    {!! $errors->first('plan_staff', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-12  {{ $errors->has('desc') ? 'has-error' : ''}}">
    <label for="desc" class="control-label">{{ 'รายละเอียด' }}</label>
    <textarea class="form-control" name="desc" type="text" id="desc"></textarea>
    {!! $errors->first('desc', '<p class="help-block">:message</p>') !!}
</div>
                            
                            <div class="form-group col-md-12">
                                <input class="btn btn-primary" type="submit" value="สร้างแผน">
                            </div>

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
