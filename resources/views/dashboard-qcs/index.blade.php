@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">ข้อมูลผู้มาทำงานของ QC</div>
                    <div class="card-body">
                        
                        <form method="GET" action="{{ url('/DashboardQcs/index') }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                            <div class="row"> <input type="date" class="form-control" name="date" placeholder="Search..." value="{{ request('date') }}">
                                
                                <span class="input-group-append">
                                    <button class="btn btn-secondary" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                                <a href="{{ url('/DashboardQcs/index') }}" title="Back"><button class="btn btn-warning">Clear</button></a>
                            </div>
                        </form>

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>วันที่</th>
                                        <th>จำนวนคนมา</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($qcprocesslogs as $item)
                                    <tr>
                                        <td>{{ $item->log_date }}</td>
                                        <td>{{ $item->numberactive }}</td>
                                        <td>
                                            <a href="{{ url('/DashboardQcs/view/' . $item->log_date) }}" title="View planm"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a>
                                            <a href="{{ url('/DashboardQcs/mapview/' . $item->log_date . "/1") }}" title="View planm"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> MAP B</button></a>
                                            <a href="{{ url('/DashboardQcs/mapview/' . $item->log_date . "/5") }}" title="View planm"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> MAP C</button></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $qcprocesslogs->appends(['search' => Request::get('search'),'date' => Request::get('date')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
