@extends('layouts.graph')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                <div class="card-header">รายการพันกงานมาทำงานของ QC ประจำวันที่ {{ $date }}</div>
                    <div class="card-body">
                           <a href="{{ url('/DashboardQcs/index') }}" title="Back"><button class="btn btn-warning">Back</button></a>
                           
                        <div class="row">
                            @php
        $loopname = 0;
        @endphp
                            @foreach ($qcprocesslogs as $pos=>$itemx)
                            @php
        $loopname++;
        @endphp
                            @foreach ($itemx as $pos2=>$item2)
                                <div  class="col-md-4" id="piechart_{{$pos2}}_{{$loopname}}" style="height: 200px;"></div>
                            @endforeach
                            @endforeach
                            
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
        @php
        $loopname = 0;
        @endphp
        @foreach ($qcprocesslogs as $pos=>$itemx)
        @php
        $loopname++;
        @endphp
        @foreach ($itemx as $pos2=>$item2)
        google.charts.setOnLoadCallback(drawChart_{{$pos2}}_{{$loopname}});
        function drawChart_{{$pos2}}_{{$loopname}}() {

        var data = google.visualization.arrayToDataTable([
          ['สถานะ', 'จำนวน'],
          ['มาทำงาน', @if (isset($item2['Active'])) {{ $item2['Active'] }} @else 0 @endif],
          ['ไม่มาทำงาน', @if (isset($item2['Absent'])) {{ $item2['Absent'] }} @else 0 @endif]
        ]);

        var options = {
          title: '{{ $shiftlist[$pos] }} / {{ $pos2 }} '
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart_{{$pos2}}_{{$loopname}}'));

        chart.draw(data, options);

        google.visualization.events.addListener(chart, 'select', function(){
    // grab a few details before redirecting
        
        location.href = '/manpower/DashboardQcs/viewdetail/{{ $pos2 }}/{{ $pos }}/{{ $date }}';
      });  
      }

        @endforeach
        @endforeach

      
    </script>
@endsection