@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">ข้อมูลผู้มาทำงานของ QC</div>
                    <div class="card-body">
                            <a href="{{ url('/DashboardQcs/view/'.$date) }}" title="Back"><button class="btn btn-warning">ย้อนกลับ</button></a>
                          
                        
                        <br/>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>รหัส</th>
                                        <th>ชื่อ</th>
                                        <th>ตำแหน่ง</th>
                                        <th>ประเภท</th>
                                        <th>สภานะ</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($qcprocesslogsRaw as $item)
                                    <tr>
                                        <td>{{ $item->lacocode }}</td>
                                        <td>{{ $item->name }}</td>
                                        <td>{{ $item->pos }} / {{ $item->pos_code }}</td>
                                        <td>{{ $typearr[$item->type] }}</td>
                                        <td>{{ $item->status }} </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
