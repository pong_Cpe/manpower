@extends('layouts.graph')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                <div class="card-header">รายการพันกงานมาทำงานของ QC ประจำวันที่ {{ $date }}</div>
                    <div class="card-body">
                           <a href="{{ url('/DashboardQcs/index') }}" title="Back"><button class="btn btn-warning">Back</button></a>
                           
                        <div class="row">
                           <img id="mainmap" width="1000" height="540" src="/manpower/img/main_map.png" alt="The Scream" style="display: none;">
                           <canvas id="myCanvas" width="1000" height="540"/>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        window.onload = function() {
            //1000 x 600
            var rate_r= 40;
            var canvas = document.getElementById("myCanvas");
            var ctx = canvas.getContext("2d");
            var img = document.getElementById("mainmap");
            ctx.drawImage(img, 0, 0);
            
            ctx.moveTo(550, 0);
            ctx.lineTo(550, 600);
            ctx.moveTo(0, 450);
            ctx.lineTo(1000, 450);
            //ctx.stroke();

            //QCRM
            ctx.beginPath();
            ctx.arc(150, 200, rate_r*
            @php
                if(isset($qcprocesslogs[$shiftid]['QCRM'])){
                    $mapdata = $qcprocesslogs[$shiftid]['QCRM'];
                    if(isset($mapdata['Absent']) && isset($mapdata['Active'])){
                        echo $mapdata['Active']/($mapdata['Active'] + $mapdata['Absent']);
                    }elseif(isset($mapdata['Absent'])){
                        echo 0;
                    }elseif(isset($mapdata['Active'])){
                        echo 1;
                    }else{
                        echo 0;
                    }
                }else{
                    echo 0;
                }
            @endphp
            , 0, 2 * Math.PI);
            ctx.fillStyle = "#AED6F1";
            ctx.fill();
            ctx.strokeStyle = "#AED6F1";
            ctx.stroke();
            ctx.font = "30px Arial";
            ctx.fillStyle = "black";
            ctx.fillText("@php
                if(isset($qcprocesslogs[$shiftid]['QCRM'])){
                    $mapdata = $qcprocesslogs[$shiftid]['QCRM'];
                    if(isset($mapdata['Absent']) && isset($mapdata['Active'])){
                        echo $mapdata['Active'];
                    }elseif(isset($mapdata['Absent'])){
                        echo 0;
                    }elseif(isset($mapdata['Active'])){
                        echo $mapdata['Active'];
                    }else{
                        echo 0;
                    }
                }else{
                    echo 0;
                }
            @endphp", 140, 210);

            //QCPF
            ctx.beginPath();
            ctx.arc(320, 100, rate_r*
            @php
                if(isset($qcprocesslogs[$shiftid]['QCPF'])){
                    $mapdata = $qcprocesslogs[$shiftid]['QCPF'];
                    if(isset($mapdata['Absent']) && isset($mapdata['Active'])){
                        echo $mapdata['Active']/($mapdata['Active'] + $mapdata['Absent']);
                    }elseif(isset($mapdata['Absent'])){
                        echo 0;
                    }elseif(isset($mapdata['Active'])){
                        echo 1;
                    }else{
                        echo 0;
                    }
                }else{
                    echo 0;
                }
            @endphp
            , 0, 2 * Math.PI);
            ctx.fillStyle = "#EDBB99";
            ctx.fill();
            ctx.strokeStyle = "#EDBB99";
            ctx.stroke();
            ctx.font = "30px Arial";
            ctx.fillStyle = "black";
            ctx.fillText("@php
                if(isset($qcprocesslogs[$shiftid]['QCPF'])){
                    $mapdata = $qcprocesslogs[$shiftid]['QCPF'];
                    if(isset($mapdata['Absent']) && isset($mapdata['Active'])){
                        echo $mapdata['Active'];
                    }elseif(isset($mapdata['Absent'])){
                        echo 0;
                    }elseif(isset($mapdata['Active'])){
                        echo $mapdata['Active'];
                    }else{
                        echo 0;
                    }
                }else{
                    echo 0;
                }
            @endphp", 310, 110);

            //QCPK
            ctx.beginPath();
            ctx.arc(700, 100, rate_r*
            @php
                if(isset($qcprocesslogs[$shiftid]['QCPK'])){
                    $mapdata = $qcprocesslogs[$shiftid]['QCPK'];
                    if(isset($mapdata['Absent']) && isset($mapdata['Active'])){
                        echo $mapdata['Active']/($mapdata['Active'] + $mapdata['Absent']);
                    }elseif(isset($mapdata['Absent'])){
                        echo 0;
                    }elseif(isset($mapdata['Active'])){
                        echo 1;
                    }else{
                        echo 0;
                    }
                }else{
                    echo 0;
                }
            @endphp
            , 0, 2 * Math.PI);
            ctx.fillStyle = "#F9E79F";
            ctx.fill();
            ctx.strokeStyle = "#F9E79F";
            ctx.stroke();
            ctx.font = "30px Arial";
            ctx.fillStyle = "black";
            ctx.fillText("@php
                if(isset($qcprocesslogs[$shiftid]['QCPK'])){
                    $mapdata = $qcprocesslogs[$shiftid]['QCPK'];
                    if(isset($mapdata['Absent']) && isset($mapdata['Active'])){
                        echo $mapdata['Active'];
                    }elseif(isset($mapdata['Absent'])){
                        echo 0;
                    }elseif(isset($mapdata['Active'])){
                        echo $mapdata['Active'];
                    }else{
                        echo 0;
                    }
                }else{
                    echo 0;
                }
            @endphp", 690, 110);

            //QCPST
            
            ctx.beginPath();
            
            ctx.arc(350, 300, rate_r*
            @php
                if(isset($qcprocesslogs[$shiftid]['QCPST'])){
                    $mapdata = $qcprocesslogs[$shiftid]['QCPST'];
                    if(isset($mapdata['Absent']) && isset($mapdata['Active'])){
                        echo $mapdata['Active']/($mapdata['Active'] + $mapdata['Absent']);
                    }elseif(isset($mapdata['Absent'])){
                        echo 0;
                    }elseif(isset($mapdata['Active'])){
                        echo 1;
                    }else{
                        echo 0;
                    }
                }else{
                    echo 0;
                }
            @endphp
            , 0, 2 * Math.PI);
            ctx.fillStyle = "#E6B0AA";
            ctx.fill();
            ctx.strokeStyle = "#E6B0AA";
            ctx.stroke();
            ctx.font = "30px Arial";
            ctx.fillStyle = "black";
            ctx.fillText( "@php
                if(isset($qcprocesslogs[$shiftid]['QCPST'])){
                    $mapdata = $qcprocesslogs[$shiftid]['QCPST'];
                    if(isset($mapdata['Absent']) && isset($mapdata['Active'])){
                        echo $mapdata['Active'];
                    }elseif(isset($mapdata['Absent'])){
                        echo 0;
                    }elseif(isset($mapdata['Active'])){
                        echo $mapdata['Active'];
                    }else{
                        echo 0;
                    }
                }else{
                    echo 0;
                }
            @endphp"
                
            , 340, 310);
            ctx.fillStyle = "#AEB6BF";
            ctx.fillRect(130, 70, 120, 60);
            ctx.font = "30px Arial";
            ctx.fillStyle = "black";
            ctx.fillText(
            @php
                if(isset($qcprocesslogs[6])){
                    $active = 0;
                    foreach($qcprocesslogs[6] as $key=>$obj){
                        if(isset($obj['Active'])){
                            $active += $obj['Active'];
                        }
                    }
                    echo $active;
                }else{
                    echo "0";
                }
            @endphp
            , 180, 110);
        };    
    </script>
@endsection