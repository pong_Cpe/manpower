@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                <div class="card-header">สรุปการมาทำงาน {{ $date }} 
                    <a href="{{ url('/planms/processdateagain/' . $date.'/B/dash') }}" title="View planm"><button class="btn btn-primary btn-sm"><i class="fa fa-gears" aria-hidden="true"></i> Remapp B</button></a> | 
                    <a href="{{ url('/planms/processdateagain/' . $date.'/C/dash') }}" title="View planm"><button class="btn btn-primary btn-sm"><i class="fa fa-gears" aria-hidden="true"></i> Remapp C</button></a></div>
                    <div class="card-body">
                        <form method="GET" action="{{ url('/monitors/main/selected') }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                            <div class="input-group">
                                <input type="date" class="form-control" name="date" placeholder="Search..." value="{{ $date }}">
                                <span class="input-group-append">
                                    <button class="btn btn-secondary" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </form>
                        @foreach ($data as $key1=>$item1)
                        <br/>
                        <button type="button" class="btn btn-success btn-lg btn-block"><h3>กะ {{ $shiftlist[$key1] }}</h3></button>
                        <br/>
                        <div class="row">
                            @foreach ($item1 as $key2=>$item2)
                            @if (isset($deplist[$key2]))
                                <div class="col-md-4">
                                    <button type="button" class="btn btn-info btn-lg btn-block"><b>
                                    
                                        {{ $deplist[$key2] }}
                                    </b>
                                    <br/>
                                    มาทำงาน @if (isset($item2['Active']))
                                        {{ sizeof($item2['Active']) }}
                                        
                                        @if (!(isset($deplist[$key2])))
                                       
     
                                   

                                        (@foreach ($item2['Active'] as $ckey=>$itemloop)
                                            {{ $itemloop->id }},
                                        @endforeach)

                                         @endif


                                    @else
                                        0
                                    @endif คน<br/>
                                    <a href="{{ url('/staffs/cometowork/'.$date.'/'.$key1.'/'.$key2) }}">ขาดงาน @if (isset($item2['Absent']))
                                        {{ sizeof($item2['Absent']) }}

                                         @if (!(isset($deplist[$key2])))
                                       
     
                                   
(
                                        @foreach ($item2['Absent'] as $ckey=>$itemloop)
                                            {{ $itemloop->id }},
                                        @endforeach
)
                                         @endif

                                    @else
                                        0
                                    @endif คน</a></button>
                                    <br/>
                                    @if (isset($planmlist[$key1][$key2]))
                                        @foreach ($planmlist[$key1][$key2] as $item)
                                            <a href="{{ url('/planms/dashboard/' . $item->id) }}">{{ $item->name }} [{{ $item->plandate or '' }} / {{ $item->shift->name or '' }}] แผนตั้ง @if ($item->pland->count() > 0)
                                                {{$item->pland->sum('plan_number')}}
                                            @else
                                                0
                                            @endif  คน / มาเข้างาน @if ($item->comewithplanduser->count() > 0)
                                                {{$item->comewithplanduser->count()}}
                                            @else
                                                0
                                            @endif คน</a><br/>
                                        @endforeach
                                    @endif
                                </div>
                            @endif
                            @endforeach                           
                        </div>
                         @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
