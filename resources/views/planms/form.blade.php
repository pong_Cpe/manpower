
                            <div class="row">
                                <div class="col-md-4 {{ $errors->has('name') ? 'has-error' : ''}}">
                                    <label for="name" class="control-label">{{ 'รายการ' }}</label>
                                    <input class="form-control" name="name" type="text" id="name" required value="{{ $planm->name or ''}}" >
                                    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                                </div>
                                <div class="col-md-4 {{ $errors->has('startdatetime') ? 'has-error' : ''}}">
                                    <label for="startdatetime" class="control-label">{{ 'วันเวลาที่เริ่ม' }}</label>
                                    <input class="form-control" name="startdatetime" type="datetime-local" id="startdatetime" required 
                                    value="@php
            if(isset($planm->startdatetime)){
                echo date('Y-m-d\TH:i',strtotime($planm->startdatetime));
            }else{
                echo \Carbon\Carbon::now()->format('Y-m-d\TH:i');
            }
        @endphp" >
                                    {!! $errors->first('startdatetime', '<p class="help-block">:message</p>') !!}
                                </div>
                                <div class="col-md-4 {{ $errors->has('enddatetime') ? 'has-error' : ''}}">
                                    <label for="enddatetime" class="control-label">{{ 'วันเวลาที่เสร็จ' }}</label>
                                    <input class="form-control" name="enddatetime" type="datetime-local" id="enddatetime" required 
                                    value="@php
            if(isset($planm->enddatetime)){
                echo date('Y-m-d\TH:i',strtotime($planm->enddatetime));
            }else{
                echo \Carbon\Carbon::now()->format('Y-m-d\TH:i');
            }
        @endphp" >
                                    {!! $errors->first('enddatetime', '<p class="help-block">:message</p>') !!}
                                </div>
                                <div class="col-md-12  {{ $errors->has('desc') ? 'has-error' : ''}}">
                                    <label for="desc" class="control-label">{{ 'รายละเอียด' }}</label>
                                    <textarea class="form-control" name="desc" type="text" id="desc">{{ $planm->desc or ''}}</textarea>
                                    {!! $errors->first('desc', '<p class="help-block">:message</p>') !!}
                                </div>

<div class="col-md-12">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
