@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">ตารางการผลิต</div>
                    <div class="card-body">
                        
                        <div class="row">
                            @foreach ($mechlist as $key=>$value)
                                <div class="col-md-4" >
                                <a href="{{ url('/planms/getselectscan/' . date('Y-m-d').'/'.$value.'/'.$key) }}" title="View planm"><button class="btn btn-block btn-info">{{ $value }}:{{ $key }}</button></a>         
                                </div>
                            @endforeach
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                @if(session('flash_message'))
                        {{session('flash_message')}}
                        @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection