@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">แก้ไขจำนวนคนในงานที่วางแผน {{ $pland->job->name }} #{{ $pland->id }}</div>
                    <div class="card-body">
                        <a href="{{ url('/main-deps') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        <form method="POST" action="{{ url('/planms/editdetailAction/' . $pland->id) }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">

                            {{ csrf_field() }}

                           <div class="row">
                                <div class="col-md-4 {{ $errors->has('plan_number') ? 'has-error' : ''}}">
                                    <label for="plan_number" class="control-label">{{ 'จำนวน' }}</label>
                                    <input class="form-control" name="plan_number" type="number" id="plan_number" required value="{{ $pland->plan_number or '0'}}" >
                                    {!! $errors->first('plan_number', '<p class="help-block">:message</p>') !!}
                                </div>
                                <div class="col-md-12">
    <input class="btn btn-primary" type="submit" value="Update">
</div>
                           </div>

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
