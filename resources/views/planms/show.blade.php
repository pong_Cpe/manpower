@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">planm {{ $planm->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/planms') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/planms/' . $planm->id . '/edit') }}" title="Edit planm"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                        <form method="POST" action="{{ url('planms' . '/' . $planm->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete planm" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                        </form>
                        <br/>
                        <br/>

                        <div class="row">
                            <div class="col-md-6" >
                                <b>Template ID</b> : {{ $planm->templatem->id }} , 
                                <b>Department</b> : {{ $planm->templatem->department->name }} , 
                                <b>name</b> : {{ $planm->templatem->name }}
                            </div>
                            <div class="col-md-6" >
                                {{ $planm->templatem->desc }}
                            </div>
                            <div class="col-md-4" >
                                <b>ID</b> : {{ $planm->id }}, 
                                <b>name</b> : {{ $planm->name }}
                            </div>
                            <div class="col-md-4" >
                                {{ $planm->plandate }} / {{ $planm->shift->name }}
                            </div>
                            <div class="col-md-4" >
                                {{ $planm->desc }}
                            </div>
                        </div>

                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Job</th>
                                        <th>Plan</th>
                                        <th>Act</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($planm->pland as $item)
                                    <tr>
                                    <td>{{ $item->job->name }}</td>
                                    <td>{{ $item->plan_number }}</td>
                                    <td>{{ $item->available_planduser->count() }}</td>
                                    <td>
                                        @if ($item->planduser->count() == 0)
                                            <a href="{{ url('/planms/editdetail/' . $item->id ) }}" title="Edit planm"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                                        @endif
                                            </td>
                                    </tr>    
                                    @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
