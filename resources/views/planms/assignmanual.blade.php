@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Assign คนงาน {{ $planDUserObj->pland->job->name }} #{{ $planDUserObj->planm->name }}</div>
                   
                    <div class="card-body">
                        <a href="{{ url('/planms') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        <form method="POST" action="{{ url('/planms/assignmanualaction/' . $planDUserObj->id) }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                            <div class="row">
                                {{ csrf_field() }}
                            <div class="form-group col-md-9 {{ $errors->has('staff_id') ? 'has-error' : ''}}">
                                <label for="staff_id" class="control-label">{{ 'พนักงาน' }}</label>
                                <select name="staff_id" class="form-control dynamic" id="staff_id" required>
                                    <option value="">-</option>
                                    @if (isset($planDUserObj->staff->name))
                                    <option value="{{ $planDUserObj->staff->id }}" selected >{{ $planDUserObj->staff->name }}</option>    
                                    @endif
                                @foreach ($staffAvailableList as $optionKey => $optionValue)
                                    <option value="{{ $optionKey }}" {{ (isset($planDUserObj->staff_id) && $planDUserObj->staff_id == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
                                @endforeach
                                </select>
                                {!! $errors->first('staff_id', '<p class="help-block">:message</p>') !!}
                            </div>
                            </div>
                            <div class="row">
                            <div class="col-md-12">
    <input class="btn btn-primary" type="submit" value="Update">
</div>
</div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
