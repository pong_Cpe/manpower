@extends('layouts.app')

@section('content')
<div class="container">
        <div class="row">
            <div class="col-md-12">

<div class="row"><a href="{{ url('/planms') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                       
  
                            <div class="col-md-12" ><h3><b>Template ID</b> : {{ $planm->templatem->id }} , 
                                <b>Department</b> : {{ $planm->templatem->department->name }} , 
                                <b>name</b> : {{ $planm->templatem->name }}
                                </h3>
                                {{ $planm->templatem->desc }}
                            </div>
                            <div class="col-md-12" >
                                <h4><b>ID</b> : {{ $planm->id }}, 
                                <b>name</b> : {{ $planm->name }} <b>วัน กะ</b> : {{ $planm->plandate }} / {{ $planm->shift->name }}</h4> {{ $planm->desc }}
                            </div>
                            
                        </div>

<div id="accordion">
     @foreach ($planm->pland as $item) 
                                    
                                    <div class="card">
    <div class="card-header" id="heading{{ $loop->iteration }}">
    <h5 class="mb-{{ $loop->iteration }}">
        @if (sizeof($item->available_planduser) == 0)
            <button class="btn btn-danger" data-toggle="collapse" data-target="#collapse{{ $loop->iteration }}" aria-expanded="true" aria-controls="collapseOne">
          <b>{{ $item->job->name }}</b> <br/>จำนวน {{ $item->plan_number }} คน จัดคนได้ {{ sizeof($item->available_planduser) }} คน
            </button>
        @else
            
        
        @if ($item->plan_number == sizeof($item->available_planduser))
            @if (sizeof($item->comtowork_planduser) == sizeof($item->available_planduser) )
                <button class="btn btn-info" data-toggle="collapse" data-target="#collapse{{ $loop->iteration }}" aria-expanded="true" aria-controls="collapseOne">
                <b>{{ $item->job->name }}</b> <br/>จำนวน {{ $item->plan_number }} คน จัดคนได้ {{ sizeof($item->available_planduser) }} คน คนมาทำงานแล้ว {{ sizeof($item->comtowork_planduser) }} คน
                </button>
            @else
                <button class="btn btn-warning" data-toggle="collapse" data-target="#collapse{{ $loop->iteration }}" aria-expanded="true" aria-controls="collapseOne">
                <b>{{ $item->job->name }}</b> <br/>จำนวน {{ $item->plan_number }} คน จัดคนได้ {{ sizeof($item->available_planduser) }} คน คนมาทำงานแล้ว {{ sizeof($item->comtowork_planduser) }} คน
                </button>
            @endif    

            
        @else
            <button class="btn btn-primary" data-toggle="collapse" data-target="#collapse{{ $loop->iteration }}" aria-expanded="true" aria-controls="collapseOne">
          <b>{{ $item->job->name }}</b> <br/>จำนวน {{ $item->plan_number }} คน จัดคนได้ {{ sizeof($item->available_planduser) }} คน คนมาทำงานแล้ว {{ sizeof($item->comtowork_planduser) }} คน
        </button>
        @endif
        @endif
      </h5>
    </div>

    <div id="collapse{{ $loop->iteration }}" class="collapse" aria-labelledby="heading{{ $loop->iteration }}" data-parent="#accordion">
      <div class="card-body">
            <div class="row">
            @php
                $loopcount = 1;
            @endphp
            @foreach ($item->planduser as $itemstaff)
                <div class="col-md-2">
                    @if (isset($itemstaff->staff->name))
                      @if ($itemstaff->status == 'Active')
                          <button class="btn btn-success btn-sm  btn-block" title="{{ $itemstaff->id }}">{{ $itemstaff->staff->name or 'Empty'}}</button>
                      @else
                          <a href="{{ url('/planms/assignmanual/' . $itemstaff->id ) }}" title="{{ $itemstaff->id }}"><button class="btn btn-primary btn-sm  btn-block">{{ $itemstaff->staff->name or 'Empty'}}</button></a>
                      @endif
                    @else
                         <a href="{{ url('/planms/assignmanual/' . $itemstaff->id ) }}" title="{{ $itemstaff->id }}"><button class="btn btn-danger btn-sm  btn-block">Empty</button></a>
                    @endif
                    </div>
                @php
                    $loopcount++;
                @endphp
            @endforeach    
            </div>
            
      </div>
    </div>
  </div>

                                    @endforeach

  
</div>

</div>
  </div>
</div>
@endsection