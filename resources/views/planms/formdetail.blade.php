
<div class="col-md-4  {{ $errors->has('job_id') ? 'has-error' : ''}}">
        {!! Form::label('job_id', 'Job', ['class' => 'control-label']) !!}
        @if (isset($templated->job_id))
            {!! Form::select('job_id', $joblist,$templated->job_id, ['class' => 'form-control']) !!}   
        @else
            {!! Form::select('job_id', $joblist,null, ['class' => 'form-control']) !!}
        @endif
        {!! $errors->first('job_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-4  {{ $errors->has('plan_number') ? 'has-error' : ''}}">
    <label for="plan_number" class="control-label">{{ 'จำนวนคน' }}</label>
    <input class="form-control" name="plan_number" type="number" id="plan_number" required value="{{ $templated->plan_number or ''}}" >
    {!! $errors->first('plan_number', '<p class="help-block">:message</p>') !!}
</div>

<div class="col-md-12">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
