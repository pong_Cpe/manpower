@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">สร้างแผนการผลิตจากแม่แบบ {{ $templatem->id }}</div>
                    <div class="card-body">
                        <br/>

                        <a href="{{ url('/templatems') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/templatems/' . $templatem->id . '/edit') }}" title="Edit templatem"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                        <form method="POST" action="{{ url('templatems' . '/' . $templatem->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete templatem" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                        </form>
                        <br/>
                        <br/>
                        <div class="row">
                            <div class="col-md-6" >
                                <b>ID</b> : {{ $templatem->id }} , 
                                <b>แผนก</b> : {{ $templatem->department->name }} , 
                                <b>รายการ</b> : {{ $templatem->name }}
                            </div>
                            <div class="col-md-6" >
                                <b>รายละเอียด</b> : {{ $templatem->desc }}
                            </div>
                        </div>
                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        <form method="POST" action="{{ url('/planms/template2PlanAction/' . $templatem->id) }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-md-4 {{ $errors->has('name') ? 'has-error' : ''}}">
                                    <label for="name" class="control-label">{{ 'รายการ' }}</label>
                                    <input class="form-control" name="name" type="text" id="name" required value="{{ $planm->name or ''}}" >
                                    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                                </div>
                                <div class="col-md-4 {{ $errors->has('plandate') ? 'has-error' : ''}}">
                                    <label for="plandate" class="control-label">{{ 'วันเวลาที่เริ่ม' }}</label>
                                    <input class="form-control" name="plandate" type="date" id="plandate" required 
                                value="{{ $planm->plandate or \Carbon\Carbon::now()->format('Y-m-d') }}" >
                                    {!! $errors->first('plandate', '<p class="help-block">:message</p>') !!}
                                </div>

                                <div class="col-md-4  {{ $errors->has('shift_id') ? 'has-error' : ''}}">
        {!! Form::label('shift_id', 'กะ', ['class' => 'control-label']) !!}
        @if (isset($planm->shift_id))
            {!! Form::select('shift_id', $shiftlist,$planm->shift_id, ['class' => 'form-control']) !!}   
        @else
            {!! Form::select('shift_id', $shiftlist,null, ['class' => 'form-control']) !!}
        @endif
        {!! $errors->first('shift_id', '<p class="help-block">:message</p>') !!}
</div>

                                
                                <div class="col-md-12  {{ $errors->has('desc') ? 'has-error' : ''}}">
                                    <label for="desc" class="control-label">{{ 'รายละเอียด' }}</label>
                                    <textarea class="form-control" name="desc" type="text" id="desc">{{ $planm->desc or ''}}</textarea>
                                    {!! $errors->first('desc', '<p class="help-block">:message</p>') !!}
                                </div>
                                <div class="col-md-12">
                                    <input class="btn btn-primary" type="submit" value="สร้างแผนการผลิต">
                                </div>
                            </div>
                        </form>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Department</th>
                                        <th>Job</th>
                                        <th>Number of person</th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    @if ($templatem->templated->count() > 0)
                                        @foreach ($templatem->templated as $subitem)
                                        <tr>
                                            <td>{{ $subitem->job->department->name or ''}}</td>
                                            <td>{{ $subitem->job->name or ''}}</td>
                                            <td>{{ $subitem->default_number }}</td>
                                            
                                        </tr> 
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="4" style="text-align: center;"><h3>ไม่มีข้อมูล</h3></td>
                                        </tr> 
                                    @endif
                                    
                                    
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
