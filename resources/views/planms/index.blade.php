@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">ตารางการผลิต</div>
                    <div class="card-body">
                        
                        <form method="GET" action="{{ url('/planms') }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                            <div class="row">
                                <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
                                 <input type="date" class="form-control" name="date" placeholder="Search..." value="{{ request('date') }}">
                                
                                <span class="input-group-append">
                                    <button class="btn btn-secondary" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                                <a href="{{ url('/planms') }}" title="Back"><button class="btn btn-warning">Clear</button></a>
                            </div>
                        </form>

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>ชื่องาน</th>
                                        <th>Date / Shift</th>
                                        <th>จำนวนคนจากแแผน</th>
                                        <th>จำนวนพนักงานจากแผน</th>
                                        <th>จำนวนพนักงานมาทำงานให้</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($planms as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->name }}</td>
                                        <td>{{ $item->plandate or '' }} / {{ $item->shift->name or '' }}</td>
                                        <td>
                                            @if ($item->pland->count() > 0)
                                                {{$item->pland->sum('plan_number')}}
                                            @else
                                                0
                                            @endif    
                                        </td>
                                        <td>@if ($item->available_planduser->count() > 0)
                                                {{$item->available_planduser->count()}}
                                            @else
                                                0
                                            @endif 
                                        </td>
                                        <td>@if ($item->comewithplanduser->count() > 0)
                                                {{$item->comewithplanduser->count()}}
                                            @else
                                                0
                                            @endif 
                                        </td>
                                        <td>{{ $item->status }}</td>
                                        <td>
                                            @if ($item->status == 'Mapped')
                                                <a href="{{ url('/planms/clearmap/' . $item->id) }}" title="View planm"><button class="btn btn-danger btn-sm"><i class="fa fa-trash" aria-hidden="true"></i> Clear Map</button></a>
                                                <a href="{{ url('/planms/processdateagain/' . $item->plandate.'/'.$item->shift->name.'/planms') }}" title="View planm"><button class="btn btn-primary btn-sm"><i class="fa fa-gears" aria-hidden="true"></i> Scan Check</button></a>
                                            @else
                                                <a href="{{ url('/planms/remap/' . $item->id) }}" title="View planm"><button class="btn btn-primary btn-sm"><i class="fa fa-gears" aria-hidden="true"></i> Re Map</button></a>
                                                <a href="{{ url('/planms/remapall/' . $item->plandate.'/'.$item->shift->name) }}" title="View planm"><button class="btn btn-primary btn-sm"><i class="fa fa-gears" aria-hidden="true"></i> Re Map All</button></a>
                                       
                                            @endif

                                            <a href="{{ url('/planms/dashboard/' . $item->id) }}" title="View planm"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> Dashboard</button></a>
                                            
                                            <a href="{{ url('/planms/' . $item->id) }}" title="View planm"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a>
                                            <a href="{{ url('/planms/' . $item->id . '/edit') }}" title="Edit planm"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                                            <form method="POST" action="{{ url('/planms' . '/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-danger btn-sm" title="Delete planm" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $planms->appends(['search' => Request::get('search'),'date' => Request::get('date')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
