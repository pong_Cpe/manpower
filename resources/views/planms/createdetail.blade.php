@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">เพิ่มงาน</div>
                    <div class="card-body">
                        <a href="{{ url('/templatems/'.$planm->id) }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <br />
                        <br />
                        <div class="row">
                            <div class="col-md-4" >
                                ID : {{ $planm->templatem->id }} , 
                                Department : {{ $planm->templatem->department->name }} , 
                                name : {{ $planm->templatem->name }}
                            </div>
                            <div class="col-md-8" >
                                desc : {{ $planm->templatem->desc }}
                            </div>
                        </div>
                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        <form method="POST" action="{{ url('/planms/createdetailAction/'.$planm->id) }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                            {{ csrf_field() }}

                            @include ('planms.formdetail', ['formMode' => 'create'])

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
