<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PODATA</title>
    <style>
    tr th {
        border: 1px solid #000000;
        word-wrap: normal;
    }
    tr th.noborder {
        border: none;
        word-wrap: normal;
    }
     tr th.noborder-last {
        border: none;
        word-wrap: normal;
    }
    tr th.noborderr {
        border: none;
        text-align: right;
        word-wrap: break-word;
    }
    tr th.noborderc {
        border: none;
        text-align: center;
        word-wrap: break-word;
        font: bolder;
    }
    tr td {
        border: 1px solid #000000;
        word-wrap: normal;
    }
    </style>
</head>
<body>
    <table>
        <thead>
            <tr>
                <th colspan="{{ 6+count($use_job) }}">วันที่ : {{ $task_main[0]['process_date'] }} 
                    ( กะ {{ $task_main[0]['shift_name'] }} ) 
                    Job: {{ $task_main[0]['name'] }} 
                    Product: {{ $task_main[0]['product'] }}
                    แผนกำลังคน: {{ $task_main[0]['plan_staff'] }}</th>
            </tr>
            <tr>
                <th>ที่</th>
                <th>ชื่อ-สกุล</th>
                <th>ชั่วโมงรวม</th>
                <th>ชั่วโมงในห้องรวม</th>                
                <th>ชั่วโมงนอกห้องรวม</th>
                <th>จำนวน scan</th> 
                @foreach ($use_job as $key=>$value)
                    <th>{{ $value }}</th>
                @endforeach
            </tr>
        </thead>
        <tbody>
            @php
                $i=1;
            @endphp
            @foreach($task_data as $key=>$value)
                @if(!empty($key))
                    <tr>
                        <td>{{ $i }}</td>
                        <td>{{ $task_data[$key]['s_name'] }}</td>
                        <td>{{ $task_data[$key]['time_all'] }}</td>
                        <td>@if(!empty($time_work[0][$key])){{ $time_work[0][$key] }}@endif</td>
                        <td>@if(!empty($time_work[1][$key])){{ $time_work[1][$key] }}@endif</td>
                        <td>{{ $task_data[$key]['count_tran'] }}</td>
                        @foreach ($use_job as $kjob=>$vjob)
                            <td>
                                @if(!empty($time[0][$key][$kjob]))
                                    {{ $time[0][$key][$kjob] }}                                
                                @endif
                                {{-- @if(!empty($tran_log[$key][$kjob]))
                                    @foreach ($tran_log[$key][$kjob] as $ktran=>$vtran)
                                        @foreach ($vtran as $kin=>$vin)
                                            @if($kin=='In')
                                                <p style="color:green">-{{ $vin }}-</p>
                                            @elseif($kin=='Out')
                                                <p style="color:red">*{{ $vin }}*</p>
                                            @else
                                                <p>{{ $vin }}</p>
                                            @endif
                                        @endforeach                                        
                                    @endforeach
                                @endif --}}
                            </td>
                        @endforeach                        
                    </tr>
                    @php
                        $i++;
                    @endphp
                @endif
            @endforeach
        </tbody>
    </table>
</body>
</html>