<div class="col-md-3  {{ $errors->has('name') ? 'has-error' : ''}}">
    <label for="name" class="control-label">{{ 'ชื่อ' }}</label>
    <input class="form-control" name="name" type="text" id="name" required value="{{ $shift->name or ''}}" >
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>

<div class="col-md-3  {{ $errors->has('starttime') ? 'has-error' : ''}}">
    <label for="starttime" class="control-label">{{ 'เวลาเริ่ม' }}</label>
    <input class="form-control" name="starttime" type="time" id="starttime" required value="{{ $shift->starttime or ''}}" >
    {!! $errors->first('starttime', '<p class="help-block">:message</p>') !!}
</div>

<div class="col-md-3  {{ $errors->has('endtime') ? 'has-error' : ''}}">
    <label for="endtime" class="control-label">{{ 'เวลาเลิก' }}</label>
    <input class="form-control" name="endtime" type="time" id="endtime" required value="{{ $shift->endtime or ''}}" >
    {!! $errors->first('endtime', '<p class="help-block">:message</p>') !!}
</div>

<div class="col-md-3  {{ $errors->has('overday') ? 'has-error' : ''}}">
        {!! Form::label('overday', 'ข้ามวันหรือไม่', ['class' => 'control-label']) !!}
        @if (isset($shift->overday))
            {!! Form::select('overday', $overdaylist,$shift->overday, ['class' => 'form-control']) !!}   
        @else
            {!! Form::select('overday', $overdaylist,null, ['class' => 'form-control']) !!}
        @endif
        {!! $errors->first('overday', '<p class="help-block">:message</p>') !!}
</div>

<div class="col-md-12  {{ $errors->has('desc') ? 'has-error' : ''}}">
    <label for="desc" class="control-label">{{ 'รายละเอียด' }}</label>
    <textarea class="form-control" name="desc" type="text" id="desc">{{ $shift->desc or ''}}</textarea>
    {!! $errors->first('desc', '<p class="help-block">:message</p>') !!}
</div>

<div class="col-md-12">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
