@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                <div class="card-header">พนักงานที่ไม่แสดงข้อมูลมาทำงาน {{$date}} แผนก {{ $maindepObj->name }} กะ {{ $shiftObj->name }}</div>
                    <div class="card-body">
                        
                        <form method="GET" action="{{ url('/staffs') }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                            <div class="input-group">
                                <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
                                <span class="input-group-append">
                                    <button class="btn btn-secondary" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </form>

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>แผนก / กะ</th>
                                        <th>laco code</th>
                                        <th>Name</th>
                                        <th>สถานะ</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($processLogList as $item)
                                    <tr @if ($item->staff->status == 'Inactive')
                                        style="background-color:red;"
                                    @endif>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->staff->maindep->name or '-' }} / {{ $item->staff->shift->name or '-' }}</td>
                                        <td>{{ $item->staff->lacoid }}</td>
                                        <td>{{ $item->staff->name }}</td>
                                        <td>{{ $item->staff->status }}</td>
                                        <td>
                                            <a href="{{ url('/staffs/manualcome/' . $item->id.'/'.$date.'/'.$item->staff->shift->id.'/'.$item->staff->maindep->id) }}" title="View staff"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> มาทำงาน</button></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
