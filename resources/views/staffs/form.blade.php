<div class="col-md-2  {{ $errors->has('lacoid') ? 'has-error' : ''}}">
    <label for="lacoid" class="control-label">{{ 'รหัส' }}</label>
    <input class="form-control" name="lacoid" type="text" id="lacoid" required value="{{ $staff->lacoid or ''}}" >
    {!! $errors->first('lacoid', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-4  {{ $errors->has('name') ? 'has-error' : ''}}">
    <label for="name" class="control-label">{{ 'ชื่อ' }}</label>
    <input class="form-control" name="name" type="text" id="name" required value="{{ $staff->name or ''}}" >
    <input name="status" type="hidden" id="status" value="Active" >
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-2  {{ $errors->has('department_id') ? 'has-error' : ''}}">
        {!! Form::label('department_id', 'Deparment', ['class' => 'control-label']) !!}
        @if (isset($staff->department_id))
            {!! Form::select('department_id', $departmentlist,$staff->department_id, ['class' => 'form-control']) !!}   
        @else
            {!! Form::select('department_id', $departmentlist,null, ['class' => 'form-control']) !!}
        @endif
        {!! $errors->first('department_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-2  {{ $errors->has('shift_id') ? 'has-error' : ''}}">
        {!! Form::label('shift_id', 'Shift', ['class' => 'control-label']) !!}
        @if (isset($staff->shift_id))
            {!! Form::select('shift_id', $shiftlist,$staff->shift_id, ['class' => 'form-control']) !!}   
        @else
            {!! Form::select('shift_id', $shiftlist,null, ['class' => 'form-control']) !!}
        @endif
        {!! $errors->first('shift_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-2  {{ $errors->has('status') ? 'has-error' : ''}}">
        {!! Form::label('status', 'Status', ['class' => 'control-label']) !!}
        @if (isset($staff->status))
            {!! Form::select('status', array('Active'=>'Active','Inactive'=>'Inactive'),$staff->status, ['class' => 'form-control']) !!}   
        @else
            {!! Form::select('status', array('Active'=>'Active','Inactive'=>'Inactive'),null, ['class' => 'form-control']) !!}
        @endif
        {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
</div>

<div class="col-md-12">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
