@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">staff {{ $staff->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/staffs') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/staffs/' . $staff->id . '/edit') }}" title="Edit staff"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                        <form method="POST" action="{{ url('staffs' . '/' . $staff->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete staff" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                        </form>
                        <br/>
                        <br/>
                        <div class="row">
                            <div class="col-md-4" ><b>ID</b> : {{ $staff->id }}</div>
                            <div class="col-md-4" ><b>LACO Code</b> : {{ $staff->lacoid }}</div>
                            <div class="col-md-4" ><b>Name</b> : {{ $staff->name }}</div>
                        </div>
                        <br/>

                        <form method="POST" action="{{ url('/staffs/setExpAction/' . $staff->id) }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                            {{ csrf_field() }}
                        
                            <div id="accordion">
                            @foreach ($jobslistDep as $key=>$jobarr)
                                <div class="card">
                                    <div class="card-header" id="heading{{$key}}">
                                    <h5 class="mb-{{$key}}">
                                        <button class="btn btn-warning" data-toggle="collapse" data-target="#collapse{{$key}}" aria-expanded="true" aria-controls="collapse{{$key}}" onclick="return false;">
                                        {{ $departmentlistname[$key] }}
                                        </button>
                                    </h5>
                                    </div>

                                    <div id="collapse{{$key}}" class="collapse" aria-labelledby="heading{{$key}}" data-parent="#accordion">
                                    <div class="card-body">
                                        <div class="row">
                                            @foreach ($jobarr as $item)
                                            <div class="col-md-3">{{ $item->name }}<br/>
                                                <input class="form-control" name="exp{{$item->id}}" type="text" id="exp{{$item->id}}" value="{{ $expvalList[$item->id]->priority or ''}}" >
                                            </div>
                                            @endforeach
                                                    </div>
                                    </div>
                                </div>
                                 </div>
                            @endforeach
                            </div>

                    
                            <div class="row">
                            <div class="col-md-12">
                                <input class="btn btn-primary" type="submit" value="Set Exp">
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
