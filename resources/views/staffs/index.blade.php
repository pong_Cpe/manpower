@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">พนักงาน + ความถนัด</div>
                    <div class="card-body">
                        <a href="{{ url('/staffs/create') }}" class="btn btn-success btn-sm" title="Add New staff">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>
                        <a href="{{ url('/staffs/uploadHr') }}" class="btn btn-success btn-sm" title="Add New staff">
                            <i class="fa fa-upload" aria-hidden="true"></i> Upload
                        </a>
                        <form method="GET" action="{{ url('/staffs') }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                            <div class="input-group">
                                <input type="text" class="form-control" name="search" placeholder="รหัสพนักงาน..." value="{{ request('search') }}">
                                <select name="departmentid" class="form-control" id="departmentid">
                                    @foreach ($departmentlist as $key=>$iteml)
                                        <option value="{{ $key }}" 
                                        @if (Request::get('departmentid') == $key)
                                              selected
                                        @endif
                                        >{{ $iteml }}</option>  
                                    @endforeach
                                </select>
                                <span class="input-group-append">
                                    <button class="btn btn-secondary" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </form>

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>แผนก / กะ</th>
                                        <th>laco code</th>
                                        <th>Name</th>
                                        <th>สถานะ</th>
                                        <th>job</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($staffs as $item)
                                    <tr @if ($item->status == 'Inactive')
                                        style="background-color:red;"
                                    @endif>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->maindep->name or '-' }} / {{ $item->shift->name or '-' }}</td>
                                        <td>{{ $item->lacoid }}</td>
                                        <td>{{ $item->name }}</td>
                                        <td>{{ $item->status }}</td>
                                        <td>                                     
                                            @if ($item->exp->count()>0)
                                               @foreach ($item->exp()->orderBy('priority')->get() as $subitem)
                                        ({{ $subitem->priority or '-'}}) {{ $subitem->job->name or $subitem->job_id }} ({{ $subitem->job->department->name or '-' }})<br/>
                                                @endforeach 
                                            @endif
                                        </td>
                                        <td>
                                            <a href="{{ url('/staffs/setExp/' . $item->id) }}" title="Exp staff"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Set Exp</button></a>
                                            <a href="{{ url('/staff-shares/listshare/' . $item->id) }}" title="Exp staff"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> ส่งไปช่วยงาน</button></a>

                                            <a href="{{ url('/staffs/' . $item->id) }}" title="View staff"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a>
                                            <a href="{{ url('/staffs/' . $item->id . '/edit') }}" title="Edit staff"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                                            <form method="POST" action="{{ url('/staffs' . '/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-danger btn-sm" title="Delete staff" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $staffs->appends(['search' => Request::get('search'),'departmentid' => Request::get('departmentid')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
