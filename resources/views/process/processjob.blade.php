<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Job Process</title>
    <style>
    tr th {
        border: 1px solid #000000;
        word-wrap: normal;
    }
    tr th.noborder {
        border: none;
        word-wrap: normal;
    }
     tr th.noborder-last {
        border: none;
        word-wrap: normal;
    }
    tr th.noborderr {
        border: none;
        text-align: right;
        word-wrap: break-word;
    }
    tr th.noborderc {
        border: none;
        text-align: center;
        word-wrap: break-word;
        font: bolder;
    }
    tr td {
        border: 1px solid #000000;
        word-wrap: normal;
    }
    </style>
</head>
<body>
    <table>
        <thead>
                                    <tr>
                                        <th>พนักงาน</th>
                                        <th>งาน</th>
                                        <th>สถานะจากข้อมูล</th>
                                        <th>สถานะจากคำนวณ</th>
                                        <th>วันเวลา</th>
                                    </tr>
                                </thead>
        <tbody>
            @foreach($data as $keystaff=>$item2)
                @foreach($item2 as $keyjobdetails=>$item1)
                    @foreach($item1 as $item)
                    <tr>
                    <td>{{ $item['staff_name'] }}</td>
                    <td>{{ $item['job_name'] }}</td>
                    <td>{{ $item['db_process'] }}</td>
                    <td>{{ $item['auto_process'] }}</td>
                    <td>{{ $item['log_time'] }}</td>
                    </tr>  
                    @endforeach                                 
                @endforeach
            @endforeach
        </tbody>
    </table>
</body>
</html>