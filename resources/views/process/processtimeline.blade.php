<!DOCTYPE html>
<html>
  <head>
    <title>Bar Chart</title>
    <script src="https://cdn.jsdelivr.net/npm/moment/moment.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.3/dist/Chart.js"></script>
    <script src="{{ asset('js/timeline.js') }}"></script>

    <style>
      canvas {
        -moz-user-select: none;
        -webkit-user-select: none;
        -ms-user-select: none;
      }
    </style>
  </head>

  <body>
      @php
     //     $date=date_create("2013-03-15");
echo "ตอนนี้วันเวลา " .date("c")." งาน ".$maindata->process_date;
echo "<br/>".date('c')." - ".date($maindata->process_date.' 18:30:00');

      @endphp
    <div id="container" style="width: 90%;">
      <canvas id="canvas"></canvas>
    </div>
    <script>
      var ctx = document.getElementById("canvas").getContext("2d");
      var chart = new Chart(ctx, {
        type: "timeline",
        options: {
          elements: {
            colorFunction: function (text, data, dataset, index) {
              return '#606080';
            },
            showText: true,
            textPadding: 4,
          },
        },
        data: {
          labels: [
              @foreach($data2 as $keystaff=>$item2)
                @foreach($item2 as $keyjobdetails=>$item1)
              "{{ $keystaff }} | {{ $keyjobdetails }}",
                    @endforeach                                 
                @endforeach
              ],
          datasets: [
              
           
            
                @foreach($data2 as $keystaff=>$item2)
                @foreach($item2 as $keyjobdetails=>$item1)
            {
              data: [
                @php
                    $loop = 0;
                    $prevdatetime = "";
                    foreach($item1 as $item){
                        if($loop%2 == 1){
                            $date1=date_create($prevdatetime);
                            $date2=date_create($item['log_time']);
                            $diff=date_diff($date1,$date2);

                            echo '"'.date_format($item['log_time'],"c").'","IN T '.$diff->format("%h:%i").'"],';
                        }else{
                            $prevdatetime = $item['log_time'];
                            echo '["'.date_format($item['log_time'],"c").'",';
                        }
                        $loop++;
                    }
                    if($loop%2 == 1){
                        $date1=date_create($prevdatetime);
                        $date2=date_create(date("c"));
                        
                       if(date("c") > date($maindata->process_date." 18:30:00")){
                          $date2=date_create($maindata->process_date." 18:30:00");
                          $diff=date_diff($date1,$date2);
                          echo '"'.date_format(date_create($maindata->process_date." 18:30:00"),"c").'","IN A '.$diff->format("%h:%i").'"],';
                        }else{
                          $diff=date_diff($date1,$date2);
                          echo '"'.date("c").'","IN A '.$diff->format("%h:%i").'"],';
                        }
                            
                    }
                @endphp


              ],
            },
            @endforeach                                 
                @endforeach
           
          ],
        },
      });

   setTimeout(function(){
       location.reload();
   },(60000 * 3));

    </script>
  </body>
</html>
