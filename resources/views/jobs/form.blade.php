<div class="col-md-4  {{ $errors->has('department_id') ? 'has-error' : ''}}">
        {!! Form::label('department_id', 'แผนก', ['class' => 'control-label']) !!}
        @if (isset($job->department_id))
            {!! Form::select('department_id', $departmentlist,$job->department_id, ['class' => 'form-control']) !!}   
        @else
            {!! Form::select('department_id', $departmentlist,null, ['class' => 'form-control']) !!}
        @endif
        {!! $errors->first('department_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-4  {{ $errors->has('name') ? 'has-error' : ''}}">
    <label for="name" class="control-label">{{ 'ชื่องาน' }}</label>
    <input class="form-control" name="name" type="text" id="name" required value="{{ $job->name or ''}}" >
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-4  {{ $errors->has('job_type_id') ? 'has-error' : ''}}">
        {!! Form::label('job_type_id', 'Type', ['class' => 'control-label']) !!}
        @if (isset($job->job_type_id))
            {!! Form::select('job_type_id', $jobtypelist,$job->job_type_id, ['class' => 'form-control']) !!}   
        @else
            {!! Form::select('job_type_id', $jobtypelist,null, ['class' => 'form-control']) !!}
        @endif
        {!! $errors->first('job_type_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-12  {{ $errors->has('desc') ? 'has-error' : ''}}">
    <label for="desc" class="control-label">{{ 'รายละเอียด' }}</label>
    <textarea class="form-control" name="desc" type="text" id="desc">{{ $job->desc or ''}}</textarea>
    {!! $errors->first('desc', '<p class="help-block">:message</p>') !!}
</div>

<div class="col-md-12">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
