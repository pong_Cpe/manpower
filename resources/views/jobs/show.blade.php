@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">งาน {{ $job->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/jobs') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/jobs/' . $job->id . '/edit') }}" title="Edit job"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                        <form method="POST" action="{{ url('jobs' . '/' . $job->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete job" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                        </form>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $job->id }}</td>
                                    </tr>
                                    <tr>
                                        <th>ส่วนงาน</th><td>{{ $job->department->name }}</td>
                                    </tr>
                                    <tr>
                                        <th>งาน</th><td>{{ $job->name }}</td>
                                    </tr>
                                    <tr>
                                        <th>Desc</th><td>{{ $job->desc }}</td>
                                    </tr>
                                    <tr>
                                        <th>Type</th><td>{{ $job->jobtype->name }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
