@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">StaffShare {{ $staffshare->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/staff-shares/listshare/'.$staffshare->staff_id) }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/staff-shares/' . $staffshare->id . '/edit') }}" title="Edit StaffShare"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                        <form method="POST" action="{{ url('staffshares' . '/' . $staffshare->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete StaffShare" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                        </form>
                        <br/>
                        <br/>
                        <div class="col-md-12"><b>Staff : </b>{{ $staffObj->lacoid }} {{ $staffObj->name }} <b>จากแผนก : </b> {{ $staffObj->maindep->name }} </div>
                        <div class="col-md-12"><b>วันที่ : </b>{{ $staffshare->date_share }} <b>ไปช่วยงานแผนก : </b> {{ $staffshare->maindep->name }}  <b>Note : </b> {{ $staffshare->note }} </div>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $staffshare->id }}</td>
                                    </tr>
                                    
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
