<div class="col-md-12"><b>Staff : </b>{{ $staffObj->lacoid }} {{ $staffObj->name }} <b>จากแผนก : </b> {{ $staffObj->maindep->name }} </div>

<div class="col-md-4 {{ $errors->has('date_share') ? 'has-error' : ''}}">
    <input type="hidden" name="staff_id" id="staff_id" value="{{ $staffObj->id }}">
    <label for="date_share" class="control-label">{{ 'วันที่ช่วยงาน' }}</label>
    <input class="form-control" name="date_share" type="date" id="date_share" required value="{{ $staffshare->date_share or \Carbon\Carbon::now()->format('Y-m-d') }}" >
    {!! $errors->first('date_share', '<p class="help-block">:message</p>') !!}
</div>

<div class="col-md-4  {{ $errors->has('to_main_dep_id') ? 'has-error' : ''}}">
        {!! Form::label('to_main_dep_id', 'Deparment', ['class' => 'control-label']) !!}
        @if (isset($staffshare->to_main_dep_id))
            {!! Form::select('to_main_dep_id', $departmentlist,$staffshare->to_main_dep_id, ['class' => 'form-control']) !!}   
        @else
            {!! Form::select('to_main_dep_id', $departmentlist,null, ['class' => 'form-control']) !!}
        @endif
        {!! $errors->first('to_main_dep_id', '<p class="help-block">:message</p>') !!}
</div>

<div class="col-md-4  {{ $errors->has('note') ? 'has-error' : ''}}">
    <label for="note" class="control-label">{{ 'Note' }}</label>
    <input class="form-control" name="note" type="text" id="note" value="{{ $staff->note or ''}}" >
    {!! $errors->first('note', '<p class="help-block">:message</p>') !!}
</div>

<div class="col-md-12 form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
