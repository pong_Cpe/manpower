@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Lacostaffs</div>
                    <div class="card-body">
                        <a href="{{ url('/laco-staffs/create') }}" class="btn btn-success btn-sm" title="Add New LacoStaff">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>
                        
                        <a href="{{ url('/laco-staffs/upload') }}" class="btn btn-success btn-sm" title="Add New staff">
                            <i class="fa fa-upload" aria-hidden="true"></i> Upload
                        </a>
                        <form method="GET" action="{{ url('/laco-staffs') }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                            <div class="input-group">
                                <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
                                <span class="input-group-append">
                                    <button class="btn btn-secondary" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </form>

                        @if(Session::has('flash_message'))
                        <p class="alert alert-info">{{ Session::get('flash_message') }}</p>
                        @endif
                        <br/>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>รหัสพนักงาน</th>
                                        <th>ชื่อ สกุล</th>
                                        <th>แผนก / กะ</th>
                                        <th>สถานะ</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($lacostaffs as $item)
                                    <tr>
                                        <td>{{ $item->lacoid }}</td>
                                        <td>{{ $item->name }}</td>
                                        <td>{{ $item->maindep->name }} / {{ $item->shift->name }}</td>
                                        <td>{{ $item->status }}</td>
                                        
                                        <td>
                                            <a href="{{ url('/laco-staffs/' . $item->id) }}" title="View LacoStaff"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a>
                                            <a href="{{ url('/laco-staffs/' . $item->id . '/edit') }}" title="Edit LacoStaff"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                                            <form method="POST" action="{{ url('/laco-staffs' . '/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-danger btn-sm" title="Delete LacoStaff" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $lacostaffs->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
